## COLEÇÕES

## Sumário

1. [Coleções equals](#coleções-equals)
2. [Coleções hashCode](#coleções-hashCode)
3. [Coleções Complexidade BigO](#coleções-complexidade-bigo)
4. [Coleções List](#coleções-list)
5. [Coleções Sorting List](#coleções-sorting-list)
6. [Coleções Sorting List Comparable](#coleções-sorting-list-comparable)
7. [Coleções Sorting List Comparator](#coleções-sorting-list-comparator)
8. [Coleções Binary Search](#coleções-binary-search)
9. [Coleções Conversão de lista para arrays e vice versa](#coleções-conversão-de-lista-para-arrays-e-vice-versa)
10. [Coleções Iterator](#coleções-iterator)
11. [Coleções Seat HashSet](#coleções-seat-hashset)
12. [Coleções NavigableSet TreeSet](#coleções-navigableSet-TreeSet)
13. [Coleções HashMap LinkedHashMap](#coleções-hashmap-linkedhashmap)
14. [Coleções NavigableMap TreeMap](#Coleções-navigablemap-treemap)
15. [Coleções Queue PriorityQueue](#coleções-queue-priorityqueue)

## Coleções equals

Observe no exemplo abaixo:

- Note que a comparação abaixo retorna o valor true, pois referência para o mesmo objeto em memória

```java
public class EqualsTest01 {
    public static void main(String[] args) {
        String name = "André Luiz";
        String name2 = new String("André Luiz");
        System.out.println(name == name2); // true
    }
}
```

Agora vamos ver outro exemplo:

- Note neste caso agora que o retorno da comparação de igualdade será `falso`, pois temos uma variável no poll de Strings e a outra variável está no hipe
- Para nós seres humanos quando olhamos a comparação sabemos que sim, eles são iguais, porém para o java não são iguais.
- No Java quando usamos `==` o valor que será comparado não é o valor em si do objeto, mas sim o valor da referência.
- 

```java
public class EqualsTest01 {
    public static void main(String[] args) {
        String name = "André Luiz";
        String name2 = new String("André Luiz");
        System.out.println(name == name2); // false
    }
}
```

Agora neste outro exemplo temos:
- Temos a comparação com retorno true.

```java
public class EqualsTest01 {
    public static void main(String[] args) {
        String name = "André Luiz";
        String name2 = new String("André Luiz");
        System.out.println(name.equals(name2)); // true
    }
}
```

Agora observe essa outra teste:

- Note que temos dois objetos criados com as mesma especificações em seus atributos, porém quando comparamos `s1.equals(s2)` temos um retorno `false`.

```java
public class EqualsTest01 {
    public static void main(String[] args) {
        Smartphone s1 = new Smartphone("1ABC1", "Iphone");
        Smartphone s2 = new Smartphone("1ABC1", "Iphone");
        System.out.println(s1.equals(s2)); // false
    }
}
```

Veja na imagem abaixo porque a comparação anterior retorna `false`:

- Note que temos duas variáveis de referência e dois objetos
- Quando usamos o `equals` que vem o objeto basicamente estamos comparando o this
- O que acontece aqui é que estamos comparando se a variável `s1` faz referência para o mesmo objeto da variável `s2`.
- Então neste caso, podemos ver que não, elas não fazem referência para o mesmo objeto, logo o retorno é `falso`

<img alt="colecoes equals" src="./public/images/colecoes-equals.png" width="850"> 

Podemos mudar esse comportamento de comparação do Java da seguinte forma:

Para isso temos que sobrescrever o método equals do Java, porém para isso temos de seguir algumas regras:

- `Reflexivo`: x.equals(x) tem que ser true para tudo que for diferente de null
- `Simétrico`: para x e y diferentes de null, se x.equals(y) == true logo, y.equals(x) == true
- `Transitividade`: Para x, y, z diferentes de null, se x.equals(y) == true, e x.equals(z) == true logo y.equals(z) == true
- `Consistente`: x.equals(x) sempre retorna true se x for diferente de null
- para x diferente de null, x.equals(null) tem que retornar false.


No exemplo abaixo vamos fazer o sobrescrita do método equals:

- Note que agora se executarmos nosso código o retorno será true, pois neste momento estamos verificando se os objetos possuem o mesmo serial Number.
- Isso deve ser usado com cuidado, pois depende muito da regra de negócio da nossa aplicação.


```java
public class Smartphone {
    private String serialNumber;
    private String marca;

    // Reflexivo: x.equals(x) tem que ser true para tudo que for diferente de null
    // Simétrico: para x e y diferentes de null, se x.equals(y) == true logo, y.equals(x) == true
    // Transitividade: Para x, y, z diferentes de null, se x.equals(y) == true, e x.equals(z) == true logo y.equals(z) == true
    // Consistente: x.equals(x) sempre retorna true se x for diferente de null
    // para x diferente de null, x.equals(null) tem que retornar false.
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (this.getClass() != obj.getClass()) return false;
        Smartphone smartphone = (Smartphone) obj;
        return serialNumber != null && serialNumber.equals(smartphone.serialNumber);
    }

    public Smartphone(String serialNumber, String marca) {
        this.serialNumber = serialNumber;
        this.marca = marca;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
}
```

## Coleções hashCode

Vamos entender o que é um hashcode:

<img alt="colecoes hash code" src="./public/images/hashcode.png" width="850">

O Hash basicamente é gerar um valor numérico para idenficarmos o nosso valor. 

porém devemos tomar cuidado com isso, pois imagine o seguinte cenário:

<img alt="colecoes hash code" src="./public/images/hashcode2.png" width="850">

Da mesma forma anterior vamos fazer a sobrescrita do método `hashCode` porém ele temabém tem algumas regras:

- se x.equals(y) == true, y.hashCode()x == x.hashCode()
- y.hashCode() == x.hashCode() não necessariamente o equals de y.equals()x tem que ser true
- x.equals(y) == false
- y.hashCode() != x.hasCode() x.equals(y) deverá ser false.

```java
public class Smartphone {
    private String serialNumber;
    private String marca;
    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (this.getClass() != obj.getClass()) return false;
        Smartphone smartphone = (Smartphone) obj;
        return serialNumber != null && serialNumber.equals(smartphone.serialNumber);
    }

    // se x.equals(y) == true, y.hashCode()x == x.hashCode()
    // y.hashCode() == x.hashCode() não necessariamente o equals de y.equals()x tem que ser true
    // x.equals(y) == false
    // y.hashCode() != x.hasCode() x.equals(y) deverá ser false.
    @Override
    public int hashCode() {
        return serialNumber == null ? 0 : this.serialNumber.hashCode();
    }

    public Smartphone(String serialNumber, String marca) {
        this.serialNumber = serialNumber;
        this.marca = marca;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
}
```

## Coleções Complexidade BigO


<img alt="collections 2" src="./public/images/collection-java2.png" width="850">

A complexidade BigO define a velocidade baseado no tipo de operação que você esta fazendo.

- As coleções podem ser ordenadas, podem ser sortidas
- Quando usamos collections no Java devemos observar muito a questão de performance.
- QUando trabalhamos com coleções devemos ficar atentos com a complexidade BigO

<img alt="collections" src="./public/images/collection-java.png" width="850">

## Coleções List

Antigamente no Java o List era usado da seguinte forma:

```java
public class ListTest01 {
    public static void main(String[] args) {
        List<String> nomes = new ArrayList<>();
        nomes.add("André");
        nomes.add("DevDojo Academy");

        for (String nome: nomes) {
            System.out.println(nome);
        }
        nomes.add("Paulo");

        System.out.println("--------------------------");

        for (int i=0; i < nomes.size(); i++) {
            System.out.println(nomes.get(i));
        }
    }
}
```

Note que temos um problema aqui, a lista aceita valores de qualquer tipo, string, int, e isso não é muito bom.

Com a introdução das Generics o Java força em tempo de compilação a definição do tipo. Então usamos o systax diamond(sintaxe diamante) onde vamos forçar um tipo em tempo de compilação.

Com isso no exemplo abaixo vamos forçar a nossa Colection List a ter apenas tipos definidos, então neste momento não é possível colocar números em uma lista de strings.

```java
public class ListTest01 {
    public static void main(String[] args) {
        List<String> nomes = new ArrayList<>();
        nomes.add("André");
        nomes.add("DevDojo Academy");

        for (String nome: nomes) {
            System.out.println(nome);
        }
        System.out.println("--------------------------");

        for (int i=0; i < nomes.size(); i++) {
            System.out.println(nomes.get(i));
        }
    }
}
```

Dessa forma podemos tipar como String e podemos usar qualquer método que for para strings, já que neste momentos sabemos que ali só contém Strings.

### List Parte 2

Quando trabalhamos com List o valor que será definido para o List deve ser um `Objeto` como no nosso caso : `List<String>`, não é possível definir um List com tipos primitivos como int, double, float etc. Para isso temos que utilizar os Wrappers.

```java
public class ListTest01 {
    public static void main(String[] args) {
       List<Integer> numeros = new ArrayList<>();
       numeros.add(100);
       numeros.add(200);
       numeros.add(300);

       for (int n: numeros) {
           System.out.println(n);
       }
    }
}
```

Assim como o add Podemos remover elementos de uma lista

```java
public class ListTest01 {
    public static void main(String[] args) {
        List<String> nomes = new ArrayList<>();
        nomes.add("André");
        nomes.add("DevDojo Academy");
        nomes.remove(1);
        nomes.remove("André");
    }
}
```

Outra utilidade importante é:

imagine que temos duas listas de nomes e queremos adicionar os valores da lista1 na lista 2:

```java
public class ListTest01 {
    public static void main(String[] args) {
        List<String> nomes = new ArrayList<>();
        List<String> nomes2 = new ArrayList<>();

        nomes.add("André");
        nomes.add("DevDojo");

        nomes2.add("Paulo");
        nomes2.add("Silva");

        nomes.addAll(nomes2);
    }
}
```

Neste caso ele junta os dois arrays sem duplicar os itens.

### Parte 3

Imagine que pegamos vários dados do banco de dados, então vamos criar uma lista de smartphones a adicionar estes itens a esta lista:

```java
public class SmartphoneListTest01 {
    public static void main(String[] args) {
        Smartphone s1 = new Smartphone("1ABC1", "Iphone");
        Smartphone s2 = new Smartphone("22222", "Pixel");
        Smartphone s3 = new Smartphone("33333", "Samsung");

        List<Smartphone> smartphones = new ArrayList<>(6);
        smartphones.add(s1);
        smartphones.add(s2);
        smartphones.add(s3);

        for (Smartphone smartphone: smartphones) {
            System.out.println(smartphone);
        }

        Smartphone s4 = new Smartphone("22222", "Pixel");
        System.out.println(smartphones.contains(s4)); // Verifica através do serialNumber

        int indexSmartPhone = smartphones.indexOf(s4);//traz o indice do objeto que estamos passando
        System.out.println(indexSmartPhone);
    }
}
```

## Coleções Sorting List

Com o Sorting List podemos ordenar listas de acordo com o tipo do seus dados

- Para casos de listas com Strings, vamos ter a ordenação em ordem alfabética
- Números também são ordenados
- Quando fazemos a ordenação os indices também são alterados, ja que os itens trocam de posições.

```java
public class ListSortTest01 {
    public static void main(String[] args) {
        List<String> mangas = new ArrayList<>(6);
        mangas.add("Berserk");
        mangas.add("Hellsing Ultimate");
        mangas.add("Attack on Titan");
        mangas.add("Pokemon");
        mangas.add("Dragon ball Z");

        Collections.sort(mangas); // Reorganiza a lista em ordem alfabética

        for (String manga : mangas) {
            System.out.println(manga);
        }
    }
}
```

Exemplo com `List<Double>` 

```java
public class ListSortTest01 {
    public static void main(String[] args) {
        List<Double> salarios = new ArrayList<>();
        salarios.add(100.21);
        salarios.add(23.98);
        salarios.add(21.21);
        salarios.add(98.10);

        Collections.sort(salarios);
        System.out.println(salarios);
    }
}
```

Podemos ordenar também uma lista de determinados objetos, porém para isso devemos ter atenção em alguns pontos:

- Se tentarmos realizar um sort em uma lista de `Mangas` por exemplo o Java não sabe por qual atributo irá ordenar, e como não informamos ao Java qual atributo será usado, isso causará uma exception.

Vamos ver o exemplo na prática:

- Vamos criar uma classe chamada `Manga`
- Vamos criar o `construtor`
- Vamos criar os `getters` and `Setters`
- Vamos Sobrescrever os métodos `toString`
- Vamos sobrescrever os método `Equals` e `HashCode`.

```java
public class Manga {
    private Long id;
    private String nome;
    private double preco;

    public Manga(Long id, String nome, double preco) {
        Objects.requireNonNull(id, "id is not null");
        Objects.requireNonNull(nome, "name is not null");
        this.id = id;
        this.nome = nome;
        this.preco = preco;
    }

    @Override
    public String toString() {
        return "Manga{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", preco=" + preco +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Manga manga = (Manga) o;
        return Double.compare(manga.preco, preco) == 0 && id.equals(manga.id) && nome.equals(manga.nome);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, preco);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}
```

## Coleções Sorting List Comparable

De posse da nossa classe `Manga` criada no tópico anterior, neste tópico vamos realizar o sort de uma lista de `Mangas`:

Como vimos anteriormente, as classes String, Double conseguem realizar a ordenação normalmente, isso por que elas implementam a classe `Comparable` com isso podemos fazer o mesmo para a nossa classe `Manga` e neste momento ela passará.

Para isso precisamos de alguns passos:

- Precisamos implementar a `Comparable` passando o tipo `Manga`
- Precisamos implementar o método `compareTo` que possui uma regra:
  - Esse método `compareTo` retorna um inteiro
  - Nele temos acesso a dois objetos o `this` que é o objeto que está excutando esse `compareTo`
  - E temos o outro objeto que está sendo passado como argumento.
    
  Regras:
    - Retorna negativo se  o this for < que `manga` 
    - se this == `manga`, return 0
    - positivo se this > `manga`

```java
 public int compareTo(Manga manga) {
    if (this.id < manga.getId()) {
        return -1;
    }
    if (this.id.equals(manga.getId())) {
        return 0;
    } else {
        return 1;
    }
}
```

podemos melhorar isso usando da seguinte forma:

```java
// Isso não funciona para tipos primitivos, pois não possuem o método compareTo
public int compareTo(Manga manga) {
    return this.id.compareTo(manga.getId());
}
```

Então agora, que temos a implementação do comparable, ao realizar um sort da nossa lista de mangas, vamos ter o retorno da lista ordenada.

Observação:

É importante aqui, lembrar que a única forma de isso funcionar é realizar a implementação do `Comparable` pois é através dele que o java sabe como vai ordenar pois ele vai chamar a implementação do `compareTo`.


```java
public class MangaSortTest01 {
    public static void main(String[] args) {
        List<Manga> mangas = new ArrayList<>(6);

        mangas.add(new Manga(6L, "Hellsing Ultimate", 9.5));
        mangas.add(new Manga(8L, "Pokemon", 11.20));
        mangas.add(new Manga(5L, "Berserk", 19.9));
        mangas.add(new Manga(9L, "Dragon ball Z", 2.99));
        mangas.add(new Manga(7L, "Attack on Titan", 3.2));

        System.out.println("Lista de Mangas: ");
        for (Manga manga : mangas) {
            System.out.println(manga);
        }

        System.out.println("Lista de Mangas ordenados:");
        Collections.sort(mangas);
        for (Manga manga : mangas) {
            System.out.println(manga);
        }
    }
}
```

Então no fim nossa classe ficaria dessa forma:

```java
public class Manga implements Comparable<Manga> {
    private Long id;
    private String nome;
    private double preco;

    public Manga(Long id, String nome, double preco) {
        Objects.requireNonNull(id, "id is not null");
        Objects.requireNonNull(nome, "name is not null");
        this.id = id;
        this.nome = nome;
        this.preco = preco;
    }

    @Override
    public String toString() {
        return "Manga{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", preco=" + preco +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Manga manga = (Manga) o;
        return Double.compare(manga.preco, preco) == 0 && id.equals(manga.id) && nome.equals(manga.nome);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, preco);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    /**
     * Regras:
     * negativo se this < manga
     * se this == manga return 0
     * positivo se this > manga
     *
     */
    @Override
    public int compareTo(Manga manga) {
       return this.id.compareTo(manga.getId());
    }
}
```

```java
 public int compareTo(Manga manga) {
    // Comparação por nome
    // return this.nome.compareTo(manga.getNome());
}
```

## Coleções Sorting List Comparator

- vamos a uma situação que pode ocorrer quando trabalhamos com ordenação de listas.
- Imagine que temos um sistema com um classe ordenada por nome, e a equipe possui 5 desenvolvedores, e em determinado momento você precisa alterar a ordenação, neste momento não será mais por nome.
- Já podemos imaginar o problema que podemos ter não é?
- Então para que não façamos a alteração e quebramos tudo que já foi desenvolvido podemos usar o `Comparator` para contornar esse problema.

O Comparator também é uma interface, porém a diferença entre ele o e Comparable é que essa interface, tem o método compare, e ela não é implementada diretamente na nossa classe.

Então vamos lá:

Imagine que temos nossos `Mangas` ordenados por nome, e digamos que em um específico lugar no software ele precisa ser ordenado pelo id


```java
public class MangaSortTest01 {
    public static void main(String[] args) {
        List<Manga> mangas = new ArrayList<>(6);

        mangas.add(new Manga(6L, "Hellsing Ultimate", 9.5));
        mangas.add(new Manga(8L, "Pokemon", 11.20));
        mangas.add(new Manga(5L, "Berserk", 19.9));
        mangas.add(new Manga(9L, "Dragon ball Z", 2.99));
        mangas.add(new Manga(7L, "Attack on Titan", 3.2));

        System.out.println("Lista de Mangas: ");
        for (Manga manga : mangas) {
            System.out.println(manga);
        }

        System.out.println("Lista de Mangas ordenados:");
        Collections.sort(mangas);
        for (Manga manga : mangas) {
            System.out.println(manga);
        }

        System.out.println("Lista pelo id");
        Collections.sort(mangas, new MangaByIdComparator());
        for (Manga manga : mangas) {
            System.out.println(manga);
        }
    }
}
```

## Coleções Binary Search

É mais uma forma de fazermos busca em uma coleção.

Faz uma busca na lista que passarmos para ele e ele retornará o índice, ou caso não encontre ele retorna algo como `(-(ponto de inserção) -1)`;

```java
public class BinarySearchTest01 {
    public static void main(String[] args) {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(2);
        numeros.add(0);
        numeros.add(4);
        numeros.add(3);

        // (- (ponto de inserção) -1)
        Collections.sort(numeros);
        System.out.println(Collections.binarySearch(numeros, 2)); // retorna 1 caso encontre
        System.out.println(Collections.binarySearch(numeros, 1)); // retorna -2
    }
}
```

```java
public class BinarySearchTest02 {
    public static void main(String[] args) {
        List<Manga> mangas = new ArrayList<>(6);

        mangas.add(new Manga(6L, "Hellsing Ultimate", 9.5));
        mangas.add(new Manga(8L, "Pokemon", 11.20));
        mangas.add(new Manga(5L, "Berserk", 19.9));
        mangas.add(new Manga(9L, "Dragon ball Z", 2.99));
        mangas.add(new Manga(7L, "Attack on Titan", 3.2));

        System.out.println("Lista de Mangas ordenados por nome: ");
        Collections.sort(mangas);
        for (Manga manga : mangas) {
            System.out.println(manga);
        }

        Manga mangaToSearch = new Manga(9L, "Dragon ball Z", 2.99);
        System.out.println(Collections.binarySearch(mangas, mangaToSearch)); // retorna 4
    }
}
```

```java
public class BinarySearchTest02 {
    public static void main(String[] args) {
        MangaByIdComparator mangaByIdComparator = new MangaByIdComparator();
        List<Manga> mangas = new ArrayList<>(6);

        mangas.add(new Manga(6L, "Hellsing Ultimate", 9.5));
        mangas.add(new Manga(8L, "Pokemon", 11.20));
        mangas.add(new Manga(5L, "Berserk", 19.9));
        mangas.add(new Manga(9L, "Dragon ball Z", 2.99));
        mangas.add(new Manga(7L, "Attack on Titan", 3.2));

        System.out.println("Lista de Mangas ordenados por nome: ");
        mangas.sort(new MangaByIdComparator());
        for (Manga manga : mangas) {
            System.out.println(manga);
        }

        Manga mangaToSearch = new Manga(9L, "Dragon ball Z", 2.99);
        System.out.println(Collections.binarySearch(mangas, mangaToSearch, mangaByIdComparator));
    }
}

```

## Coleções Conversão de lista para arrays e vice versa

Pegar em arrays os valores de um `ArrayList` 

```java
public class ListaArrayCOnversaoTest01 {
    public static void main(String[] args) {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Integer[] integersArray = numeros.toArray(new Integer[0]);
        System.out.println(Arrays.toString(integersArray));
    }
}
```

Agora imagine que temos um array de inteiros.

Transformando um array em uma lista:

```java
public class ListaArrayCOnversaoTest01 {
    public static void main(String[] args) {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        Integer[] listToArray = numeros.toArray(new Integer[0]);
        System.out.println(Arrays.toString(listToArray));

        System.out.println("====================");

        Integer[] numerosArray = new Integer[3];
        numerosArray[0] = 1;
        numerosArray[1] = 2;
        numerosArray[2] = 3;

        List<Integer> arrayToList = Arrays.asList(numerosArray);
        arrayToList.set(0,12);

        System.out.println(Arrays.toString(numerosArray));
        System.out.println(arrayToList);

        System.out.println("====================");
    }
}
```

Da forma anterior não podemos fazer a inserção de novos dados na lista, ou seja a lista não pode ser alterada pois teremos uma exception. 

Mas temo um forma de alterarmos nossa lista da seguinte forma:

```java
List<Integer> numerosList = new ArrayList<>(Arrays.asList(numerosArray));
numerosList.add(15);
System.out.println(numerosList);
System.out.println(numerosList);
List<String> strings = Arrays.asList("1", "2");
```

## Coleções Iterator

Como podemos remover dados de uma lista

É uma pessíma ideia fazer remove usando `Foreach`:

```java
 // NÃO FAÇA ISSO NÃO FAÇA ISSO NÃO FAÇA ISSO NÃO FAÇA ISSO
 for (Manga manga : mangas) { 
    if(manga.getQuantidade() == 0) {
        mangas.remove(manga);
    }
}
```


Antigamente a remoção funciona assim:

```java
public class IteratorTest01 {
    public static void main(String[] args) {
        List<Manga> mangas = new ArrayList<>(6);

        mangas.add(new Manga(6L, "Hellsing Ultimate", 2.99, 0));
        mangas.add(new Manga(8L, "Pokemon", 5.00, 1));
        mangas.add(new Manga(5L, "Berserk", 7.99, 0));
        mangas.add(new Manga(9L, "Dragon ball Z", 3.99, 1));
        mangas.add(new Manga(7L, "Attack on Titan", 80, 1));

        Iterator<Manga> mangaIterator = mangas.iterator();
        while (mangaIterator.hasNext()) {
            if(mangaIterator.next().getQuantidade() == 0) {
                mangaIterator.remove();
            }
        }
        System.out.println(mangas);
    }
}
```

Porém com o Java 8 com a programação funcional e adicionou o método removeIf

Note que usando o `LinkedList` vamos ter o mesmo resultado, porém uam vantagem do LinkedList é que ele é um array linkado, ou seja cada elemento da lista, conhece o próximo elemento e o anterior.

```java
import java.util.LinkedList;

public class IteratorTest01 {
  public static void main(String[] args) {
    List<Manga> mangas = new LinkedList<>();

    mangas.add(new Manga(6L, "Hellsing Ultimate", 2.99, 0));
    mangas.add(new Manga(8L, "Pokemon", 5.00, 1));
    mangas.add(new Manga(5L, "Berserk", 7.99, 0));
    mangas.add(new Manga(9L, "Dragon ball Z", 3.99, 1));
    mangas.add(new Manga(7L, "Attack on Titan", 80, 1));

    // navega entre todos os mangas e remove os mangas em que a quantidade for == 0
    mangas.removeIf(manga -> manga.getQuantidade() == 0);

    System.out.println(mangas);
  }
}
```

## Coleções Seat HashSet

- O Set não permite elementos duplicados dentro de uma coleção.
- O set não possui indice, pois o set não é indexado então não existe o `get` por isso devemos iterar sobre a lista.
- Se tentarmos inserir elementos duplicados em um set, ele não será inserido, pois o java vai verificar atraves do equals que garante isso.

```java
public class SetTest01 {
    public static void main(String[] args) {
        Set<Manga> mangas = new HashSet<>();

        mangas.add(new Manga(6L, "Hellsing Ultimate", 2.99, 0));
        mangas.add(new Manga(8L, "Pokemon", 5.00, 1));
        mangas.add(new Manga(5L, "Berserk", 7.99, 0));
        mangas.add(new Manga(9L, "Dragon ball Z", 3.99, 1));
        mangas.add(new Manga(7L, "Attack on Titan", 80, 1));
        mangas.add(new Manga(7L, "Attack on Titan", 80, 1));

        for (Manga manga: mangas) {
            System.out.println(manga);
        }
    }
}
```

## Coleções NavigableSet TreeSet

```java
public class NavigableSetTest01 {
    public static void main(String[] args) {
        NavigableSet<Smartphone> set = new TreeSet<>(new SmartphoneMarca());
        Smartphone smartphone = new Smartphone("123", "Nokia");
        set.add(smartphone);
        System.out.println(set);
        System.out.println("----------------------");

        NavigableSet<Manga> mangas =  new TreeSet<>();
        mangas.add(new Manga(6L, "Hellsing Ultimate", 2.99, 0));
        mangas.add(new Manga(8L, "Pokemon", 5.00, 1));
        mangas.add(new Manga(5L, "Berserk", 7.99, 0));
        mangas.add(new Manga(9L, "Dragon ball Z", 3.99, 1));
        mangas.add(new Manga(7L, "Attack on Titan", 80, 1));
        for (Manga manga : mangas) {
            System.out.println(manga);
        }
    }
}
```

Inveretendo a ordem:

```java
for (Manga manga : mangas.descendingSet()) {
    System.out.println(manga);
}
```

```java
public class NavigableSetTest01 {
    public static void main(String[] args) {
        NavigableSet<Smartphone> set = new TreeSet<>(new SmartphoneMarca());
        Smartphone smartphone = new Smartphone("123", "Nokia");
        set.add(smartphone);
        System.out.println(set);
        System.out.println("----------------------");

        NavigableSet<Manga> mangas =  new TreeSet<>(new MangaPrecoComparator());
        mangas.add(new Manga(6L, "Hellsing Ultimate", 2.99, 0));
        mangas.add(new Manga(8L, "Pokemon", 5.00, 1));
        mangas.add(new Manga(5L, "Berserk", 7.99, 0));
        mangas.add(new Manga(9L, "Dragon ball Z", 3.99, 1));
        mangas.add(new Manga(7L, "Attack on Titan", 80, 1));

        for (Manga manga : mangas) {
            System.out.println(manga);
        }
    }
}
```

Temos 4 métodos interessantes:

- `lower <`: traz o que é imediatamente menor dentro da coleção
- `floor <=`: traz o mesmo o o menor imediato
- `higher >`: traz o maior
- `ceiling >=`: traz o maior ou igual

```java
class SmartphoneMarca implements Comparator<Smartphone> {

    @Override
    public int compare(Smartphone o, Smartphone t1) {
        return o.getMarca().compareTo(t1.getMarca());
    }
}

class MangaPrecoComparator implements Comparator<Manga> {

    @Override
    public int compare(Manga manga, Manga t1) {
        return Double.compare(manga.getPreco(), t1.getPreco());
    }
}

public class NavigableSetTest01 {
    public static void main(String[] args) {
        NavigableSet<Smartphone> set = new TreeSet<>(new SmartphoneMarca());
        Smartphone smartphone = new Smartphone("123", "Nokia");
        set.add(smartphone);
        System.out.println(set);
        System.out.println("----------------------");

        NavigableSet<Manga> mangas =  new TreeSet<>(new MangaPrecoComparator());
        mangas.add(new Manga(6L, "Hellsing Ultimate", 2.99, 0));
        mangas.add(new Manga(8L, "Pokemon", 5.00, 1));
        mangas.add(new Manga(5L, "Berserk", 7.99, 0));
        mangas.add(new Manga(9L, "Dragon ball Z", 3.99, 1));
        mangas.add(new Manga(7L, "Attack on Titan", 80, 1));

        for (Manga manga : mangas) {
            System.out.println(manga);
        }

        System.out.println("--------------------------------");

        Manga yuyu = new Manga(21L, "Yuyu Hakusho", 8, 5);
        System.out.println(mangas.lower(yuyu));
    }
}
```

## Coleções HashMap LinkedHashMap

- É uma lista com Keys e Values
- No map ao tentarmos adicionar duas chaves iguais ele sobrescreve o valor. 

```java
public class MapTest01 {
    public static void main(String[] args) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("teklado", "teclado");
        map.put("mouze", "mouse");
        map.put("vc", "você");
        map.putIfAbsent("vc", "você2");

        System.out.println("chaves e valores: ");
        System.out.println(map);
        for(String key : map.keySet()) {
            System.out.println(key + " : " +map.get(key));
        }

        System.out.println("------------------------");
        System.out.println("Chaves: ");
        for(String key : map.keySet()) {
            System.out.println(key);
        }

        System.out.println("------------------------");
        System.out.println("Valores: ");
        for(String value : map.values()) {
            System.out.println(value);
        }

        System.out.println("------------------------");
        System.out.println("Outra forma: ");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " - "+entry.getValue());
        }
    }
}
```

### parte 2

```java
public class MapTest02 {
    public static void main(String[] args) {
        Consumidor consumidor1 = new Consumidor("André Luiz");
        Consumidor consumidor2 = new Consumidor("DevDojo Acedemy");

        Manga manga1 = new Manga(6L, "Hellsing Ultimate", 9.5);
        Manga manga2 = new Manga(8L, "Pokemon", 11.20);
        Manga manga3 = new Manga(5L, "Berserk", 19.9);
        Manga manga4 = new Manga(9L, "Dragon ball Z", 2.99);
        Manga manga5 = new Manga(7L, "Attack on Titan", 3.2);

        Map<Consumidor, Manga> consumidorManga = new HashMap<>();
        consumidorManga.put(consumidor1, manga1);
        consumidorManga.put(consumidor2, manga4);
        for (Map.Entry<Consumidor, Manga> entry : consumidorManga.entrySet()) {
            System.out.println(entry.getKey().getNome() + " - "+entry.getValue().getNome());
        }
    }
}
```

### parte 3

<img alt="hashMap" src="./public/images/hashMap.png" width="850">

```java
public class MapTest03 {
    public static void main(String[] args) {
        Consumidor consumidor1 = new Consumidor("André Luiz");
        Consumidor consumidor2 = new Consumidor("DevDojo Acedemy");

        Manga manga1 = new Manga(6L, "Hellsing Ultimate", 9.5);
        Manga manga2 = new Manga(8L, "Pokemon", 11.20);
        Manga manga3 = new Manga(5L, "Berserk", 19.9);
        Manga manga4 = new Manga(9L, "Dragon ball Z", 2.99);
        Manga manga5 = new Manga(7L, "Attack on Titan", 3.2);

        List<Manga> mangaConsumidorList1 =  List.of(manga1, manga2, manga3);
        List<Manga> mangaConsumidorList2 =  List.of(manga3, manga4);
        Map<Consumidor, List<Manga>> consumidorManga = new HashMap<>();
        consumidorManga.put(consumidor1, mangaConsumidorList1);
        consumidorManga.put(consumidor2, mangaConsumidorList2);

        for (Map.Entry<Consumidor, List<Manga>> entry : consumidorManga.entrySet()) {
            System.out.println(entry.getKey().getNome());
            for (Manga manga : entry.getValue()) {
                System.out.println(manga.getNome());
            }
        }
    }
}
```

## Coleções NavigableMap TreeMap

```java
public class NavigableMapTest01 {
    public static void main(String[] args) {
        NavigableMap<String, String> map = new TreeMap<>();
        map.put("A", "Letra A");
        map.put("D", "Letra D");
        map.put("B", "Letra B");
        map.put("C", "Letra C");
        map.put("E", "Letra E");

        for(Map.Entry<String, String> entry: map.entrySet()) {
            System.out.println(entry.getKey()+" - "+entry.getValue());
        }

        System.out.println(map.headMap("C")); // Retorna todos que estão abaixo da letra c
        System.out.println(map.headMap("C", true)); // incluindo o C
        System.out.println(map.ceilingEntry("C"));
        System.out.println(map.higherEntry("C"));
        System.out.println(map.floorEntry("C"));
        System.out.println(map.lowerEntry("C"));
    }
}
```

## Coleções Queue PriorityQueue

- Queue é mais um tipo de coleção no java que baseado no padrão `FIFO - First in first out`.
- trabalhando com a classe `PriorityQueue` podemos trabalhar com prioridades customizadas.

```java
public class QueueTest01 {
    public static void main(String[] args) {
        Queue<String> fila = new PriorityQueue<>();
        fila.add("C");
        fila.add("A");
        fila.add("B");

       while(!fila.isEmpty()) {
           System.out.println(fila.poll());
       }
    }
}
```