package src.academy.devdojo.maratona.java.javaintermediario.Clambdas.test;

import java.util.List;
import java.util.function.Consumer;

public class LambdaTest01 {
    public static void main(String[] args) {
        List<String> strings = List.of("André", "Suane", "Paulo", "Luffy");
        List<Integer> numbers = List.of(1,2,3,4,5);
        forEach(strings, System.out::println);
        forEach(numbers, System.out::println);
    }

    private static <T> void forEach(List<T> list, Consumer<T> consumer) {
        for (T e : list) {
            consumer.accept(e);
        }
    }
}
