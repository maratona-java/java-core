package src.academy.devdojo.maratona.java.javaintermediario.Bcomportamento.interfaces;

import src.academy.devdojo.maratona.java.javaintermediario.Bcomportamento.domain.Car;

public interface CarPredicate {
    boolean test(Car car);
}
