package src.academy.devdojo.maratona.java.javaintermediario.Bcomportamento.test;

import src.academy.devdojo.maratona.java.javaintermediario.Bcomportamento.domain.Car;
import src.academy.devdojo.maratona.java.javaintermediario.Bcomportamento.interfaces.CarPredicate;

import java.util.ArrayList;
import java.util.List;

public class ComportamentoPorParametroTest02 {
    private static List<Car> cars = List.of(new Car("green", 2011), new Car("black", 1998), new Car("red", 2019));

    public static void main(String[] args) {
        List<Car> filterCars = filter(cars, car -> car.getColor().equals("red"));
        List<Car> filterBeforeCars = filter(cars, car -> car.getYear() < 2015);
        System.out.println(filterCars);
    }

   private static List<Car> filter(List<Car> cars, CarPredicate carPredicate) {
       List<Car> filteredCar = new ArrayList<>();
       for (Car car : cars) {
           if (carPredicate.test(car)) {
               filteredCar.add(car);
           }
       }
       return filteredCar;
   }
}
