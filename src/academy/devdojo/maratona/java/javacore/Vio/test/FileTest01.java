package src.academy.devdojo.maratona.java.javacore.Vio.test;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;

public class FileTest01 {
    public static void main(String[] args) {
        File file = new File("tmp/tile.txt");

        createNewFile(file);

//        if (deleteFile(file)) {
//            System.out.println("Arquivo deletado com sucesso");
//        } else {
//            System.out.println("Nenhum arquivo encontrado");
//        }
    }

    private static void createNewFile(File file) {
        try {
            boolean isCreated = file.createNewFile();
            System.out.println(isCreated);
            System.out.println("path: "+file.getPath());
            System.out.println("path absolute: "+file.getAbsolutePath());
            System.out.println("is directory: "+file.isDirectory());
            System.out.println("is file: "+file.isFile());
            System.out.println("is Hidden: "+file.isHidden()); // é oculto
            System.out.println("last modified: "+ Instant.ofEpochMilli(file.lastModified()).atZone(ZoneId.systemDefault()).toLocalDateTime());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Boolean deleteFile(File file) {
        if (file.exists()) {
             return file.delete();
        }
        return false;
    }
}
