package src.academy.devdojo.maratona.java.javacore.Vio.test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterTest01 {
    public static void main(String[] args) {
        File file = new File("tmp/file2.txt");

        try (FileWriter fw = new FileWriter(file, true)) {
            fw.write("O DevDojo é o melhor curso de Java\nContinuando na próxima linha");
            // Existe a possiblidade de fecharmos o nosso arquivo porém ainda tem coisas dentro do buffer e como ele será fechado automaticamente já que estamos utilizando o try with resources, então usamos o flush
            fw.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
