package src.academy.devdojo.maratona.java.javacore.Vio.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class BufferedReaderTest01 {
    public static void main(String[] args) {
        File file = new File("tmp/file2.txt");

        try (FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr)) {

            String line;
            while((line = br.readLine()) != null) {
                System.out.println(line);
            }
            br.readLine(); // Lê uma linha inteira

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
