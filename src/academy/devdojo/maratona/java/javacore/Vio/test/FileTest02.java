package src.academy.devdojo.maratona.java.javacore.Vio.test;

import java.io.File;
import java.io.IOException;

public class FileTest02 {
    public static void main(String[] args) throws IOException {
        File fileDirectory = new File("tmp/pasta");
        boolean isDirectorCreated =  fileDirectory.mkdir();
        System.out.println(isDirectorCreated);

        File fileArquivoDirectory = new File(fileDirectory, "arquivo.txt");
        boolean isFileCreated = fileArquivoDirectory.createNewFile();
        System.out.println(isFileCreated);

        // Rename file
        File fileRenamed = new File(fileDirectory, "arquivo_renomeado.txt");
        boolean isRenamed = fileArquivoDirectory.renameTo(fileRenamed);
        System.out.println(isRenamed);

        File directoryRenamed = new File("tmp/pasta2");
        boolean isDirectoryRenamed = fileDirectory.renameTo(directoryRenamed);
        System.out.println(isDirectoryRenamed);
    }
}
