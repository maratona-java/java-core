package src.academy.devdojo.maratona.java.javacore.Vio.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class BufferedWriterTest01 {
    public static void main(String[] args) {
        File file = new File("tmp/file3.txt");

        try (FileWriter fw = new FileWriter(file, true);
             BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write("O DevDojo é o melhor curso de Java");
            bw.newLine(); // Pega o line separator do SO
            bw.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
