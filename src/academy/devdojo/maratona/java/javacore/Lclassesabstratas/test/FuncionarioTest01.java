package src.academy.devdojo.maratona.java.javacore.Lclassesabstratas.test;

import src.academy.devdojo.maratona.java.javacore.Lclassesabstratas.domain.Desenvolvedor;
import src.academy.devdojo.maratona.java.javacore.Lclassesabstratas.domain.Gerente;

public class FuncionarioTest01 {
    public static void main(String[] args) {
        Gerente gerente = new Gerente("Nami", 5000);
        Desenvolvedor desenvolvedor = new Desenvolvedor("Touya", 12000);

        System.out.println(gerente);
        System.out.println(desenvolvedor);
    }
}
