package src.academy.devdojo.maratona.java.javacore.Qstrings;

public class StringBuilderTest01 {
    public static void main(String[] args) {
        String nome = "André Luiz";
        nome.concat(" Devdojo");
        System.out.println(nome);
        StringBuilder sb = new StringBuilder("André Luiz");
        sb.append(" DevDojo").append(" Academy");
        System.out.println(sb);
        sb.reverse();
        System.out.println(sb);
        sb.delete(0,3);
        System.out.println(sb);

    }
}
