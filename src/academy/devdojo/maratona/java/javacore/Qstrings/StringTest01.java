package src.academy.devdojo.maratona.java.javacore.Qstrings;

public class StringTest01 {
    public static void main(String[] args) {
        String nome = "André"; //String constant pool
        String nome2 = "André";
        nome = nome.concat("Luiz"); // = nome += "Luiz"

        // Verifica se as string fazem referência para o mesmo local na memória
        System.out.println(nome == nome2);
        String nome3 = new String("André"); // 1 variavel de referẽncia, 2 objeto do tipo string, 3 uma string no pool de string
        System.out.println(nome == nome3); // false, porque está em outro loca e não no pool de Strings
    }
}
