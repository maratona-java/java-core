package src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.domain;

import java.text.NumberFormat;

public class Funcionario {
    private String nome;
    private int idade;
    private double[] salarios;
    private double media = 0;

    public void imprimir() {
        System.out.println("Nome: "+ this.nome + "\n");
        System.out.println("Idade: "+ this.idade + "\n");

        System.out.println("Salaŕios: \n");
        for (double salario: salarios) {
            System.out.println("- R$ " + NumberFormat.getCurrencyInstance().format(salario)+"\n");
        }
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public void setSalarios(double[] salarios) {
        this.salarios = salarios;
    }


    public String getNome() {
        return nome;
    }

    public int getIdade() {
        return idade;
    }

    public double[] getSalarios() {
        return salarios;
    }

    public double getMedia() {
        return media;
    }

    public void mediaSalarial() {
        double total = 0;

        for (double salario: this.salarios) {
            total += salario;
        }

        double mediaSalarial = total / this.salarios.length;

        System.out.println("Média Salarial: R$ " + NumberFormat.getCurrencyInstance().format(mediaSalarial));
    }
}
