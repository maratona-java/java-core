package src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.domain;

public class Pessoa {
    private String nome;
    private int idade;

    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @param idade
     */
    public void setIdade(int idade) throws Exception {
        if (idade < 0) {
            throw new Exception("Idade inválida!");
        }
        this.idade = idade;
    }

    public String getNome() {
        return this.nome;
    }

    public int getIdade() {
        return this.idade;
    }

    public void imprime() {
        System.out.println(this.nome);
        System.out.println(this.idade);
    }
}
