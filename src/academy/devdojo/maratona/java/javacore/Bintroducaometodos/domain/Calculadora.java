package src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.domain;

public class Calculadora {

    public void somaDoisNumeros() {
        System.out.println(10 + 10);
    }

    public void subtraiDoisNumeros(){
        System.out.println(20-10);
    }

    public void multiplicaDoisNumeros(int num1, int num2) {
        System.out.println(num1 * num2);
    }

    public double divideDoisNumeros(double num1, double num2) {
        return num1 / num2;
    }

    public Boolean isPar(int num1) {
        return num1 % 2 == 0;
    }

    public void alteraDoisNumeros(int num1, int num2) {
        num1 = 99;
        num2 = 33;
        System.out.println("Dentro do alteraDoisNumeros");
        System.out.println("Num1 "+ num1);
        System.out.println("Num2 "+ num2);
    }

    public void somaArray(int[] numeros) {
        int soma = 0;
        for (int num:numeros) {
            soma += num;
        }
        System.out.println(soma);
    }

    public void somaVarArgs(int ...numeros) { // deve ser o ultimo parametro
        int soma = 0;
        for (int num:numeros) {
            soma += num;
        }
        System.out.println(soma);
    }
}
