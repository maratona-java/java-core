package src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.test;

import src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.domain.Estudante;

public class EstudanteTest02 {
    public static void main(String[] args) {
        Estudante estudante01 = new Estudante();
        Estudante estudante02 = new Estudante();

        estudante01.nome = "Goku";
        estudante01.idade = 15;
        estudante01.sexo = 'M';

        estudante02.nome = "Sakura";
        estudante02.idade = 25;
        estudante02.sexo = 'F';

        estudante01.imprime();
        estudante02.imprime();
    }
}
