package src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.test;

import src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.domain.Estudante;
import src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.domain.ImpressoraEstudante;

public class EstudanteTest01 {
    public static void main(String[] args) {
        Estudante estudante01 = new Estudante();
        Estudante estudante02 = new Estudante();

        estudante01.nome = "Goku";
        estudante01.idade = 15;
        estudante01.sexo = 'M';

        estudante02.nome = "Sakura";
        estudante02.idade = 25;
        estudante02.sexo = 'F';

        ImpressoraEstudante impressora = new ImpressoraEstudante();

        impressora.imprime(estudante01); // Passando a referência do objeto e não a cópia como em tipos primitivos
        impressora.imprime(estudante02);
    }
}
