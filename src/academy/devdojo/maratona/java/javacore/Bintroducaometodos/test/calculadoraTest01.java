package src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.test;

import src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.domain.Calculadora;

public class calculadoraTest01 {
    public static void main(String[] args) {
        Calculadora calculadora = new Calculadora();
        calculadora.somaDoisNumeros();
        System.out.println("Finalizando calculadora");

        calculadora.multiplicaDoisNumeros(100, 2);
        System.out.println(calculadora.divideDoisNumeros(100, 2));
        System.out.println("Par: "+ calculadora.isPar(11));

        System.out.println("-------------------------------------");

        int num1 = 1;
        int num2 = 2;

        calculadora.alteraDoisNumeros(num1, num2); // Quando passamos variáveis do tipo primitivo sempre uma cópia é enviada
    }
}
