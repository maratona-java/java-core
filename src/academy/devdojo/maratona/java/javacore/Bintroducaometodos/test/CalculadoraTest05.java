package src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.test;

import src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.domain.Calculadora;

public class CalculadoraTest05 {
    public static void main(String[] args) {
        Calculadora calculadora = new Calculadora();
        int[] numeros = {1,2,3,4,5};
        calculadora.somaArray(numeros);
        calculadora.somaVarArgs(1,2,3,4,5,6); // Por baixo dos panos será transformado em array
    }
}
