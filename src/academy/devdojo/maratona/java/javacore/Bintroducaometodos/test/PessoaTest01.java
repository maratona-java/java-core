package src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.test;

import src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.domain.Pessoa;

public class PessoaTest01 {
    public static void main(String[] args) {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Marcos");
        try {
            pessoa.setIdade(-10);
            pessoa.imprime();
        }catch (Exception e) {
            System.out.println("Erro: "+ e);
        }

        System.out.println("O nome da pessoa é: "+ pessoa.getNome());
    }
}
