package src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.test;

import src.academy.devdojo.maratona.java.javacore.Bintroducaometodos.domain.Funcionario;

/**
 * Crie uma classe Funcionário com os seguintes atributos
 *
 * nome
 * idade
 * salario // três salário devem ser guardados
 *
 * Crie dois métodos
 *
 * 1. Para imprimir os dados
 * 2. Para tirar a média dos salários e imprimir o resultado.
 */
public class FuncionarioTest01 {
    public static void main(String[] args) {
        Funcionario funcionario = new Funcionario();
        funcionario.setNome("André");
        funcionario.setIdade(25);
        funcionario.setSalarios(new double[]{1200, 987.32, 2000});

        funcionario.imprimir();
        funcionario.mediaSalarial();
    }
}
