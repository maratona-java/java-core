package src.academy.devdojo.maratona.java.javacore.Gexercicio.domain;

public class Seminario {
    private String titulo;
    private Aluno[] alunos;
    private Professor professor;
    private Local local;

    public Seminario(String titulo) {
        this.titulo = titulo;
    }

    public void imprimeInformacoes() {
        System.out.println("Seminário: "+ this.titulo);
        if (professor !=  null) {
            System.out.println("Professor: "+this.professor.getNome());
        }
        if (local != null) {
            System.out.println("Local: "+ this.getLocal().getEndereco());
        }
        if (alunos != null) {
            System.out.println("\nAlunos: \n");
            for (Aluno aluno: this.alunos) {
                System.out.println("Nome: "+ aluno.getNome());
            }
        } else {
            System.out.println("\nNenhum aluno cadastrado!");
        }
    }

    public void setNome(String titulo) {
        this.titulo = titulo;
    }

    public String getNome() {
        return this.titulo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Aluno[] getAlunos() {
        return alunos;
    }

    public void setAluno(Aluno[] aluno) {
        this.alunos= aluno;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }
}
