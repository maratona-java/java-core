package src.academy.devdojo.maratona.java.javacore.Gexercicio.test;

import src.academy.devdojo.maratona.java.javacore.Gexercicio.domain.Aluno;
import src.academy.devdojo.maratona.java.javacore.Gexercicio.domain.Local;
import src.academy.devdojo.maratona.java.javacore.Gexercicio.domain.Professor;
import src.academy.devdojo.maratona.java.javacore.Gexercicio.domain.Seminario;

/**
 * Crie um sistema que gerencie seminários
 *
 * O sistema deverá cadastrar seminários, estudantes, professores e local onde será realizado
 *
 * Um aluno poderá estar em apenas um seminário
 * Um seminário poderá ter nenhum ou vários alunos
 * Um professor proderá ministrar cários seminários
 * Um seminário deve ter um local
 *
 * Campo básicos (exlcuindo relacionamento)
 * seminário: titulo
 * aluno: nome e idade
 * professor: nome, especialidade
 * local: endereço
 *
 */
public class ExercicioTest01 {
    public static void main(String[] args) {

        Aluno aluno = new Aluno("André Luiz", 30);
        Aluno aluno2 = new Aluno("Joao Silva", 40);
        Aluno aluno3 = new Aluno("Maria Silva", 60);

        Professor professor = new Professor("William Suane", "JAVA");
        Seminario seminario = new Seminario("Como aprender Java");
        Local local = new Local("Av. João josé de Oliveira");


        Aluno[] alunos = {aluno, aluno2, aluno3};
        seminario.setAluno(alunos);
        seminario.setProfessor(professor);


        seminario.imprimeInformacoes();
    }
}
