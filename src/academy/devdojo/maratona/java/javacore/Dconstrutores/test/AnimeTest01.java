package src.academy.devdojo.maratona.java.javacore.Dconstrutores.test;

import src.academy.devdojo.maratona.java.javacore.Dconstrutores.domain.Anime;

public class AnimeTest01 {
    public static void main(String[] args) {
        Anime anime = new Anime("Akudama Drive", "TV", 105, "Ação", "production IG");
        Anime anime2 = new Anime("Dragon Ball Z");

        anime.imprime();
        anime2.imprime();
    }
}
