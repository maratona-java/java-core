package src.academy.devdojo.maratona.java.javacore.Jmodificarfinal.test;

import src.academy.devdojo.maratona.java.javacore.Jmodificarfinal.domain.Carro;
import src.academy.devdojo.maratona.java.javacore.Jmodificarfinal.domain.Comprador;

public class CarroTest01 {
    public static void main(String[] args) {
        Carro carro = new Carro();
        Comprador comprador = new Comprador();

        System.out.println(Carro.VELOCIDADE_LIMITE);
        System.out.println(carro.COMPRADOR);
        carro.COMPRADOR.setNome("Kuririn");

        System.out.println(carro.COMPRADOR);
        carro.imprime();
    }
}
