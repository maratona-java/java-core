package src.academy.devdojo.maratona.java.javacore.Wnio.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class PathTest02 {
    public static void main(String[] args) throws IOException {
        // Pega o diretório que estamos
        Path pastapath = Paths.get("tmp/pasta");

        // Se a pasta já existe ele lança exception
        if (Files.notExists(pastapath)) {
            Path pastaDirectory = Files.createDirectory(pastapath);
        }

        // Neste caso não lança exception caso o diretório já exista!!!
        Path subPatasPaths = Paths.get("tmp/pasta3/subpasta/susubpasta");
        Path subPastaDirectory = Files.createDirectories(subPatasPaths);


        Path filePath = Paths.get(subPatasPaths.toString(), "file2.txt");
        if (Files.notExists(pastapath)) {
            Path filePathCreated = Files.createFile(filePath);
        }

        // Copy file

        Path source = filePath;
        Path target = Paths.get(filePath.getParent().toString(), "file_renamed.txt");
        Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
    }
}
