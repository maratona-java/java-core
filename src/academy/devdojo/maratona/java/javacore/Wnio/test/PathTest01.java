package src.academy.devdojo.maratona.java.javacore.Wnio.test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathTest01 {
    public static void main(String[] args) {
        Path p1 = Paths.get("tmp/file2.txt");
        System.out.println(p1.getFileName());
        System.out.println(p1.getFileName().toFile());

        Path p2 = Paths.get("tmp","file2.txt");
        System.out.println(p2.toFile());
        System.out.println(p2.getFileName());
    }
}
