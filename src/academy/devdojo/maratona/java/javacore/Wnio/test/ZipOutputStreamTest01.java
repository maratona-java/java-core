package src.academy.devdojo.maratona.java.javacore.Wnio.test;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipOutputStreamTest01 {
    public static void main(String[] args) {
        Path arquivoZip = Paths.get("tmp/pasta/arquivo.zip");
        Path aruivosParaZipar = Paths.get("tmp/pasta");
        zip(arquivoZip, aruivosParaZipar);
    }

    private static void zip(Path arquivoZip, Path arquivosParaZipar) {
        try(ZipOutputStream zipstream = new ZipOutputStream(Files.newOutputStream(arquivoZip));
            DirectoryStream<Path> directoryStream = Files.newDirectoryStream(arquivosParaZipar)) {

            for (Path file : directoryStream) {
                ZipEntry zipEntry = new ZipEntry(file.getFileName().toString());
                zipstream.putNextEntry(zipEntry);
                Files.copy(file, zipstream);
                zipstream.closeEntry();
            }

            System.out.println("Arquivo criado com sucesso");

        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
