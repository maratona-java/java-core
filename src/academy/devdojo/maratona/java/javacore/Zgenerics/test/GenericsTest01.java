package src.academy.devdojo.maratona.java.javacore.Zgenerics.test;

import src.academy.devdojo.maratona.java.javacore.Ycolecoes.domain.Consumidor;

import java.util.ArrayList;
import java.util.List;

public class GenericsTest01 {
    public static void main(String[] args) {
       // Type erasure
       List<String> lista = new ArrayList<>();
       lista.add("Goku");
       lista.add("Vegeta");
       lista.add("Kuririm");

       for (String l: lista) {
           System.out.println(l);
       }

       add(lista, new Consumidor("Midoriya"));

       // Problema
       for (String l: lista) {
            System.out.println(l);
        }
    }

    private static void add(List lista, Consumidor consumidor) {
        lista.add(consumidor);
    }
}
