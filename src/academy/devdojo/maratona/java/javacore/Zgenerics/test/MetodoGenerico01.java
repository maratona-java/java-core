package src.academy.devdojo.maratona.java.javacore.Zgenerics.test;

import src.academy.devdojo.maratona.java.javacore.Zgenerics.domain.Barco;

import java.util.List;

public class MetodoGenerico01 {
    public static void main(String[] args) {
        List<Barco> barcoList = criarArrayComUmObjeto(new Barco("Canoa Marota"));
        System.out.println(barcoList);
    }

    private static <T> List<T> criarArrayComUmObjeto(T t) {
        List<T> list = List.of(t);
        return list;
    }
}