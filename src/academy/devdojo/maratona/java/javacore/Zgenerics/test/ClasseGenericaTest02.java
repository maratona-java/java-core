package src.academy.devdojo.maratona.java.javacore.Zgenerics.test;

import src.academy.devdojo.maratona.java.javacore.Zgenerics.domain.Barco;
import src.academy.devdojo.maratona.java.javacore.Zgenerics.services.BarcoRentavelService;


public class ClasseGenericaTest02 {
    public static void main(String[] args) {
        BarcoRentavelService barcoRentavelService = new BarcoRentavelService();
        Barco barco = barcoRentavelService.buscarbarcoDisponivel();
        System.out.println("Usando carro por um mês...");
        barcoRentavelService.retornarCarcoAlugado(barco);
    }
}
