package src.academy.devdojo.maratona.java.javacore.Zgenerics.test;

import src.academy.devdojo.maratona.java.javacore.Zgenerics.domain.Barco;
import src.academy.devdojo.maratona.java.javacore.Zgenerics.domain.Carro;
import src.academy.devdojo.maratona.java.javacore.Zgenerics.services.RentalService;

import java.util.ArrayList;
import java.util.List;

public class ClasseGenericaTest03 {
    public static void main(String[] args) {
        List<Carro> carrosDisponiveis = new ArrayList<>(List.of(new Carro("BMW"), new Carro("Fusca")));
        List<Barco> barcosDisponiveis = new ArrayList<>(List.of(new Barco("Lancha"), new Barco("Canoa")));

        RentalService<Carro> rentalService = new RentalService<>(carrosDisponiveis);
        Carro carro = rentalService.buscarObjetoDisponivel();
        System.out.println("Usando carro por um mês...");
        rentalService.retornarObjetoAlugado(carro);

        System.out.println("----------------------------");

        RentalService<Barco> rentalService1 = new RentalService<>(barcosDisponiveis);
        Barco barco = rentalService1.buscarObjetoDisponivel();
        System.out.println("Usando carro por um mês...");
        rentalService1.retornarObjetoAlugado(barco);
    }
}
