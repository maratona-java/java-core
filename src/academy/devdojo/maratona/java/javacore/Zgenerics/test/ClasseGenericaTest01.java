package src.academy.devdojo.maratona.java.javacore.Zgenerics.test;

import src.academy.devdojo.maratona.java.javacore.Zgenerics.domain.Carro;
import src.academy.devdojo.maratona.java.javacore.Zgenerics.services.CarroRentavelService;

public class ClasseGenericaTest01 {
    public static void main(String[] args) {
        CarroRentavelService carroRentavelService = new CarroRentavelService();
        Carro carro = carroRentavelService.buscarCarroDisponivel();
        System.out.println("Usando carro por um mês...");
        carroRentavelService.retornarCarroAlugado(carro);
    }
}
