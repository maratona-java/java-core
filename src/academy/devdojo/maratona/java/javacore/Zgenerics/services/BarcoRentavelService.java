package src.academy.devdojo.maratona.java.javacore.Zgenerics.services;

import src.academy.devdojo.maratona.java.javacore.Zgenerics.domain.Barco;
import src.academy.devdojo.maratona.java.javacore.Zgenerics.domain.Carro;

import java.util.ArrayList;
import java.util.List;

public class BarcoRentavelService {
    private List<Barco> barcosDisponiveis = new ArrayList<>(List.of(new Barco("Lanche"), new Barco("Canoa")));

    public Barco buscarbarcoDisponivel() {
        System.out.println("Buscando barco disponível...");
        Barco barco = barcosDisponiveis.remove(0);
        System.out.println("Alugando barco: "+ barco);
        System.out.println("Barcos disponíveis para alugar: ");
        System.out.println(barcosDisponiveis);
        return barco;
    }

    public void retornarCarcoAlugado(Barco barco) {
        System.out.println("Devolvendo barco "+ barco);
        barcosDisponiveis.add(barco);
        System.out.println("Barcos disponíveis ");
        System.out.println(barcosDisponiveis);
    }
}
