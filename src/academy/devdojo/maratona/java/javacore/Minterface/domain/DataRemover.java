package src.academy.devdojo.maratona.java.javacore.Minterface.domain;

public interface DataRemover {
    void remove();
}
