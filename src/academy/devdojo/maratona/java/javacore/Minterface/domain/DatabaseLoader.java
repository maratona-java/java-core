package src.academy.devdojo.maratona.java.javacore.Minterface.domain;

public class DatabaseLoader implements DataLoader, DataRemover {

    public static final int MAX_DATA_SIZE = 10;

    @Override
    public void load() {
        System.out.println("Carregando dados do banco");
    }

    @Override
    public void remove() {
        System.out.println("Removendo do banco de dados");
    }

    // Não pode ser sobrescrito
    static void retrieveMaxDataSize() {
        System.out.println("Dentro do retrieveMaxDataSize na interface");
    }
}
