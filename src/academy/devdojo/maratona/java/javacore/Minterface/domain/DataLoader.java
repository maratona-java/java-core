package src.academy.devdojo.maratona.java.javacore.Minterface.domain;

public interface DataLoader {
    void load();

    default void checkPermission() {
        System.out.println("Fazendo checagem de permissões");
    }
}
