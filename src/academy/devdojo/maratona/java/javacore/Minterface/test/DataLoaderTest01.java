package src.academy.devdojo.maratona.java.javacore.Minterface.test;

import src.academy.devdojo.maratona.java.javacore.Minterface.domain.DatabaseLoader;
import src.academy.devdojo.maratona.java.javacore.Minterface.domain.FileLoader;

public class DataLoaderTest01 {
    public static void main(String[] args) {
        DatabaseLoader databaseLoader = new DatabaseLoader();
        FileLoader fileLoader = new FileLoader();

        databaseLoader.load();
        fileLoader.load();

        System.out.println("-------------------------");

        databaseLoader.remove();
        fileLoader.remove();

        databaseLoader.checkPermission();
        fileLoader.checkPermission();
    }
}
