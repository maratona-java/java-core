package src.academy.devdojo.maratona.java.javacore.Aintroducaoclasses.test;

import src.academy.devdojo.maratona.java.javacore.Aintroducaoclasses.domain.Professor;

public class ProfessorTest01 {
    public static void main(String[] args) {
        Professor professor = new Professor();
        professor.nome = "José";
        professor.idade = 200;
        professor.sexo = 'M';

        System.out.println("Nome: " + professor.nome + " Idade: "+ professor.idade + " Sexo: " + professor.sexo);
    }
}
