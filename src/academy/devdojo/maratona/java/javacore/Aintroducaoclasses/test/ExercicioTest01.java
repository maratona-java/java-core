package src.academy.devdojo.maratona.java.javacore.Aintroducaoclasses.test;

import src.academy.devdojo.maratona.java.javacore.Aintroducaoclasses.domain.Carro;

/**
 * Crie uma classe carro com os seguintes atributos
 *
 * Nome
 * Modelo
 * ano
 *
 * Em seguida, crie dois objetos distintos e imprima seus valores
 */
public class ExercicioTest01 {
    public static void main(String[] args) {
        Carro carro = new Carro();
        Carro carro2 = new Carro();

        carro.nome = "Fusca";
        carro.Modelo = "Esporte";
        carro.ano = 1987;

        carro2.nome = "Mustang";
        carro2.Modelo = "GT 500";
        carro2.ano = 1968;

        System.out.println(carro.nome + " " + carro.Modelo + " " + carro.ano);
        System.out.println(carro2.nome + " " + carro2.Modelo + " " + carro2.ano);
    }
}
