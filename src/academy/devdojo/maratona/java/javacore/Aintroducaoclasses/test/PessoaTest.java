package src.academy.devdojo.maratona.java.javacore.Aintroducaoclasses.test;

import src.academy.devdojo.maratona.java.javacore.Aintroducaoclasses.domain.Estudante;

public class PessoaTest {
    public static void main(String[] args) {
        Estudante estudante = new Estudante();
        estudante.nome = "paulo";
        estudante.idade = 35;
        estudante.sexo = 'M';

        System.out.println(estudante.nome);
        System.out.println(estudante.idade);
        System.out.println(estudante.sexo);
    }
}
