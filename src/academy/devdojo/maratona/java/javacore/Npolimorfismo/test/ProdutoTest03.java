package src.academy.devdojo.maratona.java.javacore.Npolimorfismo.test;

import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain.Computador;
import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain.Produto;
import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain.Tomate;
import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.service.CalculadoraImposto;

public class ProdutoTest03 {
    public static void main(String[] args) {
        Produto produto = new Computador("Ryzen 9", 3000);

        Tomate tomate = new Tomate("Tomate verde", 10);
        tomate.setDatValidade("11/12/2025");

        CalculadoraImposto.calcularImposto(tomate);

    }
}
