package src.academy.devdojo.maratona.java.javacore.Npolimorfismo.test;

import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.repository.RepositoryInterface;
import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.service.DatabaseRepository;

public class RepositoryTest {
    public static void main(String[] args) {
        RepositoryInterface repository = new DatabaseRepository();
        repository.save();
    }
}
