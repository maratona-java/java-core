package src.academy.devdojo.maratona.java.javacore.Npolimorfismo.test;

import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain.Computador;
import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain.Televisao;
import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain.Tomate;
import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.service.CalculadoraImposto;

public class ProdutoTest01 {
    public static void main(String[] args) {
        Computador computador = new Computador("NUC10i7", 11000);
        Tomate tomate = new Tomate("Tomate verde", 10);
        Televisao televisao = new Televisao("Samsung 60\"", 4100);

        CalculadoraImposto.calcularImposto(computador);
        System.out.println("----------------------------------");
        CalculadoraImposto.calcularImposto(tomate);
        System.out.println("----------------------------------");
        CalculadoraImposto.calcularImposto(televisao);
    }
}
