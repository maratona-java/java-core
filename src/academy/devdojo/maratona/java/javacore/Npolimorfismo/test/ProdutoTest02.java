package src.academy.devdojo.maratona.java.javacore.Npolimorfismo.test;

import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain.Computador;
import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain.Produto;
import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain.Tomate;

public class ProdutoTest02 {
    public static void main(String[] args) {
        Produto produto = new Computador("Ryzen 9", 3000);
        System.out.println("Nome: "+ produto.getNome());
        System.out.println("Valor: "+produto.getValor());
        System.out.println("Imposto:"+ produto.calcularImposto());

        System.out.println("=======================================");

        Produto produto2 = new Tomate("Tomate verde", 10);
        System.out.println("Nome: "+ produto2.getNome());
        System.out.println("Valor: "+produto2.getValor());
        System.out.println("Imposto:"+ produto2.calcularImposto());
    }
}
