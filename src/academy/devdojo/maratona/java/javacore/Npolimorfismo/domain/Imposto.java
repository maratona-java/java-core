package src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain;

public interface Imposto {
    public double calcularImposto();
}
