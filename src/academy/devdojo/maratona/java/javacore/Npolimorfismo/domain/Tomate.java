package src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain;

public class Tomate extends Produto{
    public static final double IMPOSTO_POR_CENTO = 0.06;

    private String datValidade;
    public Tomate(String nome, double valor) {
        super(nome, valor);
    }

    @Override
    public double calcularImposto() {
        return this.valor * IMPOSTO_POR_CENTO;
    }

    public String getDatValidade() {
        return datValidade;
    }

    public void setDatValidade(String datValidade) {
        this.datValidade = datValidade;
    }
}
