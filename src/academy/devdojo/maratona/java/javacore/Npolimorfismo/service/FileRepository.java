package src.academy.devdojo.maratona.java.javacore.Npolimorfismo.service;

import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.repository.RepositoryInterface;

public class FileRepository implements RepositoryInterface {
    @Override
    public void save() {
        System.out.println("Salvando em um arquivo");
    }
}
