package src.academy.devdojo.maratona.java.javacore.Npolimorfismo.service;

import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain.Produto;
import src.academy.devdojo.maratona.java.javacore.Npolimorfismo.domain.Tomate;

import java.text.NumberFormat;

public class CalculadoraImposto {
    public static void calcularImposto(Produto produto) {
        System.out.println("Relatório de imposto");
        System.out.println("Produto: "+produto.getNome());
        System.out.println("Preço: "+NumberFormat.getCurrencyInstance().format(produto.getValor()));
        System.out.println("Produto: "+ NumberFormat.getCurrencyInstance().format(produto.calcularImposto()));

        if (produto instanceof Tomate) {
            Tomate tomate = (Tomate) produto;
            System.out.println(tomate.getDatValidade());
        }
    }
}
