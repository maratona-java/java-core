package src.academy.devdojo.maratona.java.javacore.Sformatacao.test;

import java.util.Locale;

public class LocaleTest02 {
    public static void main(String[] args) {
        System.out.println(Locale.getDefault());
        String[] isoCountires = Locale.getISOCountries();
        String[] isoLanguages = Locale.getISOLanguages();
        for (String isoLanguage : isoLanguages) {
            System.out.print(isoLanguage+ " ");
        }
        System.out.println();
        for (String isoCountry : isoCountires) {
            System.out.print(isoCountry+ " ");
        }
    }
}
