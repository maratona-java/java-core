package src.academy.devdojo.maratona.java.javacore.Xserializacao.domain;

public class Turma {
    private String nome;

    public Turma(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
