package src.academy.devdojo.maratona.java.javacore.Kenum.domain;

public enum TipoPagamento {
    DEBITO {
        @Override
        public double calcularDesconto(double valor) {
            return valor * 0.1;
        }
    }, CREDITO {
        @Override
        public double calcularDesconto(double valor) {
            return valor *0.05;
        }
    };

    // Método sem corpo, criado apenas para ser sobrescrito.
    public abstract double calcularDesconto(double valor);
}
