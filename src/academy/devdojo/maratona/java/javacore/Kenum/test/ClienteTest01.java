package src.academy.devdojo.maratona.java.javacore.Kenum.test;

import src.academy.devdojo.maratona.java.javacore.Kenum.domain.Cliente;
import src.academy.devdojo.maratona.java.javacore.Kenum.domain.TipoCliente;
import src.academy.devdojo.maratona.java.javacore.Kenum.domain.TipoPagamento;

public class ClienteTest01 {
    public static void main(String[] args) {
       Cliente cliente1 = new Cliente("Paulo", TipoCliente.PESSOA_FISICA, TipoPagamento.CREDITO);
       Cliente cliente2 = new Cliente("Marcos", TipoCliente.PESSOA_JURIDICA, TipoPagamento.CREDITO);

       System.out.println(cliente1);
       System.out.println(cliente2);
       System.out.println(TipoPagamento.DEBITO.calcularDesconto(100));

       //TipoCliente tipocliente = TipoCliente.valueOf("Pessoa Física");
       TipoCliente tipocliente = TipoCliente.TipoClientePorNomeRelatorio("Pessoa Física");


        System.out.println(tipocliente);

    }
}
