package src.academy.devdojo.maratona.java.javacore.Oexception.exception;

public class InvalidLoginException extends Exception {
    public InvalidLoginException() {
        super("Invalid Login");
    }

    public InvalidLoginException(String message) {
        super(message);
    }
}
