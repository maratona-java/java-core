package src.academy.devdojo.maratona.java.javacore.Oexception.exception.test;

import src.academy.devdojo.maratona.java.javacore.Oexception.exception.domain.Leitor1;
import src.academy.devdojo.maratona.java.javacore.Oexception.exception.domain.Leitor2;

import java.io.IOException;

public class LeitorTest01 {
    public static void main(String[] args) {
        lerArquivo();
    }

    public static void lerArquivo() {
        try (Leitor1 leitor1 = new Leitor1();
             Leitor2 leitor2 = new Leitor2()){

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
