package src.academy.devdojo.maratona.java.javacore.Oexception.exception.test;

import java.io.File;
import java.io.IOException;

public class ExceptionTest02 {
    public static void main(String[] args) {
        try {
            createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void createNewFile() throws IOException {
        File file = new File("tmp/teste.txt");

        try {
            boolean isCreated = file.createNewFile();
            System.out.println("Arquivo criado "+ isCreated);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
