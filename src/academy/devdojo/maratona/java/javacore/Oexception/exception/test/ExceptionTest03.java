package src.academy.devdojo.maratona.java.javacore.Oexception.exception.test;

import java.io.File;
import java.io.IOException;

public class ExceptionTest03 {
    public static void main(String[] args) throws IOException {
        try {
            createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createNewFile() throws IOException {
        File file = new File("tmp/teste.txt");

        try {
            boolean isCreated = file.createNewFile();
            System.out.println("Arquivo criado "+ isCreated);
        } catch (IOException e) { // Trata a exception
            e.printStackTrace();
            throw e; // Joga pra cima a exception ou cria uma outra exception
        }
    }
}
