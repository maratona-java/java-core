package src.academy.devdojo.maratona.java.javacore.Oexception.exception.domain;

import src.academy.devdojo.maratona.java.javacore.Oexception.exception.InvalidLoginException;

import java.io.FileNotFoundException;

public class Pessoa {
    public void salvar() throws InvalidLoginException, FileNotFoundException {
        System.out.println("salvando pessoa");
    }
}
