package src.academy.devdojo.maratona.java.javacore.Oexception.exception.test;

import src.academy.devdojo.maratona.java.javacore.Oexception.exception.InvalidLoginException;

import java.util.Scanner;

public class InvalidLoginExceptionTest01 {
    public static void main(String[] args) {
        try {
            login();
        }catch (InvalidLoginException e) {
            e.printStackTrace();
        }
    }

    private static void login() throws InvalidLoginException{
        Scanner keyboard = new Scanner(System.in);
        String username = "Goku";
        String password = "ssj";

        System.out.println("Usuário");
        String usernameReceived = keyboard.nextLine();
        System.out.println("Password");
        String passwordReceived = keyboard.nextLine();

        if (!username.equals(usernameReceived) || !(password.equals(password))) {
            throw new InvalidLoginException("User or password invalid!");
        }

        System.out.println("Usuário logado com sucesso!");
    }
}
