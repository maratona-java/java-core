package src.academy.devdojo.maratona.java.javacore.Oexception.exception.test;

import src.academy.devdojo.maratona.java.javacore.Oexception.exception.domain.Funcionario;
import src.academy.devdojo.maratona.java.javacore.Oexception.exception.domain.Pessoa;

public class SobrescritaComExceptionTest01 {
    public static void main(String[] args) {
        Pessoa pessoa = new Pessoa();
        Funcionario funcionario = new Funcionario();

        funcionario.salvar();
    }
}
