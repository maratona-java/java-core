package src.academy.devdojo.maratona.java.javacore.Oexception.runtime.test;
public class RuntimeExceptionTest03 {
    public static void main(String[] args) {
        try {
            System.out.println("Abrindo arquivo");
            System.out.println("Escrevendo dados no arquivo");

        } catch (Exception e) {
            e.printStackTrace();
        } finally { // Sempre será executado
            System.out.println("Fechando recurso liberado do sistema operacional");
        }
    }
}
