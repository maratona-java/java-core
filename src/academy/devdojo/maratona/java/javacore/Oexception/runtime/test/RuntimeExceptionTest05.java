package src.academy.devdojo.maratona.java.javacore.Oexception.runtime.test;

import java.io.FileNotFoundException;
import java.sql.SQLException;

public class RuntimeExceptionTest05 {
    public static void main(String[] args) {
        try {
            talvezLanceUmaException();
        } catch (SQLException | FileNotFoundException e) {
            System.out.println("SQL Exception");
        }
    }

    private static void talvezLanceUmaException() throws SQLException, FileNotFoundException {

    }
}
