package src.academy.devdojo.maratona.java.javacore.Oexception.runtime.test;

public class RuntimeExceptionTest02 {
    public static void main(String[] args) {
        try {
            int resultado = divisao(10, 0);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param a
     * @param b
     * @return
     * @throws IllegalArgumentException
     */
    private static int divisao(int a, int b) { // Informa que este método pode lançar uma exception
        if (b == 0) {
            throw new IllegalArgumentException("Não é possível fazer divisão por zero");
        }
        return a/b;
    }
}
