package src.academy.devdojo.maratona.java.javacore.Isobrescrita.test;

import src.academy.devdojo.maratona.java.javacore.Isobrescrita.domain.Anime;

public class AnimeTest01 {
    public static void main(String[] args) {
        Anime anime = new Anime("Dr. Stone");

        System.out.println(anime.toString());
    }
}
