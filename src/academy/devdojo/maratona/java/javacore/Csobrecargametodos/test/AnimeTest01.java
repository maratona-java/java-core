package src.academy.devdojo.maratona.java.javacore.Csobrecargametodos.test;

import src.academy.devdojo.maratona.java.javacore.Csobrecargametodos.domain.Anime;

public class AnimeTest01 {
    public static void main(String[] args) {
        Anime anime = new Anime();
        anime.init("Akudama Drive", "TV", 100);

        anime.imprime();
    }
}
