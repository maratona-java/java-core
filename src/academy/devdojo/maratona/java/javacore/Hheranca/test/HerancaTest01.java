package src.academy.devdojo.maratona.java.javacore.Hheranca.test;

import src.academy.devdojo.maratona.java.javacore.Hheranca.domain.Endereco;
import src.academy.devdojo.maratona.java.javacore.Hheranca.domain.Funcionario;
import src.academy.devdojo.maratona.java.javacore.Hheranca.domain.Pessoa;

public class HerancaTest01 {
    public static void main(String[] args) {
        Endereco endereco = new Endereco();
        endereco.setRua("Rua 3");
        endereco.setCep("012345298");

        Pessoa pessoa = new Pessoa("João Carlos");
        pessoa.setCpf("12345789");
        pessoa.setEndereco(endereco);
        pessoa.imprime();

        System.out.println("-----------------------------");

        Funcionario funcionario = new Funcionario("Paulo Silva");
        funcionario.setCpf("22222");
        funcionario.setEndereco(endereco);
        funcionario.setSalario(2000);

        funcionario.imprime();

        funcionario.relatorioPagamento();
    }
}
