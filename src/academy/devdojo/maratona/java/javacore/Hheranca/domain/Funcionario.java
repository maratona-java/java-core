package src.academy.devdojo.maratona.java.javacore.Hheranca.domain;

public class Funcionario extends Pessoa {

    private double salario;

    public Funcionario(String nome) {
        super(nome);
    }

    public double getSalario() {
        return salario;
    }

    public void imprime() {
       super.imprime();
       System.out.println("Salário: "+ this.getSalario());
    }

    public void relatorioPagamento() {
        System.out.println("Eu "+ this.nome);
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
}
