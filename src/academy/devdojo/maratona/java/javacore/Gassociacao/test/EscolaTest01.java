package src.academy.devdojo.maratona.java.javacore.Gassociacao.test;

import src.academy.devdojo.maratona.java.javacore.Gassociacao.domain.Professor;
import src.academy.devdojo.maratona.java.javacore.Gassociacao.domain.Escola;

public class EscolaTest01 {
    public static void main(String[] args) {
        Professor professor = new Professor("Jiraya Sensei");
        Professor[] professores = {professor};
        Escola escola = new Escola("Konoha", professores);

        escola.imprime();
    }
}
