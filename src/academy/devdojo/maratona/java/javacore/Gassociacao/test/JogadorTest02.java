package src.academy.devdojo.maratona.java.javacore.Gassociacao.test;

import src.academy.devdojo.maratona.java.javacore.Gassociacao.domain.Jogador;
import src.academy.devdojo.maratona.java.javacore.Gassociacao.domain.Time;

public class JogadorTest02 {
    public static void main(String[] args) {
        Jogador jogador1 = new Jogador("Pelé");
        jogador1.imprime();
        Time time = new Time("Brasil");

        jogador1.setTime(time);
        jogador1.imprime();
    }
}
