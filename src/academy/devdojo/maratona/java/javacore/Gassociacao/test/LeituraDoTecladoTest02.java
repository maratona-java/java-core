package src.academy.devdojo.maratona.java.javacore.Gassociacao.test;

import java.util.Scanner;

public class LeituraDoTecladoTest02 {
    public static void main(String[] args) {
        System.out.println("Software previsão do futuro");
        System.out.println("Digite sua pergunta e eu responderei sim ou não");

        Scanner input = new Scanner(System.in);

        String pergunta = input.nextLine();
        if (pergunta.charAt(0) == ' ') {
            System.out.println("SIM");
        } else {
            System.out.println("NÃO");
        }
    }
}
