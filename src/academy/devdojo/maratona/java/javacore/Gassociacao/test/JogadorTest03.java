package src.academy.devdojo.maratona.java.javacore.Gassociacao.test;

import src.academy.devdojo.maratona.java.javacore.Gassociacao.domain.Jogador;
import src.academy.devdojo.maratona.java.javacore.Gassociacao.domain.Time;

public class JogadorTest03 {
    public static void main(String[] args) {
        Jogador jogador1 = new Jogador("Cafú");
        Jogador jogador2 = new Jogador("Ronaldo");
        Jogador jogador3 = new Jogador("Romário");

        Jogador jogador4 = new Jogador("Messi");
        Jogador jogador5 = new Jogador("Di Maria");


        Time time =  new Time("Brasil");
        Time time2 = new Time("Argentina");

        Jogador[] jogadores = {jogador1, jogador2, jogador3};
        Jogador[] jogadores2 = {jogador4, jogador5};

        jogador1.setTime(time);
        jogador2.setTime(time);
        jogador3.setTime(time);

        jogador4.setTime(time2);
        jogador5.setTime(time2);

        time.setJogadores(jogadores);
        time2.setJogadores(jogadores2);

        time.imprime();

        System.out.println("----------------");

        time2.imprime();
    }
}
