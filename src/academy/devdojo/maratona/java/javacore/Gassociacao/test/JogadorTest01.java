package src.academy.devdojo.maratona.java.javacore.Gassociacao.test;

import src.academy.devdojo.maratona.java.javacore.Gassociacao.domain.Jogador;

public class JogadorTest01 {
    public static void main(String[] args) {
        Jogador joagador1 = new Jogador("Pelé");
        Jogador joagador2 = new Jogador("Romário");
        Jogador joagador3 = new Jogador("Cafú");

        Jogador[] jogadores = new Jogador[]{joagador1, joagador2, joagador3};

        for (Jogador jogador: jogadores) {
            jogador.imprime();
        }

    }
}
