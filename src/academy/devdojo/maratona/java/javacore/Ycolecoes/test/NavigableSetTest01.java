package src.academy.devdojo.maratona.java.javacore.Ycolecoes.test;

import src.academy.devdojo.maratona.java.javacore.Ycolecoes.domain.Manga;
import src.academy.devdojo.maratona.java.javacore.Ycolecoes.domain.Smartphone;

import java.util.*;

class SmartphoneMarca implements Comparator<Smartphone> {

    @Override
    public int compare(Smartphone o, Smartphone t1) {
        return o.getMarca().compareTo(t1.getMarca());
    }
}

class MangaPrecoComparator implements Comparator<Manga> {

    @Override
    public int compare(Manga manga, Manga t1) {
        return Double.compare(manga.getPreco(), t1.getPreco());
    }
}

public class NavigableSetTest01 {
    public static void main(String[] args) {
        NavigableSet<Smartphone> set = new TreeSet<>(new SmartphoneMarca());
        Smartphone smartphone = new Smartphone("123", "Nokia");
        set.add(smartphone);
        System.out.println(set);
        System.out.println("----------------------");

        NavigableSet<Manga> mangas =  new TreeSet<>(new MangaPrecoComparator());
        mangas.add(new Manga(6L, "Hellsing Ultimate", 2.99, 0));
        mangas.add(new Manga(8L, "Pokemon", 5.00, 1));
        mangas.add(new Manga(5L, "Berserk", 7.99, 0));
        mangas.add(new Manga(9L, "Dragon ball Z", 3.99, 1));
        mangas.add(new Manga(7L, "Attack on Titan", 80, 1));

        for (Manga manga : mangas) {
            System.out.println(manga);
        }

        System.out.println("--------------------------------");

        Manga yuyu = new Manga(21L, "Yuyu Hakusho", 8, 5);
        System.out.println(mangas.lower(yuyu));
    }
}
