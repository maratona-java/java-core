package src.academy.devdojo.maratona.java.javacore.Ycolecoes.test;

import src.academy.devdojo.maratona.java.javacore.Ycolecoes.domain.Manga;

import java.util.HashSet;
import java.util.Set;

public class SetTest01 {
    public static void main(String[] args) {
        Set<Manga> mangas = new HashSet<>();

        mangas.add(new Manga(6L, "Hellsing Ultimate", 2.99, 0));
        mangas.add(new Manga(8L, "Pokemon", 5.00, 1));
        mangas.add(new Manga(5L, "Berserk", 7.99, 0));
        mangas.add(new Manga(9L, "Dragon ball Z", 3.99, 1));
        mangas.add(new Manga(7L, "Attack on Titan", 80, 1));
        mangas.add(new Manga(7L, "Attack on Titan", 80, 1));

        for (Manga manga: mangas) {
            System.out.println(manga);
        }
    }
}
