package src.academy.devdojo.maratona.java.javacore.Ycolecoes.test;

import src.academy.devdojo.maratona.java.javacore.Ycolecoes.domain.Manga;

import java.util.LinkedList;
import java.util.List;

public class IteratorTest01 {
    public static void main(String[] args) {
        List<Manga> mangas = new LinkedList<>();

        mangas.add(new Manga(6L, "Hellsing Ultimate", 2.99, 0));
        mangas.add(new Manga(8L, "Pokemon", 5.00, 1));
        mangas.add(new Manga(5L, "Berserk", 7.99, 0));
        mangas.add(new Manga(9L, "Dragon ball Z", 3.99, 1));
        mangas.add(new Manga(7L, "Attack on Titan", 80, 1));

        // navega entre todos os mangas e remove os mangas em que a quantidade for == 0
        mangas.removeIf(manga -> manga.getQuantidade() == 0);

        System.out.println(mangas);
    }
}
