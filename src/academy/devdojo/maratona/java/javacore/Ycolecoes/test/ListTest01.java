package src.academy.devdojo.maratona.java.javacore.Ycolecoes.test;

import java.util.ArrayList;
import java.util.List;

public class ListTest01 {
    public static void main(String[] args) {
       List<String> nomes = new ArrayList<>();
       List<String> nomes2 = new ArrayList<>();

       nomes.add("André");
       nomes.add("DevDojo");

       nomes2.add("Paulo");
       nomes2.add("Silva");

       nomes.addAll(nomes2);

       for (String nome: nomes) {
           System.out.println(nome);
       }
       nomes.add("Paulo");

        System.out.println("--------------------------");

       for (int i=0; i < nomes.size(); i++) {
           System.out.println(nomes.get(i));
       }

       List<Integer> numeros = new ArrayList<>();
       numeros.add(100);
       numeros.add(200);
       numeros.add(300);

       for (int n: numeros) {
           System.out.println(n);
       }
    }
}
