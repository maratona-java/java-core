package src.academy.devdojo.maratona.java.javacore.Ycolecoes.test;

import src.academy.devdojo.maratona.java.javacore.Ycolecoes.domain.Manga;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BinarySearchTest02 {
    public static void main(String[] args) {
        MangaByIdComparator mangaByIdComparator = new MangaByIdComparator();
        List<Manga> mangas = new ArrayList<>(6);

        mangas.add(new Manga(6L, "Hellsing Ultimate", 9.5));
        mangas.add(new Manga(8L, "Pokemon", 11.20));
        mangas.add(new Manga(5L, "Berserk", 19.9));
        mangas.add(new Manga(9L, "Dragon ball Z", 2.99));
        mangas.add(new Manga(7L, "Attack on Titan", 3.2));

        System.out.println("Lista de Mangas ordenados por nome: ");
        mangas.sort(new MangaByIdComparator());
        for (Manga manga : mangas) {
            System.out.println(manga);
        }

        Manga mangaToSearch = new Manga(9L, "Dragon ball Z", 2.99);
        System.out.println(Collections.binarySearch(mangas, mangaToSearch, mangaByIdComparator));
    }
}
