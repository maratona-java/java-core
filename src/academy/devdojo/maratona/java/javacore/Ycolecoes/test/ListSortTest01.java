package src.academy.devdojo.maratona.java.javacore.Ycolecoes.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListSortTest01 {
    public static void main(String[] args) {
        List<String> mangas = new ArrayList<>(6);
        mangas.add("Berserk");
        mangas.add("Hellsing Ultimate");
        mangas.add("Attack on Titan");
        mangas.add("Pokemon");
        mangas.add("Dragon ball Z");

        Collections.sort(mangas); // Reorganiza a lista em ordem alfabética

        for (String manga : mangas) {
            System.out.println(manga);
        }

        System.out.println("------------------------");
        System.out.println("Salários:");

        List<Double> salarios = new ArrayList<>();
        salarios.add(100.21);
        salarios.add(23.98);
        salarios.add(21.21);
        salarios.add(98.10);

        Collections.sort(salarios);
        System.out.println(salarios);

        System.out.println("---------------------------");
    }
}
