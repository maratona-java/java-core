package src.academy.devdojo.maratona.java.javacore.Ycolecoes.test;

import src.academy.devdojo.maratona.java.javacore.Ycolecoes.domain.Manga;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class MangaByIdComparator implements Comparator<Manga> {
    @Override
    public int compare(Manga manga, Manga manga2) {
        return manga.getId().compareTo(manga2.getId());
    }
}

public class MangaSortTest01 {
    public static void main(String[] args) {
        List<Manga> mangas = new ArrayList<>(6);

        mangas.add(new Manga(6L, "Hellsing Ultimate", 9.5));
        mangas.add(new Manga(8L, "Pokemon", 11.20));
        mangas.add(new Manga(5L, "Berserk", 19.9));
        mangas.add(new Manga(9L, "Dragon ball Z", 2.99));
        mangas.add(new Manga(7L, "Attack on Titan", 3.2));

        System.out.println("Lista de Mangas: ");
        for (Manga manga : mangas) {
            System.out.println(manga);
        }

        System.out.println("Lista de Mangas ordenados:");
        Collections.sort(mangas);
        for (Manga manga : mangas) {
            System.out.println(manga);
        }

        System.out.println("Lista pelo id");
        Collections.sort(mangas, new MangaByIdComparator());
        for (Manga manga : mangas) {
            System.out.println(manga);
        }
    }
}
