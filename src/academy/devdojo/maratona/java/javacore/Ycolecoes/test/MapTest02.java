package src.academy.devdojo.maratona.java.javacore.Ycolecoes.test;

import src.academy.devdojo.maratona.java.javacore.Ycolecoes.domain.Consumidor;
import src.academy.devdojo.maratona.java.javacore.Ycolecoes.domain.Manga;

import java.util.HashMap;
import java.util.Map;

public class MapTest02 {
    public static void main(String[] args) {
        Consumidor consumidor1 = new Consumidor("André Luiz");
        Consumidor consumidor2 = new Consumidor("DevDojo Acedemy");

        Manga manga1 = new Manga(6L, "Hellsing Ultimate", 9.5);
        Manga manga2 = new Manga(8L, "Pokemon", 11.20);
        Manga manga3 = new Manga(5L, "Berserk", 19.9);
        Manga manga4 = new Manga(9L, "Dragon ball Z", 2.99);
        Manga manga5 = new Manga(7L, "Attack on Titan", 3.2);

        Map<Consumidor, Manga> consumidorManga = new HashMap<>();
        consumidorManga.put(consumidor1, manga1);
        consumidorManga.put(consumidor2, manga4);
        for (Map.Entry<Consumidor, Manga> entry : consumidorManga.entrySet()) {
            System.out.println(entry.getKey().getNome() + " - "+entry.getValue().getNome());
        }
    }
}
