package src.academy.devdojo.maratona.java.javacore.Ycolecoes.test;

import src.academy.devdojo.maratona.java.javacore.Ycolecoes.domain.Smartphone;

import java.util.ArrayList;
import java.util.List;

public class SmartphoneListTest01 {
    public static void main(String[] args) {
        Smartphone s1 = new Smartphone("1ABC1", "Iphone");
        Smartphone s2 = new Smartphone("22222", "Pixel");
        Smartphone s3 = new Smartphone("33333", "Samsung");

        List<Smartphone> smartphones = new ArrayList<>(6);
        smartphones.add(s1);
        smartphones.add(s2);
        smartphones.add(s3);

        for (Smartphone smartphone: smartphones) {
            System.out.println(smartphone);
        }

        Smartphone s4 = new Smartphone("22222", "Pixel");
        System.out.println(smartphones.contains(s4)); // Verifica através do serialNumber

        int indexSmartPhone = smartphones.indexOf(s4);//traz o indice do objeto que estamos passando se não encontrar retorna -1

        System.out.println("Smartphone encontrado:");
        System.out.println(smartphones.get(indexSmartPhone));
    }
}
