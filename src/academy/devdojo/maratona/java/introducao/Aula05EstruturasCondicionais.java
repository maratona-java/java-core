package src.academy.devdojo.maratona.java.introducao;

public class Aula05EstruturasCondicionais {
    public static void main(String args[]) {

        int idade = 15;

        if (idade >= 18) {
            System.out.println("Pode entrar na balada!");
        } else {
            System.out.println("Não pode entrar na balada");
        }

        // Operador ternário

        boolean isPodeBeber = idade >= 18 ? true : false;
        System.out.println(isPodeBeber);

        // TABELA VERDADE
        // V && V = V
        // V && F = F
        // F && F = F
        // v && V && V && F = F


        // Switch
        // Imprima o dia da semana, considerando 1 como domingo
        byte dia = 15;

        switch (dia) {
            case 1:
                System.out.println("Domingo");
                break;
            case 2:
                System.out.println("Segunda-Feira");
                break;
            case 3:
                System.out.println("Terça-Feira");
                break;
            case 4:
                System.out.println("Quarta-Feira");
                break;
            case 5:
                System.out.println("Quinta-Feira");
                break;
            case 6:
                System.out.println("Sexta-Feira");
                break;
            case 7:
                System.out.println("Sábado");
                break;
            default:
                System.out.println("Opção inválida!");
                break;
        }
    }
}
