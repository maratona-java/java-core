package src.academy.devdojo.maratona.java.introducao;

import java.util.Date;

/**
 * Prática
 *
 * Crie variáveis para os campos descritos abaixo entre <> e imprima a seguinte mensagem:
 *
 * Eu <nome>, morando no <endereço>,
 * confirmo que recebi o salário de <salario>, na data <data>
 */
public class Aula03TiposPrimitivosExercicios {
    public static void main(String args[]){
        String nome = "André";
        String endereco = "Rua das Couves";
        double salario = 2589.00;
        Date data = new Date();

        String relatório = "Eu "+nome +", morando na "+endereco + ", \n " +
                "confirmo que recebi o salário de "+ salario + ", na data ";

        System.out.println(relatório);

    }
}
