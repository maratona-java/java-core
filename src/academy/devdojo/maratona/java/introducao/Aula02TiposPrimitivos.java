package src.academy.devdojo.maratona.java.introducao;

public class Aula02TiposPrimitivos {
    public static void main(String[] args) {
        // int, double, float, char, byte, short, long, boolean

        int age = 10;
        long numeroGrande = 1000000;
        double salario = 2000;
        double salarioFloat = 2500;
        byte idadeByte = 10;
        short idadeShort = 10;
        boolean verdadeiro = true;
        boolean falso = false;
        char caracter = 65;

        System.out.println("A idade é "+ age + " anos.");
        System.out.println(verdadeiro);
        System.out.println(falso);
        System.out.println(caracter);


        // Casting é forçar o Java a colocar um valor dentro do outro
        // Lembrando que casting não é muito recomendado

        int idade = (int) 100000000000L;
        System.out.println(idade);

        String nome = "um grande texto";

        System.out.println(nome);
        System.out.println("Oi meu nome é" + nome);
    }
}
