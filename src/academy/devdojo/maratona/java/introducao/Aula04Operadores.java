package src.academy.devdojo.maratona.java.introducao;

public class Aula04Operadores {
    public static void main(String args[]){
        // + - / *
        int numero01 = 10;
        int numero02 = 20;
        System.out.print(numero01+numero02+"\n");
        System.out.print(numero01-numero02+"\n");
        System.out.print(numero01*numero02+"\n");
        System.out.print(numero01/numero02+"\n");
        System.out.print("Valor 02"+numero02+"\n");

        // Resto %
        int resto = 20 % 2;
        System.out.println(resto);

        // Operadores lógicos < > <= >= == !=
        boolean isDezMaiorQueVinte = 10 > 20;
        System.out.println("isDezMaiorQueVinte"+ isDezMaiorQueVinte);
        System.out.println("isDezMaiorQueVinte"+ isDezMaiorQueVinte);

        // Operadores lógicos && (AND) || (OU) ! (NEGAÇÃO)

        int minhaIdade = 35;
        float salarioBase = 3500F;
        boolean isDentroDaLei = minhaIdade > 30 && salarioBase >= 4612;
        System.out.println("Dentro da lei: "+ isDentroDaLei);

        boolean isAcompanhadoPelosPais = true;
        boolean isPodeEntrarNaBalada = minhaIdade >= 18 || isAcompanhadoPelosPais;
        System.out.println("Pode entrar na balada: " + isPodeEntrarNaBalada);

        // Operadores de Atribuição = += -= *= /= %=

        double bonus = 1800;
        bonus += 1000;
        System.out.println(bonus);
        bonus -= 1000;
        bonus *= 1000;


    }
}
