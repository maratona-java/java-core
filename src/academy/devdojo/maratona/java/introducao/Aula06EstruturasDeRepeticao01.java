package src.academy.devdojo.maratona.java.introducao;

public class Aula06EstruturasDeRepeticao01 {
    public static void main(String[] args) {
        // while, do while, for

        int count = 0;
        while(count < 10) {
            System.out.println(count);
            count++;
        }

        for (int i=1;i <=10; i++) {
            System.out.println("for: "+ i);
        }

        // STOP ITERATOR

        for (int i =1; i <=50; i++) {
            System.out.println(i);
            if(i == 25) {
                break;
            }
        }

        // CONTINUE
        for (int i =1; i <=50; i++) {
            if(i == 25) {
                continue; // Sai do loop e não imprime o valor 25 na linha 31
            }
            System.out.println(i);
        }

    }
}
