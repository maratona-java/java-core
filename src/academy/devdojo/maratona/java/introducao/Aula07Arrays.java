package src.academy.devdojo.maratona.java.introducao;

public class Aula07Arrays {
    public static void main(String[] args) {

        // Arrays são sempre variáveis do tipo references
        int[] idades = new int[3];
        idades[0] = 21;

        System.out.println(idades[0]);

        /**
         * Padrões de variáveis vazias
         *
         * byte, shot, int, ling, float e dougle -> 0
         * char -> '\u0000' ' '
         * boolean false
         * String null -> tipo references são nulos
         */

        // Como iterar sobre arrays

        String[] nomes = new String[3];
        nomes[0] = "Goku";
        nomes[1] = "Vegeta";
        nomes[2] = "Gohan";

        for (int i=0; i < nomes.length; i++) {
            System.out.println(nomes[i]);
        }

        // Formas de declaração de arrays

        String[] otherNames = {"Paulo", "Maria", "João"};
        for (int i=0; i <otherNames.length; i++) {
            System.out.println(otherNames[i]);
        }

        // Foreach

        int[] numbers = {1,2,3};
        for (int num: numbers) {
            System.out.println(num);
        }


        System.out.println("Arrays Multidimensioais");
        // Arrays Multidimensionais (Arrays de arrays)
        // 1, 2, 3, 4, 5
        // 31, 28, 31, dias

        int[][] days = new int[3][3];
        days[0][0] = 100;
        days[0][1] = 200;
        days[0][2] = 300;

        days[1][0] = 400;
        days[1][1] = 500;
        days[1][2] = 600;

        days[2][0] = 700;
        days[2][1] = 800;
        days[2][2] = 900;

        for (int i=0; i < days.length; i++) {
            for (int j=0; j < days[i].length; j++)
            System.out.println(days[i][j]);
        }

        System.out.println("Print array multidensionais");
        for (int[] baseArray: days) {
            for (int num:baseArray) {
                System.out.println(num);
            }
        }

        // Formas de inicialização de arrays multidimensionais.

        int[] array = {1,2,3};
        int [][] arrayInt = new int[3][];

        arrayInt[2] = new int[]{1,2,3,4,5};
    }
}
