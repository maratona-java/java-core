package src.academy.devdojo.maratona.java.introducao;

/**
 * Este é um comentário inclusive posso até usar <b>html</b>
 */
public class Aula01OlaDevDojo {
    // isso é um comentário de um linha

    /**
     * Este é um comentário de várias linhas
     *
     */
    /*
        Outro comentário
        Está é uma aula apenas para mostrar os comentários, mas evite usar comentários no código, código com muito
        comentário significam que seu código não é explícito o suficiente.
     */
    public static void main(String[] args) {
        System.out.println("KA-ME-HA-ME-HA");
    }
}