## Classes Internas

## Sumário

1. [Introduction](#introduction)
2. [Classes Locais](#classes-locais)
3. [Classes Anônimas](#classes-anônimas)
4. [Classes aninhadas estáticas](#métodos-genéricos)

## Introduction

Classes internas basicamente são uma classe dentro da outra é muito útil.


Note no exemplo abaixo que temos uma classe dentro da outra, e para instanciarmos uma classe do tipo `Inner` precisamos instanciar primeiro a classe `OuterClassesTest01`.

```java
class OuterClassesTest01 {
    private String name = "Monkey D. Luffy";

    // Nested
    class Inner {
        public void printOuterClassAttribute() {
            System.out.println(name);
            System.out.println(this); // this se refere a essa classe a Inner
            System.out.println(OuterClassesTest01.this); // referencia da classe OuterClasseTest01
        }
    }

    public static void main(String[] args) {
        OuterClassesTest01 outerClass = new OuterClassesTest01();
        Inner inner = outerClass.new Inner();
        inner.printOuterClassAttribute();

        // Segunda forma
        Inner inner2 = new OuterClassesTest01().new Inner();
        inner2.printOuterClassAttribute();
    }
}
```

## Classes Locais

Nunca veremos isso, só por questões didáticas:

```java
public class OuterClassesTest02 {

    private String name = "Midoriya";

    void print() {
        final String lastName = "Izuku";
        class LocalClass {
            public void printLocal() {
                System.out.println(name + " "+lastName);
            }
        }
        new LocalClass().printLocal();
    }

    public static void main(String[] args) {
        OuterClassesTest02 outer = new OuterClassesTest02();
        outer.print();
    }
}
```

## Classes Anônimas

Classes anônimas são classes que vão existir brevemente no nosso código e que não podem ser reutilizadas em nenhum lugar.

```java
class BarcoNameComparator implements Comparator<Barco> {
    public int compare(Barco o1, Barco o2) {
        return o1.getNome().compareTo(o2.getNome());
    }
}

public class AnonymousClassesTest02 {
    public static void main(String[] args) {
        List<Barco> barcoList = new ArrayList<>(List.of(new Barco("Lancha"), new Barco("Canoa")));
        barcoList.sort(new Comparator<Barco>() {
            @Override
            public int compare(Barco barco, Barco t1) {
                return barco.getNome().compareTo(t1.getNome());
            }
        });
    }
}
```

## Classes aninhadas estáticas

```java
public class OuterClassesTest03 {
    private String name;
    static class Nested {
        void print() {
            System.out.println(new OuterClassesTest03().name);
        }
    }

    public static void main(String[] args) {
        Nested nested = new Nested();
        nested.print();
    }
}
``

