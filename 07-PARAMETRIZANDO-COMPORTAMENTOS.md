## Parametrizando Comportamentos

Algo que nos ajuda a desenvolver códigos mais robustos e com maior facilidade para manutenções

Primeiro vamos a um exemplo de código e depois vamos ver como podemos melhorá-lo:

```java
public class ComportamentoPorParametroTest01 {
    private static List<Car> cars = List.of(new Car("green", 2011), new Car("black", 1998), new Car("red", 2019));

    public static void main(String[] args) {
        System.out.println(filterCarByColor(cars, "green"));
        System.out.println(filterCarByColor(cars, "red"));
    }

    private static List<Car> filterCarByColor(List<Car> cars, String color) {
        List<Car> filterCar = new ArrayList<>();
        for (Car car : cars) {
            if (car.getColor().equals(color)) {
                filterCar.add(car);
            }
        }
        return filterCar;
    }
}
```

imagine que temos o código acima e precisamos alterar a funcionalidade e melhorar nosso código.

Então vamos criar uma interface chamada `CarPredicate`:

```java
public interface CarPredicate {
    boolean test(Car car);
}
```

```java
public class ComportamentoPorParametroTest02 {
    private static List<Car> cars = List.of(new Car("green", 2011), new Car("black", 1998), new Car("red", 2019));

    public static void main(String[] args) {
        List<Car> filterCars = filter(cars, new CarPredicate() {

            @Override
            public boolean test(Car car) {
                return car.getColor().equals("green");
            }
        });
        System.out.println(filterCars);
    }

   private static List<Car> filter(List<Car> cars, CarPredicate carPredicate) {
       List<Car> filteredCar = new ArrayList<>();
       for (Car car : cars) {
           if (carPredicate.test(car)) {
               filteredCar.add(car);
           }
       }
       return filteredCar;
   }
}
```

podemos simplificar o método usando lambda function:

```java
public class ComportamentoPorParametroTest02 {
    private static List<Car> cars = List.of(new Car("green", 2011), new Car("black", 1998), new Car("red", 2019));

    public static void main(String[] args) {
        List<Car> filterCars = filter(cars, car -> car.getColor().equals("green"));
        System.out.println(filterCars);
    }

   private static List<Car> filter(List<Car> cars, CarPredicate carPredicate) {
       List<Car> filteredCar = new ArrayList<>();
       for (Car car : cars) {
           if (carPredicate.test(car)) {
               filteredCar.add(car);
           }
       }
       return filteredCar;
   }
}
```

Note que podemos montar um código bem pequeno mais muito robusto, pois estamos passando o comportamento como argumento da funcão:

```java
public class ComportamentoPorParametroTest02 {
    private static List<Car> cars = List.of(new Car("green", 2011), new Car("black", 1998), new Car("red", 2019));

    public static void main(String[] args) {
        List<Car> filterCars = filter(cars, car -> car.getColor().equals("red"));
        List<Car> filterBeforeCars = filter(cars, car -> car.getYear() < 2015);
        System.out.println(filterCars);
    }

   private static List<Car> filter(List<Car> cars, CarPredicate carPredicate) {
       List<Car> filteredCar = new ArrayList<>();
       for (Car car : cars) {
           if (carPredicate.test(car)) {
               filteredCar.add(car);
           }
       }
       return filteredCar;
   }
}
```

O java já possui um interface chamada predicate para fazer o que nós fizemos no exemplo anterior:


Vamos ver um exemplo bem genérico:

```java
public class ComportamentoPorParamentroTest03 {
    private static List<Car> cars = List.of(new Car("green", 2011), new Car("black", 1998), new Car("red", 2019));

    public static void main(String[] args) {
        List<Integer> nums = List.of(1,2,3,4,5,6,7,8,9,10);
        System.out.println(filter(nums, num -> num %2 == 0));
    }

    private static <T> List<T> filter(List<T> list, Predicate<T> predicate) {
        List<T> filteredList = new ArrayList<>();
        for (T element : list) {
            if (predicate.test(element)) {
                filteredList.add(element);
            }
        }
        return filteredList;
    }
}
```
