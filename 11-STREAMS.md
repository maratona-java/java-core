## Streams

## Sumário

1. [Introduction](#introduction)
2. [FlatMap](#flatmap)
3. [Finding and Matching](#finding-and-matching)
4. [Reduce](#reduce)
5. [Gerando streams](#gerando-streams)
6. [Collectors Summarizing](#collectors-summarizing)
7. [Collectors Grouping By](#collectors-grouping-by)
8. [Parallel](#parallel)


## Introduction

## FlatMap

## Finding and Matching

## Reduce

## Gerando streams

## Collectors Summarizing

## Collectors Grouping By

## Parallel
