## Maratona Java

### Como o Java funciona?

O Java é uma linguagem compilada, e tem um `JVM (Java Virtual Machine)` que é uma camada que fica entre o sistema operacional e a nossa aplicação que estamos desenvolvendo, ou seja não vamos desenvolver aplicações para o sistema operacional mas sim para a JVM que é uma máquina virtual que roda em cima do sistema operacional. Com isso vamos ter apenas uma versão da nossa aplicação.

- A máquina virutal deve ser instalada em cada sistema operacional, cada sistema operacional vai ter sua instalação.
- Então desenvolvemos a aplicação para a JVM, com isso é possível rodar a aplicação nos diferentes sistemas operacionais.
- O que executa na JVM não é nossos arquivo `.java`, mas sim os bytecodes que são arquivos do tipo `.class`.
- `.java`: é o que nós seres humanos conseguimos entender, códigos que nós escrevemos.
- `javac`: Através de um comando `javac` que é o Java compiler ele vai gerar um arquivo `.class` que é um byte code.
- `.class`: é um byte code que a JVM entende.
- Para utilizar a JVM no nosso computador, temos duas opções:
- JDK - Java Development Kit (Kit de desenvolvimento Java): Ele possui um JVM, um compilador, ferramentas de debug etc.
- Java possui retrocompatibilidade.
- JRE - Java Runtime environment

<img alt="como o java funciona" src="./public/images/como-o-java-funciona.png" width="1200">


### fazendo o download da JDK 15

O Java é uma linguagem convencionada, ela possui diversos tipos de convenções onde temos convenção de instalação, configuração, códigos e etc. e a vantagem de termos um convenção em diversos países, é que independente de onde estamos atuando vamos ter a linguagem de programação java no mesmo estilo.

Por padrão o `JCP (Java community process)` diz que que quando instalamos o java, precisamos deixá-lo disponível por diversas partes do nosso computador, ou seja precisamo definir o Java no path das nossas variáveis de ambiente. também precisamos dizer para as outras aplicações onde o Java foi instalado.

### Executando compilação manualmente

1 - Vamos criar uma classe Java chamada `OlaDevDojo.java`

```java
package dev;

public class OlaDevDojo {
    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("KA-ME-HA-ME-HAAAA");
    }
}
```

2 - Agora vamos gerar o byte code através do comando javac:

    javac OlaDevDojo.java

Após realizar o comando com o javac vai ser gerado um arquivo chamado `OlaDevDojo.class` esse é o nosso byte code, uma linguagem de baixo nível e é esse arquivo que vamos passar para a JVM.
Agora vamos passar o arquivo para nossa JVM atraves do comando

    java dev/OlaDevDojo

ou com o comando:

    java introducao.src.academy.devdojo.maratona.java.Aula01OlaDevDojo

### Organizando o código em pacotes

java é uma linguagem orientada a pacotes, e o intuito dos pacotes é organizar as classes.

## Tipos primitivos no Java

Tipos primitivos são os tipos que vão guardar em memória um valor simples e o Java possui 8 tipos primitivos. Uma observação sobre tipos primitivos é que todas são escritas em letrar minúsculas

- int
- double
- float
- char
- byte
- short
- long
- boolean

Como funcionam as variáveis no java

<img alt="como funciona as variáveis" src="public/images/como-funciona-variaveis.png" width="1200">

## Arrays

<img alt="Arrays" src="./public/images/arrays01.png" width="850">

### Arrays Multidimensionais

<img alt="Arrays" src="./public/images/arrays-multidimensionais.png" width="850">