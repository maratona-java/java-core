## Exceptions

Documentação:https://docs.oracle.com/en/java/javase/15/docs/api/index.html

## Sumário

1. [Exceptions](#exceptions)
2. [Como funciona uma Stack](#como-funciona-uma-Stack)
3. [RuntimeException](#runtimeexception)
4. [Lançando exceção do Unchecked](#lançando-exceção-do-unchecked)
5. [Lançando exceção do Checked](#lançando-exceção-do-checked)
6. [Bloco Finally](#bloco-finally)
7. [Capturando múltiplas exceptions](#capturando-múltiplas-exceptions)
8. [Multi catch em linha](#multi-catch-em-linha)
9. [Try with resources](#try-with-resources)
10. [Exceção customizada](#Exceção customizada)
11. [Exceção e regras de sobrescrita](#exceção-e-regras-de-sobrescrita)

## Exceptions

Exception são condições anormais do nosso programa.

Existem alguns exemplos simples de erro:

- Tentativa de leitura de um arquivo que foi deletado, ou movido.
- Tentativa de conexão com o banco de dados, onde temos uma falha na conexão, internet cair, banco de dados fora do ar.

Enfim são condições que estão além do nosso controle.

O Java nos oferece uma quantidade enorme de classes e formas para trabalharmos com esses fluxos anormais que com certeza vai ocorrer em qualquer aplicação.

### Cadeia de Exception no Java

vamos analisar a imagem abaixo:

- As `exceptions` no java como tudo são objetos 
- Todas as exceptions no Java são da classe `Throwable`, que é a classe `Mãe`.
- Temos dois tipos o `Error` e `Exception`.
  - `Erros`: são situações que não podemos recuperar:
    - Imagine que seu sistema está rodando e acaba a memória
    - Erros no seu programa, são impossíveis de tentar recuperar, devemos parar o programa resolver o problema e inciar novamente.
    - Quando temos `erros` geralmente a JVM para de executar.
    - Podemos ter também um erro chamado de `StackOverflow` estouro de memória da chamada da Stack

<img alt="exceptions" src="./public/images/exceptions.png" width="850">

## Como funciona uma Stack

<img alt="cadeia de exceptions no java" src="./public/images/stack.png" width="850">

Imagine agora que dentro da nossa stack temos um método recursivo que chama ele mesmo. Então nesse caso, se não tivermos um limite vamos ter um erro de estouro de stack ou `StackOverflow`.

<img alt="stackoverflow" src="./public/images/stack-over-flow.png" width="850">

Então lembrando que Erro é algo que acontece na JVM que provavelmente você não vai recuperar em tempo de execução, então a solução é arrumar o código

```java
public class StackOverflowTest01 {
    public static void main(String[] args) {
        recursividade();
    }

    // Método recursivo
    public static void recursividade() {
        recursividade();
    }
}
```

## RuntimeException

- Vale lembrar que a classe `Throwable` tem duas filhas que são as classes `Error` e `Exception`.
- Então como as classes são irmãs, `erros são erros e exceptions são exceptions`.

### Checked e Unchecked Exceptions (Exceções checadas e Exceções não checadas)

`Checked`: Exceptions do tipo `Checked` são exceptions que são filhas da classe Exception diretamente e se não tratadas vão lançar um erro em tempo de compilação, então você nem consegue compilar seu código.
  - Quando trabalhamos com arquivos, provavelmente vão lançar o `IOException`, e neste caso se não tratarmos o código não compila.

`Uncheked`: São filhas da classe `RuntimeException` ou a própria classe RuntimeException. Na arvore é da RuntimeException para baixo.
  - Essas exceptions RuntimeException ou as filhas de RuntimeExceptions, que quando lançadas pelo programa quase 100% das vezes o problema é você que desenvolveu algo errado, ou não fez a tratativa.
  - Um cast errado, acessar um posição que não existe em um array, etc.
  - Todas essas exceptions devem ser tratadas pelo desenvolvedor, isso não pode acontecer.

<b>Observação</b>

- RuntimeExceptions são do tipo `Unchecked` e você não é obrigado a tratá-las.
- Exceptions do tipo `Checked` ou as `Checked Exceptions` obrigatoriamente você precisa fazer o tratamento delas senão o código não compila.

Uma `RuntimeException` é uma exceção que vai ocorrer em tempo de execução.

### Causando algumas Unchecked exceptions

Vamos causar algumas RuntimeExceptions para ver o que acontece com o nosso programa:

- 1 - NullPointerException

```java
public class RuntimeExceptionTest01 {
    public static void main(String[] args) {
        Object object = null;
        System.out.println(object.toString());
    }
}
```

- 2 - ArrayIndexOutOfBoundsException
```java
public class RuntimeExceptionTest01 {
  public static void main(String[] args) {
    int[] nums = {1,2};
    System.out.println(nums[10]);
  }
} 
```

### Exceptions do tipo Checked Exceptions

Checked Exceptions são exceções que são filhas diretas da `Exception`.

Vamos exemplificar uma classe que precisa de um tratamento de Checked Exception.

Documentaçao: https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/io/File.html

Observe na figura abaixo na própria documentação do Java temos o método `createNewFile()` e observe que esse método tem a possibilidade de lançar uma `IOexception`.

<img alt="ioexception" src="./public/images/ioexception.png" width="850"> 

Se acessarmos a documentação da `IOException` vamos ver que ela é filha da classe `Exception` isso significa que ela é uma `Checked Exception` ou seja, deve ser tratada.

<img alt="arvore exception" src="./public/images/arvore-exception.png" width="850"> 

Exemplo

```java
public class ExceptionTest01 {
    public static void main(String[] args) {

    }

    private static void createNewFile() {
        File file = new File("tmp\\teste.txt");

        try {
            boolean isCreated = file.createNewFile();
            System.out.println("Arquivo criado "+ isCreated);
        } catch (IOException e) { // Você NUNCA deve deixar o catch em branco!!! E não deve ter lógica de negócio
            throw new RuntimeException(e);
        }
    }
}
```

## Lançando exceção do Unchecked

```java
public class RuntimeExceptionTest02 {
    public static void main(String[] args) {
        int resultado = divisao(10, 0);
    }

    private static int divisao(int a, int b) throws ArithmeticException { 
        if (b == 0) {
            throw new IllegalArgumentException("Não é possível fazer divisão por zero");
        }
        return a/b;
    }
}
```

Em exceptions do tipo `Unchecked` ou `Runtime` você não é obrigado e é muito comum não ver a especificação do throws logo após a função como o `ArithmeticException`.

```java
private static int divisao(int a, int b) throws ArithmeticException { 
```

## Lançando exceção do Checked

Quando temos exception do tipo `Checked` ou filhas de `Exception` somos obrigados a adicionar na assinatura do método a informação de que o método pode lançar uma exception.

a informação de que nosso método pode lançar uma exception é uma programação defensiva, e é muito importante.


O tratamento de exceptions com `Try e Catch` tem seus prós e contras:

- 1 - O Pró é que se você faz tratamento de exceptions é bom porque seu código não vai explodir.
- 2 - Um caso negativo é que uma pessoa que está chamando seu método não tem opção de fazer o tratamento da forma que ele quiser.

Então é importante saber que tem casos que é importante fazer o tratamento de exceptions é tem casos que não.

Neste caso temos a opção de não querer fazer o tratamento na função criar novo arquivo, mas sim passar a responsabilidade para quem está chamando.

- Quando colocamos o `throws IOException` estamos dizendo quem chamar essa função vai ter que fazer o tratamento de exception, ou jogar a exceptions pra frente.
- No exemplo abaixo estamos passando a responsabilidade para a classe main, é ela que está chamando a função, então é ela que vai ter que fazer o tratamento de exceptions, ou passar a diante.


```java
public class ExceptionTest02 {
    public static void main(String[] args) {
        try {
          createNewFile(); // O método main deve tratar a exception caso contrátrio temos um erro explodindo no sistema.    
        } catch(IOException e) {
          e.printStackTrace();
        }
        
    }

    private static void createNewFile() throws IOException { // joga a responsabilidade de tratar uma possível exception para que chamar
        File file = new File("tmp/teste.txt");
        boolean isCreated = file.createNewFile();
        System.out.println("Arquivo criado "+ isCreated);
    }
}
```

### Quando devo utilizar o throws ou fazer o tratamento com try catch?

- Se você está criando um método privado as chances é que você vai ter o try catch, pois ninguem de fora dessa classe vai ter acesso. então não faz sentido jogar essa exception pra cima.
- Se o método for `publico` não temos ideia de quem pode chamar esse método, então neste caso é interessante manter o throws e jogar a responsabilidade pra frente.
- Depende muito de projeto para projeto


É possível ainda fazer algo híbrido que é capturar a exception e lançar 

```java
public class ExceptionTest03 {
    public static void main(String[] args) throws IOException {
        try {
            createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createNewFile() throws IOException {
        File file = new File("tmp/teste.txt");

        try {
            boolean isCreated = file.createNewFile();
            System.out.println("Arquivo criado "+ isCreated);
        } catch (IOException e) { // Trata a exception
            e.printStackTrace();
            throw e; // Joga pra cima outra exception
        }
    }
}
```

## Bloco Finally

O bloco finally é um bloco que vai ser executado, independente da condição que vai acontecer no nosso bloco try catch.

Quando trabalhamos com arquivo, ou banco de dados podemos ter alguns problemas que podem ocorrer, por exemplo, imagine que abrimos uma arquivo, o sistema operacional vai alocar recursos para essa tarefa, porém ao finalizar essa tarefa devemos fechar os recursos para não termos problema. então temos algo do tipo

```java
public class RuntimeTimeException03 {
    public static void main(String[] args) {
        try {
            System.out.println("Abrindo arquivo");
            System.out.println("Escrevendo dados no arquivo");
            System.out.println("Fechando recurso liberado do sistema operacional");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
```

Imagine que quando estamos escrevendo no arquivo temos uma Exception, a linha abaixo nunca será executada portanto o recurso ficará aberto. e cadas vez que é chamado mais um recusro é aberto.

Então aqui temos um problema, pois os recursos são abertos, e não são fechados o que pode dar problema de memória.

Então dado esse problema anterior onde podemos ter vários arquivos abrindo processos e não fechando, para resolvermos isso da melhor forma seria usando o `bloco finally`.

O bloco finally será executados independente se há ou não erro, então neste caso, podemos mover para dentro dele o fechamento dos recursos. Com isso mesmo que haja um Exception nós vamos fechar os recursos normalmente.

```java
public class RuntimeTimeException03 {
    public static void main(String[] args) {
        try {
            System.out.println("Abrindo arquivo");
            System.out.println("Escrevendo dados no arquivo");

        } catch (Exception e) {
            e.printStackTrace();
        } finally { // Sempre será executado
            System.out.println("Fechando recurso liberado do sistema operacional");
        }
    }
}
```

## Capturando múltiplas exceptions

Existem casos em que podemos capturar multiplas exception, para isso temos multiplos catchs

Observe o exemplo abaixo:

```java
public class RuntimeExceptionTest04 {
    public static void main(String[] args) {
        try {
            throw new RuntimeException();
        }catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Dentro do ArrayIndexOutOfBoundsException");
        }catch (IndexOutOfBoundsException e) {
            System.out.println("Dentro do IndexOutOfBoundsException");
        }catch (IllegalArgumentException e){
            System.out.println("Dentro do IllegalArgumentException");
        }catch (ArithmeticException e) {
            System.out.println("Dentro do ArithmeticException");
        }catch (RuntimeException e) {
            System.out.println("Dentro do RuntimeException");
        }
    }
}
```

Note nome exemplo anterior que temos vários catchs para capturar multiplas exceptions porém temos algumas regras:

- Não podemos colocar um tipo mais genérico a frente dos outros catchs, pois elas pegam todas as exceptions. conceito do polimorfismo.
- Exceptions do tipo mais genérica devem vir sempre no final.

Um método pode lançar mais de uma exception:

```java
public class RuntimeExceptionTest04 {
    public static void main(String[] args) {
        try {
            talvezLanceUmaException();
        } catch (SQLException e) {
            System.out.println("SQL Exception");
        } catch (FileNotFoundException e) {
            System.out.println("SQL FileNotFoundException");
        }
    }

    private static void talvezLanceUmaException() throws SQLException, FileNotFoundException {

    }
}
```

## Multi catch em linha 

Existe uma sintaxe para melhor a legibilidade do código, essa sintaxe diz que você pode colocar exceptions que não estão na mesma linha de herança, dentro de um mesmo catch separadas por pipes |`.

```java
public class RuntimeExceptionTest05 {
    public static void main(String[] args) {
        try {
            talvezLanceUmaException();
        } catch (SQLException | FileNotFoundException e) {
            System.out.println("SQL Exception");
        }
    }

    private static void talvezLanceUmaException() throws SQLException, FileNotFoundException {

    }
}
```

Mas devemos tomar cuidade por não devemos fazer isso quando temos exceptions na mesma linha de herança.

## Try with resources

Vamos observar o código abaixo:

```java
public class TryWithResourcesTest01 {
  public static void main(String[] args) {

  }

  public static void ReadFile() {
    Reader reader = null;
    try {
      reader = new BufferedReader(new FileReader("teste.txt"));
    }catch (FileNotFoundException e) {
      e.printStackTrace();
    }finally {
      try {
        if (reader != null) {
          reader.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
}
```

Note que no código acima fica um pouco feio, então por conta disso o Java incluiu o `Try with resoources`:

O `Try with resources` se encarrega de tomar conta de fechar a conexão da variável de referência Reader e para isso devemos seguir algumas regras:

- Você só pode colocar colocar variáveis ou objetos dentro do try with resource que implementam a interface `Closeable` e `AutoCloseable`.
- Temos essa regra por que se lembrarmos do polimorfismo pois nós temos certeza absoluta que o Java vai conseguir chamar o close, porque o `Reader` implementa o `Closeable` e Closeable tem apenas um método que é o close. 
- Neste caso também podemos remover o bloco catch e adicionar o `throws IOException`.

Com isso o código fica da seguinte forma:

```java
 public static void ReadFile() throws IOException{
    try (Reader reader = new BufferedReader(new FileReader("teste.txt")) ){
    }
}
```

vamos a outro exemplo:

Vamos criar uma classe chamada `Leitor1` que implementa `Closeable`:

```java
public class Leitor1 implements Closeable {

    @Override
    public void close() throws IOException {
        System.out.println("Fechando leitor 1");
    }
}
```

Vamos criar outra classe chamada `Leitor2` que implementa `Closeable`:

```java
public class Leitor2 implements Closeable {
    public void close() throws IOException {
        System.out.println("Fechando leitor 2");
    }
}
```

- Agora em nosso arquivo principal vamos criar uma função chamada `lerArquivo()`.
- E chamar a função lerArquivo na função `main`:

```java
public class LeitorTest01 {
    public static void main(String[] args) {
        lerArquivo();
    }

    public static void lerArquivo() {
        try (Leitor1 leitor1 = new Leitor1();
             Leitor2 leitor2 = new Leitor2()){

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
```

Note que temos a seguinte saida:

    Fechando leitor 2
    Fechando leitor 1

Aqui temos duas coisas:

- 1 - O Java foi responsável por chamar o método close
- 2 - Eles são fechados na ordem inversa que foram declarados.

## Exceção customizada

O Java possui diversas exceptions por padrão na linguagem , porém em alguns casos precisamos criar as nossas próprias exceptions é o que vamos ver nesse tópico:

Podemos criar nossas própiras exceptions, porém aqui fica uma pergunta:

Vamos criar exceptions do tipo `checked` ou `unchecked?

vamos criar uma exception chamada `InvalidLoginException:

```java
public class InvalidLoginException extends Exception {
    public InvalidLoginException() {
        super("Invalid Login");
    }

    public InvalidLoginException(String message) {
        super(message);
    }
}
```
 e no nosso arquivo main vamos fazer o seguinte:

```java
public class InvalidLoginExceptionTest01 {
    public static void main(String[] args) {
        try {
            login();
        }catch (InvalidLoginException e) {
            e.printStackTrace();
        }
    }

    private static void login() throws InvalidLoginException{
        Scanner keyboard = new Scanner(System.in);
        String username = "Goku";
        String password = "ssj";

        System.out.println("Usuário");
        String usernameReceived = keyboard.nextLine();
        System.out.println("Password");
        String passwordReceived = keyboard.nextLine();

        if (!username.equals(usernameReceived) || !(password.equals(password))) {
            throw new InvalidLoginException("User or password invalid!");
        }

        System.out.println("Usuário logado com sucesso!");
    }
}
```

## Exceção e regras de sobrescrita

Para este tópico vamos criar uma classe chamada `Pessoa`:

```java
public class Pessoa {
    public void salvar() throws InvalidLoginException, FileNotFoundException {
        System.out.println("salvando pessoa");
    }
}
```

Vamos criar também uma classe chamada `Funcionario` que extende de `Pessoa`:

```java
public class Funcionario extends Pessoa{
    public void salvar() {
        System.out.println("Salvando funcionário");
    }
}
```


No arquivo main vamos digitar:

```java
public class SobrescritaComExceptionTest01 {
    public static void main(String[] args) {
        Pessoa pessoa = new Pessoa();
        Funcionario funcionario = new Funcionario();

        funcionario.salvar();
    }
}
```

Note que não haverá nenhum erro pois há algumas regras:

- 1 - Quando estamos sobrescrevendo um método não somos obrigados a declarar as mesmas Exception que aquele método está declarando, pois a funcionalidade da sobrescrita é justamente permitir criar coisas diferentes
- 2 - Podemos lançar uma ou mais exeptions que esse método original possui. 
- 3 - Podemos adicinoar qualquer exception do tipo uncheked 
- 4 - Não podemos adicionar exceptions mais genéricas ou tipo checked no throws
- 5 - Não podemos adicionar nenhuma exception do tipo checked que não foi declarada no método originalpor exemplo Exception.