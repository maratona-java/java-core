## Generics

## Sumário

1. [Introduction](#introduction)
2. [Wildcard](#wildcard)
3. [Classes Genéricas](#classes-genéricas)
4. [Métodos Genéricos](#métodos-genéricos)

## Introduction

Antes de inciarmos os estudos sobre Generics vamos entender o porque foi criado esse recurso.

Antigamente tinhamos o seguinte:

- Note no código abaixo que criavamos uma lista que aceitava qualquer coisa.
- ja que essa lista aceita qualquer tipo, imagine para usar um recurso em uma string, teriamos que fazer vários if para saber o tipo do elemento, pois na iteração não fazemos ideia do que pode ser


```java
public class GenericsTest01 {
    public static void main(String[] args) {
        List lista = new ArrayList();
        lista.add("Dragon Ball");
        lista.add(123L);
        lista.add(10);
        lista.add(new Consumidor("Goku"));
        for (Object o : lista) {
            System.out.println(o); // Teriamos que fazer vários if para saber qual o tipo
        }
    }
}
```

Então para manter a funcionalidade anterior sem quebrar e melhorar o Java criou em tempo de compilação as chamadas `Generics` onde vamos forçar que a lista tenha um determinado tipo:

```java
List<String> lista = new ArrayListr<>();
```

### Exemplo

```java
public class GenericsTest01 {
    public static void main(String[] args) {
        // Type erasure -> O tipo é apagado quando compilado, na JVM não da pra saber o tipo
       List<String> lista = new ArrayList<>();
       lista.add("Goku");
       lista.add("Vegeta");
       lista.add("Kuririm");

       for (String l: lista) {
           System.out.println(l);
       }
    }
}

```

Quando não usamos generics corremos o risco de mandar para a JVM um código quebrado que só vai dar problema em tempo de execução, já que não tivemos erro para compilar.

```java
public class GenericsTest01 {
    public static void main(String[] args) {
       // Type erasure
       List<String> lista = new ArrayList<>();
       lista.add("Goku");
       lista.add("Vegeta");
       lista.add("Kuririm");

       for (String l: lista) {
           System.out.println(l);
       }

       add(lista, new Consumidor("Midoriya"));
       
       // Problema
       for (String l: lista) {
            System.out.println(l);
        }
    }

    private static void add(List lista, Consumidor consumidor) {
        lista.add(consumidor);
    }
}
```

## Wildcard

Antes de falarmos sobre Wildcard vamos ver alguns pontos:

```java
abstract class Animal {
    public abstract void consulta();
}

class Cachorro extends Animal {

    @Override
    public void consulta() {
        System.out.println("Consultando nosso doguinho");
    }
}

class Gato extends Animal {

    @Override
    public void consulta() {
        System.out.println("Consultando Gato");
    }
}
public class WildcardTest01 {
    public static void main(String[] args) {
        Cachorro[] cachorros = {new Cachorro(), new Cachorro()};
        Gato[] gatos = {new Gato(), new Gato()};
        printConsulta(cachorros);
        printConsulta(gatos);
    }

    private static void printConsulta(Animal[] animals) {
        for (Animal animal : animals) {
            animal.consulta();
        }
        animals[0] = new Gato();
    }
}
```

### Problema


Agora vamos a um problema que podemos ter:

- O código abaixo nos traz um problema que é na chamada da funcção `printConsulta` como vamos saber se o parametro é do tipo `Cachorro` ou `Gato`?


```java
public class WildcardTest02 {
    public static void main(String[] args) {
        List<Cachorro> cachorros = List.of(new Cachorro(), new Cachorro());
        List<Gato> gatos = List.of(new Gato(), new Gato());
        printConsulta(cachorros);
    }

    private static void printConsulta(List<Animal> animals) {
        for (Animal animal : animals) {
            animal.consulta();
        }
        animals.add(new Cachorro());
    }
}
```

Agora que vimos o problema, como podemos resolver esse problema passando um tipo mais generico para evitarmos o erro de compilação:

- O WildCard é o ? ou `caracter curinga`
- para resolver o problema anterior vamos passar um interrrogação para dizera a função que vou aceitar nesse argumento qualquer tipo que seja do tipo animal, ou que extenda do tipo animal.

```java
private static void printConsulta(List<? extends Animal> animals) {
        for (Animal animal : animals) {
            animal.consulta();
        }
        // Neste caso perdemos a possibilidade de adicionar elementos nessa lista
        animals.add(new Cachorro()); // Isso se perda pois não sabemos o tipo da classe e não tem como adicionar um gato a um cachorro
    }
```

```java
public class WildcardTest02 {
    public static void main(String[] args) {
        List<Cachorro> cachorros = List.of(new Cachorro(), new Cachorro());
        List<Gato> gatos = List.of(new Gato(), new Gato());
        printConsulta(cachorros);
        printConsulta(gatos);
    }

    private static void printConsulta(List<? extends Animal> animals) {
        for (Animal animal : animals) {
            animal.consulta();
        }
    }
}
```

```java
public class WildcardTest02 {
    public static void main(String[] args) {
        List<Cachorro> cachorros = List.of(new Cachorro(), new Cachorro());
        List<Gato> gatos = List.of(new Gato(), new Gato());
        printConsulta(cachorros);
        printConsulta(gatos);
        List<Animal> animals = new ArrayList<>();
        printConsultaAnimal(animals);
    }

    private static void printConsulta(List<? extends Animal> animals) {
        for (Animal animal : animals) {
            animal.consulta();
        }
    }

    private static void printConsultaAnimal(List<? super Animal> animals) {
        animals.add(new Cachorro());
        animals.add(new Gato());
    }
}
```

## Classes Genéricas

Vejamos um exemplo:

- Imagine que temos a classe `Carro`:
```java
public class Carro {
    private String nome;

    public Carro(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Carro{" +
                "nome='" + nome + '\'' +
                '}';
    }
}
```

e temos uma outra classe chamada de `Barco`:

```java
public class Barco {
    private String nome;

    public Barco(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Barco{" +
                "nome='" + nome + '\'' +
                '}';
    }
}
```

Temos também uma classe `CarroRentavelSerive`:

```java
public class CarroRentavelService {
    private List<Carro> carrosDisponiveis = new ArrayList<>(List.of(new Carro("BMW"), new Carro("Fusca")));

    public Carro buscarCarroDisponivel() {
        System.out.println("Buscando carro disponível...");
        Carro carro = carrosDisponiveis.remove(0);
        System.out.println("Alugando carro: "+ carro);
        System.out.println("Carros disponíveis para alugar: ");
        System.out.println(carrosDisponiveis);
        return carro;
    }

    public void retornarCarroAlugado(Carro carro) {
        System.out.println("Devolvendo carro "+ carro);
        carrosDisponiveis.add(carro);
        System.out.println("Carros disponíveis ");
        System.out.println(carrosDisponiveis);
    }
}
```

Temos outra classe chamada `BarcoRentavelSerive`:

```java
public class BarcoRentavelService {
    private List<Barco> barcosDisponiveis = new ArrayList<>(List.of(new Barco("Lanche"), new Barco("Canoa")));

    public Barco buscarbarcoDisponivel() {
        System.out.println("Buscando barco disponível...");
        Barco barco = barcosDisponiveis.remove(0);
        System.out.println("Alugando barco: "+ barco);
        System.out.println("Barcos disponíveis para alugar: ");
        System.out.println(barcosDisponiveis);
        return barco;
    }

    public void retornarCarcoAlugado(Barco barco) {
        System.out.println("Devolvendo barco "+ barco);
        barcosDisponiveis.add(barco);
        System.out.println("Barcos disponíveis ");
        System.out.println(barcosDisponiveis);
    }
}
```

e nosso método main temos:

```java
public class ClasseGenericaTest01 {
    public static void main(String[] args) {
        CarroRentavelService carroRentavelService = new CarroRentavelService();
        Carro carro = carroRentavelService.buscarCarroDisponivel();
        System.out.println("Usando carro por um mês...");
        carroRentavelService.retornarCarroAlugado(carro);
    }
}
```

Imagine agora que temos outra classe que chama:

```java
public class ClasseGenericaTest02 {
    public static void main(String[] args) {
        BarcoRentavelService barcoRentavelService = new BarcoRentavelService();
        Barco barco = barcoRentavelService.buscarbarcoDisponivel();
        System.out.println("Usando carro por um mês...");
        barcoRentavelService.retornarCarcoAlugado(barco);
    }
}
```


Vamos observar alguns pontos:

- Note que temos a classe Carro e Barco que fazem a mesma coisa, no fima das contas o que muda é só o objeto.
- Repare na quantidade de códigos iguais que temos.
- Quanto código poderia ser diminuido nessa classe
- No fim das contas temos o mesmo resultado para `Carros` e `Barcos

Dados o exemplo anterior, agora vamos refatorar esse código usando classe Generica para uma abordagem melhor, vamos ver como fica:

vamos criar uma classe de serviço chamada de `RentalService` e vamos usar Generics

```java
public class RentalService<T> {

    private List<T> objetosDisponiveis;

    public RentalService(List<T> objetosDisponiveis) {
        this.objetosDisponiveis = objetosDisponiveis;
    }

    public T buscarObjetoDisponivel() {
        System.out.println("Buscando objeto disponível...");
        T t = objetosDisponiveis.remove(0);
        System.out.println("Alugando objeto: "+ t);
        System.out.println("Objetos disponíveis para alugar: ");
        System.out.println(objetosDisponiveis);
        return t;
    }

    public void retornarObjetoAlugado(T t) {
        System.out.println("Devolvendo objeto "+ t);
        objetosDisponiveis.add(t);
        System.out.println("Carros disponíveis ");
        System.out.println(objetosDisponiveis);
    }
}
```

E agora na nossa classe `Main` olha como podemos customizar nosso código.

```java
public class ClasseGenericaTest03 {
    public static void main(String[] args) {
        List<Carro> carrosDisponiveis = new ArrayList<>(List.of(new Carro("BMW"), new Carro("Fusca")));
        List<Barco> barcosDisponiveis = new ArrayList<>(List.of(new Barco("Lancha"), new Barco("Canoa")));

        RentalService<Carro> rentalService = new RentalService<>(carrosDisponiveis);
        Carro carro = rentalService.buscarObjetoDisponivel();
        System.out.println("Usando carro por um mês...");
        rentalService.retornarObjetoAlugado(carro);

        System.out.println("----------------------------");

        RentalService<Barco> rentalService1 = new RentalService<>(barcosDisponiveis);
        Barco barco = rentalService1.buscarObjetoDisponivel();
        System.out.println("Usando carro por um mês...");
        rentalService1.retornarObjetoAlugado(barco);
    }
}
```

## Métodos Genéricos

```java
public class MetodoGenerico01 {
    public static void main(String[] args) {
        List<Barco> barcoList = criarArrayComUmObjeto(new Barco("Canoa Marota"));
        System.out.println(barcoList);
    }

    private static <T> List<T> criarArrayComUmObjeto(T t) {
        List<T> list = List.of(t);
        return list;
    }
}
```