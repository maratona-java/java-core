## Orientação a Objetos

## Sumário

1. [Introdução](#introdução)
2. [Classes](#classes)
3. [Coesão](#coesão)
4. [Referência de Objetos](#referência-de-objetos)
5. [Métodos](#metodos)
6. [Parametros como cópia e referência](#parametros-como-cópia-e-referência)
7. [Modificador de acesso private get e set](#modificador-de-acesso-private-get-e-set)
8. [Sobrecarga de métodos](#sobrecarga-de-métodos)
9. [Construtores](#construtores)
10. [Blocos de inicialização](#blocos-de-inicialização)
11. [Modificador static](#modificador-static)
12. [Associação](#associação)
13. [Herança](#herança)
14. [Sequência de inicialização](#sequência-de-inicialização)
15. [Sobrescrita do método toString](#sobrescrita-do-método-tostring)
16. [Modificador final](#modificador-final)
17. [Enumeração](#enumeração)
18. [Classes abstratas](#classes-abstratas)
19. [Métodos abstratos](#métodos-abstratos)
20. [Métodos abstratos regras](#métodos-abstratos-regras)
21. [Interfaces](#interfaces)
22. [Polimorfismo](#polimorfismo)
23. [Programação Orientada a interfaces](#programação-orientada-a-interfaces)

## Introdução

A `orientação a objetos` foi criada para mapear o mundo real para o mundo computacional, vamos partir do principio que as coisas podem ser agrupadas,.

Por exemplo imagine que temos `nome`, `idade`, `sexo` se agruparmos esses dados por funcionalidades podemos ter uma pessoa, um estudante ou qualquer coisa no mundo real que é composto por esses dados.

Veja na imagem abaixo que temos os dados agrupados dentro de um contexto maior, que chamamos de objeto. 

Então temos diversos dados salvos em um único espaço de memória

<img alt="orientacao a objetos - objetos" src="./public/images/orientacao-objetos.png" width="850">

### Classes 

A partir de uma forma, formato ou padrão, podemos criar vários objetos, isso é o que chamamos de classe.

A classe é um agrupamento de coisas do mundo real que dão origem a um objeto.

## Coesão

Coesão é algo que está relacionado ao proposito das suas classes.

Quando falamos que nosso código é altamente coeso, estamos dizendo que as nossas classes não estão misturando o propósito delas de existirem

Um código com alta coesão é muit bom.

## Referência de Objetos

Imagine que temos duas variáveis chamadas `carro1` e `carro2` e agora queremos que a variável carro1 aponte para o mesmo endereço de memória do carro2

para isso podemos fazer da seguinte forma:

```java
package src.academy.devdojo.maratorna.java.javacore.Aintroducaoclasses.test;

import domain.Aintroducaoclasses.javacore.src.academy.devdojo.maratona.java.Carro;

public class ExercicioTest01 {
    public static void main(String[] args) {
        Carro carro = new Carro();
        Carro carro2 = new Carro();

        carro.nome = "Fusca";
        carro.Modelo = "Esporte";
        carro.ano = 1987;

        carro2.nome = "Mustang";
        carro2.Modelo = "GT 500";
        carro2.ano = 1968;

        carro1 = carro2;

        System.out.println(carro.nome + " " + carro.Modelo + " " + carro.ano);
        System.out.println(carro2.nome + " " + carro2.Modelo + " " + carro2.ano);
    }
}
```

Quando perdemos a referência para um objeto, nunca mais vamos conseguir acessá-lo novamente, o objeto está perdido em memória.

Só podemos fazer isso quando temos objetos do mesmo tipo.

## Métodos

Os métodos são comportamentos das classes.


## Parametros como cópia e referência

### Passando de cópia por parâmentro

Quando passamos um parâmetro de uma variável de tipo primitivo, na verdade estamos passando uma cópia do valor, e não o valor original. Exemplo:

```java
package src.academy.devdojo.maratorna.java.javacore.Bintroducaometodos.test;

import domain.Bintroducaometodos.javacore.src.academy.devdojo.maratona.java.Calculadora;

public class calculadoraTest01 {
    public static void main(String[] args) {
        Calculadora calculadora = new Calculadora();
        calculadora.somaDoisNumeros();
        System.out.println("Finalizando calculadora");

        calculadora.multiplicaDoisNumeros(100, 2);
        System.out.println(calculadora.divideDoisNumeros(100, 2));
        System.out.println("Par: " + calculadora.isPar(11));

        System.out.println("-------------------------------------");

        int num1 = 1;
        int num2 = 2;

        calculadora.alteraDoisNumeros(num1, num2); // Quando passamos variáveis do tipo primitivo sempre uma cópia é enviada
    }
}
```


### Passando parâmentro por referência

Quando passamos uma variável do tipo objeto para uma função, estamos passando a referência desse objeto para a nossa função.

Então a variável que está sendo enviada por parâmetro aponta para o mesmo endereço de memória, diferente dos casos de variáveis do tipo primitivo que são passados uma cópia da variável.

```java
package src.academy.devdojo.maratorna.java.javacore.Bintroducaometodos.test;

import domain.Bintroducaometodos.javacore.src.academy.devdojo.maratona.java.Estudante;
import domain.Bintroducaometodos.javacore.src.academy.devdojo.maratona.java.ImpressoraEstudante;

public class EstudanteTest01 {
    public static void main(String[] args) {
        Estudante estudante01 = new Estudante();
        Estudante estudante02 = new Estudante();

        estudante01.nome = "Goku";
        estudante01.idade = 15;
        estudante01.sexo = 'M';

        estudante02.nome = "Sakura";
        estudante02.idade = 25;
        estudante02.sexo = 'F';

        ImpressoraEstudante impressora = new ImpressoraEstudante();

        impressora.imprime(estudante01); // Passando a referência do objeto e não a cópia como em tipos primitivos
        impressora.imprime(estudante02);
    }
}
```

As boas práticas de programação dizem que nunca devemos alterar o valor de um objeto dentro de uma função, mas sim retornar um novo objeto com os dados alterados.

### VarArgs

```java
package src.academy.devdojo.maratorna.java.javacore.Bintroducaometodos.test;

public class CalculadoraTest05 {
    public static void main(String[] args) {
        Calculadora calculadora = new Calculadora();
        calculadora.somaVarArgs(1, 2, 3, 4, 5, 6);
    }
}

public class Calculadora {
    public void somaVarArgs(int... numeros) { // deve ser o ultimo parametro
        int soma = 0;
        for (int num : numeros) {
            soma += num;
        }
        System.out.println(soma);
    }
}
```

## Modificador de acesso private get e set

Acoplamento é o quanto uma classe conhece da outra, por exemplo:

- Observe no código abaixo que temos duas classes `PessoaTest01` e `Pessoa`.
- Note que as classes estão fortemente conectadas, pois estamos acessando nossos atributos de forma publica
- Note também que passamos a idade como `-1` e tudo funcionou normalmente.


```java
package src.academy.devdojo.maratorna.java.javacore.Bintroducaometodos.domain;

public class Pessoa {
    public String nome;
    public int idade;

    public void imprime() {
        System.out.println(this.nome);
        System.out.println(this.idade);
    }
}
```

```java
public class PessoaTest01 {
    public static void main(String[] args) {
        Pessoa pessoa = new Pessoa();
        pessoa.nome = "Jiraya";
        pessoa.idade = -1;
    }
}
```

Então recapitulando, acoplamento é o quando uma classe conhece de outra, então para isso nós temos os modificadores de acesso.

### Private


Os modificadores de acesso do tipo `private` nós ajudam a manter um baixo acoplamento.

- Atributos com o modificador de acesso `private` significa que o atributo só poderá ser acessado pelo próprio objeto e não pode ser alterado por outras classes.
- Neste caso agora vamos perder o acesso a essas informações de fora da classe `Pessoa`.

```java
package src.academy.devdojo.maratorna.java.javacore.Bintroducaometodos.domain;

public class Pessoa {
    private String nome;
    private int idade;

    public void imprime() {
        System.out.println(this.nome);
        System.out.println(this.idade);
    }
}
```

### Set

- Agora que temos as propriedades `nome` e `idade` como `privates` não podemos acessá-las através da classe `PessoaTest01`, para isso vamos criar métodos `publicos` que nos permitem acessar essas propriedades de uma forma segura.
- Então vamos criar métodos seguindo as boas práticas da seguinte forma `public setNome(String nome)` e `public setIdade(int idade)`.
- E agora a classe pode ter validações no momento de adicionar um nome ou idade;
- Então baixamos o acoplamento, pois alteramos o código e não importa o que acontece dentro.

```java
package src.academy.devdojo.maratorna.java.javacore.Bintroducaometodos.domain;

public class Pessoa {
    private String nome;
    private int idade;

    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @param idade
     */
    public void setIdade(int idade) throws Exception {
        if (idade < 0) {
            throw new Exception("Idade inválida!");
        }
        this.idade = idade;
    }

    public String getNome() {
        return this.nome;
    }

    public int getIdade() {
        return this.idade;
    }

    public void imprime() {
        System.out.println(this.nome);
        System.out.println(this.idade);
    }
}
```

e na nossa classe de Test:

```java
package src.academy.devdojo.maratorna.java.javacore.Bintroducaometodos.test;

import domain.Bintroducaometodos.javacore.src.academy.devdojo.maratona.java.Pessoa;

public class PessoaTest01 {
    public static void main(String[] args) {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Marcos");
        try {
            pessoa.setIdade(-10);
            pessoa.imprime();
        } catch (Exception e) {
            System.out.println("Erro: " + e);
        }

        System.out.println("O nome da pessoa é: " + pessoa.getNome());
    }
}
```

## Sobrecarga de métodos

Sobrecarga de métodos são métodos com o mesmo nome, porém com quantidades ou tipos de parametros diferentes:

Veja o exemplo abaixo onde temos duas funções init com parametros diferentes.

```java
 package src.academy.devdojo.maratorna.java.javacore.Csobrecargametodos.domain;

import java.lang.invoke.StringConcatFactory;

public class Anime {
    private String nome;
    private String tipo;
    private int episodios;
    private String genero;

    public void init(String nome, String tipo, int episodios) {
        this.nome = nome;
        this.tipo = tipo;
        this.episodios = episodios;
    }

    public void init(String nome, String tipo, int episodios, String genero){
        this.init(nome, tipo, episodios);
        this.genero = genero;
    }
}
```

## Construtores

- Toda as vezes que estamos criando um objeto na verdade estamos construindo um objeto, que possui um construtor mesmo que nós não definimos ele.
- Quando usamos a palavra `new` estamos chamando o construtor de um objeto.

No Java o construtor é criado da seguinte forma:
- Temos o modificador de acesso
- Temos o nome da classe, seguido de parenteses e depois um par de chaves

```java
public class Anime{
    public Anime() {} // constructor 
}
```

O JAVA TEM UMA REGRA:

`SE VOCÊ NÃO ESCREVER UM CONSTRUTOR EM SUA CLASSE, O JAVA VAI ADICIONAR UM PARA VOCÊ NO MEIO DA COMPILAÇÃO.`

O construtor é executado antes de qualquer método da nossa classe.

### Vantagens

Os contrutores nos trazem algumas vantagens como forçar a seguir algumas regras, como por exemplo exigir um nome para criação de um objeto.


```java
package src.academy.devdojo.maratorna.java.javacore.Dconstrutores.domain;

public class Anime {
    private String nome;
    private String tipo;
    private int episodios;
    private String genero;

    public Anime(String nome, String tipo, int episodios, String genero) { // Constructor
        this.nome = nome;
        this.tipo = tipo;
        this.episodios = episodios;
        this.genero = genero;
    }
}
```

### Sobrecarga de construtores

Da mesma forma que temos a sobrecarga de métodos, temos também a sobrecarga de construtores.

```java
package src.academy.devdojo.maratorna.java.javacore.Dconstrutores.domain;

public class Anime {
    private String nome;
    private String tipo;
    private int episodios;

    private String genero;

    public Anime(String nome, String tipo, int episodios, String genero) { // Constructor
        this.nome = nome;
        this.tipo = tipo;
        this.episodios = episodios;
        this.genero = genero;
    }

    public Anime(String nome) { // Sobrecarga do constructor
        this.nome = nome;
    }
}
```

```java
package src.academy.devdojo.maratorna.java.javacore.Dconstrutores.domain;

public class Anime {
    private String nome;
    private String tipo;
    private int episodios;

    private String genero;

    private String estudio;

    public Anime(String nome, String tipo, int episodios, String genero) {
        this(); 
        this.nome = nome;
        this.tipo = tipo;
        this.episodios = episodios;
        this.genero = genero;
    }

    public Anime(String nome, String tipo, int episodios, String genero, String estudio) {
        this(nome, tipo, episodios, genero);
        this.estudio = estudio;
    }

    public Anime(String nome) {
        this.nome = nome;
    }

    public Anime() {
    }
}
```

### Regras 

- o `this()` só poder chamado dessa forma dentro de construtores 
- Quando precisamos chamar outro construtor obrigatóriamente precisa ser a primeira linha do construtor.

### Pegadinha em certificações

O código abaixo não é um construtor

```java
public class Anime {
    public void Anime() { // É um método e não um construtor devido ao void 
        
    }
}
```

## Blocos de inicialização

- O Java possui um recurso chamado de Blocos de inicialização, podem ser colocados em qualquer lugar da classe, porém por convenção é colocado logo após os atribuitos e antes do construtor:
- No bloco abaixo temos um bloco de inicialização de instância, é executado toda vez que o objeto é criado.
- É executado antes do construtor.

```java
public class Anime {
    private String nome;
    private int[] episodios;

    {
        System.out.println("Dentro do bloco de incialização");
    }

    public Anime() {
        episodios = new int[100];
        for (int i = 0; i < episodios.length; i++) {
            episodios[i] = i + 1;
        }
        for (int episodio : this.episodios) {
            System.out.println(episodio);
        }
    }
}
```

A diferença de um bloco de incialização é um construtor é que é que não precisamos ficar replicando código, então imagine no caso do exemplo anterior, onde temos que iniciar um array de episodios com 100 episodios, isso teria que ser replicado em todos os construtores, porém usando o bloco de incialização não precisamos ficar replicando código.

```java
public class Anime {
    private String nome;
    private int[] episodios;
    // Dentro do bloco de inicilização, independente de qual construtor chamarmos ele será executado
    {
        episodios = new int[100];
        for (int i=0; i< episodios.length; i++) {
            episodios[i] = i+1;
        }
    }

    public Anime() {
        for (int episodio: this.episodios) {
            System.out.println(episodio);
        }
    }

    public String getNome() {
        return nome;
    }

    public int[] getEpisodios() {
        return episodios;
    }
}
```


A inicialização dos Objetos acontece antes mesmo da execução do construtor:

```java
public class Anime {
    private String nome;
    private int[] episodios = {1,2,3,4,5,6,7,8,9,10,11,12}; // Acontece antes do construtor

    public Anime() {
        for (int episodio: this.episodios) {
            System.out.println(episodio);
        }
    }
}
```

## Modificador static

Modificador estatico faz com que o atritbuto pertença a classe e não ao objeto. Então quando criamos um atributo do tipo static esse atributos pertence a classe e não ao objeto.

Quando falamos que o atributo pertence a classe estamos dizendo que para acessarmos esse atributo vamos usar o nome da classe ao inves do this.

Veja o exemplo abaixo:

- Temos um atributo publico e estatico chamado `velocidadeLimite`
- Quando acessamos ele no metodo `imprime()` usamos o nome da Classe . se atributo `Carro.velocidade limite` neste caso não se usa o this.

```java
public class Carro {
    private String nome;
    private double velocidadeMaxima;
    public static double velocidadeLimite = 250; // faz parte da instância da classe e não ao objeto

    public Carro(String nome, double velocidadeMaxima) {
        this.nome = nome;
        this.velocidadeMaxima = velocidadeMaxima;
    }

    public void imprime() {
        System.out.println("Velocidade limite " + Carro.velocidadeLimite); // Acessado via nome da classe
    }
}
```

A chamada fica da mesma forma no método main

```java
public class CarroTest01 {
    public static void main(String[] args) {
        Carro carro1 = new Carro("BMW", 280);
        Carro carro2 = new Carro("Mercedes", 275);
        Carro carro3 = new Carro("Audi", 290);

        Carro.velocidadeLimite = 180; // Todos os objetos do tipo Carro vão ter 180

        carro1.imprime();
        carro2.imprime();
        carro3.imprime();
    }
}
```

Note que todos os objetos do tipo Carro sofreram a alteração.

### Métodos estáticos

Podemos observar no exemplo abaixo que métodos estáticos não podem conter chamadas usando o this, pois o método pertence a classe e não ao objeto.

```java
package src.academy.devdojo.maratorna.java.javacore.Fmodificadorestatico.domain;

public class Carro {
    private static double velocidadeLimite = 250; // faz parte da instância da classe e não ao objeto

    public Carro(String nome, double velocidadeMaxima) {
        this.nome = nome;
        this.velocidadeMaxima = velocidadeMaxima;
    }

    public void imprime() {
        System.out.println("Velocidade limite " + src.academy.devdojo.maratona.java.javacore.Fmodificadorestatico.domain.Carro.velocidadeLimite);
    }

    // Método estático
    public static void setVelocidadeLimite(double velocidadeLimite) {
        src.academy.devdojo.maratona.java.javacore.Fmodificadorestatico.domain.Carro.velocidadeLimite = velocidadeLimite;
    }
}
```

Regra: Não podemos usar variáveis que não são estáticas dentro de métodos estáticos

As boas práticas dizem que você deve criar um método estático quando os métodos não acessam uma variável ou atributo da instacia.

Então se você possui um método que não acessa nenhuma variavel da instancia do objeto esse método deve ser static.

Para exemplificar melhor, vamos voltar na classe `Calculadora`.
- Note que os métodos dessa classe não acessam nenhum atributo de instancia, logo esses métodos poderiam ser estáticos

```java
package src.academy.devdojo.maratorna.java.javacore.Bintroducaometodos.domain;

public class Calculadora {

    public void somaDoisNumeros() {
        System.out.println(10 + 10);
    }

    public void subtraiDoisNumeros() {
        System.out.println(20 - 10);
    }

    public void multiplicaDoisNumeros(int num1, int num2) {
        System.out.println(num1 * num2);
    }

    public double divideDoisNumeros(double num1, double num2) {
        return num1 / num2;
    }
}
```

### Blocos de inicilização estáticos

Blocos de inicialização estáticos tem as mesma regras para métodos estáticos:
- Dentro de bloco estáticos não podemos chamar variáveis de instancia, pois o objeto nem foi criado ainda
- Podemos ter mais de um método estático, eles vão ser executados na ordem em que aparecem

```java
public class Anime {
    private String nome;
    private static int[] episodios;

    /**
     * 0 - Bloco de inicialização é executado quando a JVM carregar classe
     * 1 - Alocado espaço em memória para o objeto
     * 2 - Cada atributo de classe é criado e inicializado com valores default ou que for passado
     * 3 - Bloco de inicialização é executado
     * 4 - Construtor é executado
     */
    static { // Não podemos acessar variáveis de instancia, pois o objeto nem foi criado
        episodios = new int[100];
        for (int i = 0; i < episodios.length; i++) {
            episodios[i] = i + 1;
        }
    }
    
    static {
        System.out.println("Bloco estático 2");   
    }

    static {
        System.out.println("Bloco estático 3");
    }
}
```

## Associação

Associação é o relacionamento entre dois objetos.

### Arrays com Objetos

No exemplo abaixo temos a associação de um array com Objetos, ou seja, temos um array que contém 3 objetos do tipo `Jodador`.

```java
public class JogadorTest01 {
    public static void main(String[] args) {
        Jogador joagador1 = new Jogador("Pelé");
        Jogador joagador2 = new Jogador("Romário");
        Jogador joagador3 = new Jogador("Cafú");

        Jogador[] jogadores = new Jogador[]{joagador1, joagador2, joagador3};

        for (Jogador jogador: jogadores) {
            jogador.imprime();
        }
    }
}
```

### Associação unidirecional um para muitos

Jogador 1 -- 1 Personagem <br>
Time 1 -- N Jogador <br>
Jogador N -- 1 Time <br>
Estudante N -- N Curso <br>

Vamos usar nossa classe `Jogador` novamente para exemplificar:

- Um Jogador ele faz parte apenas de um time
- E um time pode ter vários jogadores

```java
public class Jogador {
    private String nome;
    private Time time;
    public Jogador(String nome) {
        this.nome = nome;
    }

    public void imprime() {
        System.out.println(this.nome);
        if(time != null) {
            System.out.println(time.getNome());
        }
    }

    public void setTime(Time time) {
        this.time = time;
    }
}
```

### Associação unidirecional muitos para um

- Uma escola pode ter vários professores
- Mas o professor só pode ter uma escola

```java
public class Escola {
    private String nome;
    private Professor[] professores;

    public void imprime() {
        System.out.println("Nome: "+this.nome);
        if (professores == null) {
            return;
        }
        for (Professor professor: professores) {
            System.out.println("Professor: "+professor.getNome());
        }
    }

    public Escola(String nome) {
        this.nome = nome;
    }

    public Escola(String nome, Professor[] professores) {
        this.nome = nome;
        this.professores = professores;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Professor[] getProfessors() {
        return professores;
    }

    public void setProfessors(Professor[] professors) {
        this.professores = professors;
    }
}
```

```java
public class Professor {
    private String nome;

    public Professor(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
```

```java
public class EscolaTest01 {
    public static void main(String[] args) {
        Professor professor = new Professor("Jiraya Sensei");
        Professor[] professores = {professor};
        Escola escola = new Escola("Konoha", professores);

        escola.imprime();
    }
}
```

### Associação bidirecional

- Um time pode ter vários jogadores

```java
package src.academy.devdojo.maratorna.java.javacore.Gassociacao.test;

import domain.Gassociacao.javacore.src.academy.devdojo.maratona.java.Jogador;
import domain.Gassociacao.javacore.src.academy.devdojo.maratona.java.Time;

public class JogadorTest03 {
    public static void main(String[] args) {
        Jogador jogador1 = new Jogador("Cafú");
        Jogador jogador2 = new Jogador("Ronaldo");
        Jogador jogador3 = new Jogador("Romário");

        Jogador jogador4 = new Jogador("Messi");
        Jogador jogador5 = new Jogador("Di Maria");


        Time time = new Time("Brasil");
        Time time2 = new Time("Argentina");

        Jogador[] jogadores = {jogador1, jogador2, jogador3};
        Jogador[] jogadores2 = {jogador4, jogador5};

        jogador1.setTime(time);
        jogador2.setTime(time);
        jogador3.setTime(time);

        jogador4.setTime(time2);
        jogador5.setTime(time2);

        time.setJogadores(jogadores);
        time2.setJogadores(jogadores2);

        time.imprime();

        System.out.println("----------------");

        time2.imprime();
    }
}
```

## Herança

A herança no Java funciona da seguinte forma:

- Imagine que temos uma classe `Pessoa` que representa uma pessoa do mundo real.


```java
public class Pessoa {
    private String nome;
    private String cpf;
    private Endereco endereco;

    public void imprime() {
        System.out.println("Nome: " + this.nome);
        System.out.println("CPF: " + this.cpf);
        System.out.println("Endereço: " + this.endereco.getRua() + ", " + this.endereco.getCep());
    }
}
```

- Agora negócio requer um cadastro de funcionaŕio, então criamos a classe `Funcionario` que deve ter os seguintes atributos, `nome`, `cpf` e `endereço`.

Se analisarmos agora, um Funcionário é uma Pessoa, e neste nosso caso, repare que as propriedades do nosso Funcionário são semelhantes a Pessoa.

```java
public class Funcionario extends Pessoa { 

    private double salario;

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
}
```

- A classe `Funcionario` é uma classe mais especializada, ou seja quanto mais baixo uma classe está na herança, mais especializada ela é.
- A classe `Pessoa` é uma classe mais geneŕica, ou seja, quanto mais alta uma classe está na Herança, mais Genérica ela é.

### Quando utilizar herança

- Devemos usar a herança quando queremos extender a funcionalidade de uma classe e manter um relacionamento entre elas, Herança deixa o código fortemente acoplado.

- Algumas pessoas evitam o máximo o uso de herança, pois qualquer alteração feita na classe Pai de todas, devemos nos preoucupar com as classes que extendem dessa classe Genérica.

- Herança é algo muito poderoso em java, porém deve ser utilizado com responsabilidade.


### Sobrescrita de método

No Java podemos fazer a sobrescrita de métodos, para isso devemos escrever um método na classe filha da mesma forma que foi escrito na classe pai.

Dessa forma vamos alterar o comportamento da classe imprime

Vamos ver a classe pessoa:

- A classe `Pessoa` possui um método chamado imprime

```java
public class Pessoa {
    private String nome;
    private String cpf;
    private Endereco endereco;

    public void imprime() {
        System.out.println("Nome: "+ this.nome);
        System.out.println("CPF: "+ this.cpf);
        System.out.println("Endereço: "+this.endereco.getRua()+ ", "+this.endereco.getCep());
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
```

A classe `Funcionario` extende a classe pessoa, então ela possui o método imprime, porém podemos fazer a sobrescrita do método imprime

```java
public class Funcionario extends Pessoa {

    private double salario;

    public double getSalario() {
        return salario;
    }

    // Sobrescrevi o método da classe pai
    public void imprime() {
        super.imprime(); // Chamei o método imprime da classe pai através do super
        System.out.println("Salário: "+ this.getSalario());
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
}
```

**OBSERVAÇÃO**

`O JAVA NÃO POSSUI HERANÇA MULTIPLA`

### Protected

O modificador de acesso `protected significa que qualquer subclasse em qualquer pacote, vai ter acesso direto aos atributos como se o atributo estivesse na propria classe.

Quando adicionamos as propriedades com o modificado de acesso `protected` todas as classes filhas, e todo o pacote terá acesso a essas prorpriedades.

```java
public class Pessoa {
    protected String nome;
    protected String cpf;
    protected Endereco endereco;

    public void imprime() {
        System.out.println("Nome: "+ this.nome);
        System.out.println("CPF: "+ this.cpf);
        System.out.println("Endereço: "+this.endereco.getRua()+ ", "+this.endereco.getCep());
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
}
```

```java
public class Funcionario extends Pessoa {

    private double salario;

    public double getSalario() {
        return salario;
    }

    public void relatorioPagamento() {
        System.out.println("Eu " + this.nome); // Accesa o atributo como se fosse publico
    }
}
```

### Construtores

Agora vamos ver como os construtores se comportam quando temos herança.

```java
public class Pessoa {
    protected String nome;
    protected String cpf;
    protected Endereco endereco;

    public Pessoa(String nome) {
        this.nome = nome;
    }

    public Pessoa(String nome, String cpf) {
        this(nome);
        this.cpf = cpf;
    }
}
```

```java
public class Funcionario extends Pessoa {

    private double salario;

    public Funcionario(String nome) {
        super(nome);
    }
}
```

No Java todas as classes extendem de uma classe Pai chamada `Object`.


## Sequência de inicialização

Observe a imagem e vamos analisar alguns pontos importantes em relação a sequência de inicialização do java.

```java
public class HerancaTest02 {
    public static void main(String[] args) {
        Funcionario funcionario = new Funcionario("Jiraya");
    }
}
```

Na classe `HerancaTest02` estamos instanciando um novo `Funcionario`, com isso temos:

- Quando temos herança a super classe daquele herança sempre precisa ser inicializada primeiro.
- Instanciamos a classe Funcionario, porem ela extende de pessoa.

- 1 - Bloco de inicialização estático da super classe é executado quando a JVM carregar classe superclasse
- 2 - Bloco de inicialização estático da subclasse é executado quando a JVM carregar classe filha
- 3 - Alocado espaço em memória para o objeto da classe pai  
- 4 - Cada atributo da superclasse é criado e inicializado com valores default ou que for passado da classe pai
- 5 - Bloco de inicialização da superclasse é executado  na ordem em que aparece
- 6 - Construtor da superclasse é executado  
- 7 - Alocado espaço em memória para o objeto da subclasse
- 8 - Cada atributo da subclasse é criado e inicializado com valores default ou que for passado da classe pai
- 9 - Bloco de inicialização da subclasse é executado  na ordem em que aparece
- 10 - Construtor da subclasse é executado  

## Sobrescrita do método toString

Todo classe em java possui o método `toString` pois é um método da classe `Object` como qualquer classe extende de objeto e por isso que temos acesso ao toString.

Porém o método toString da classe Object não tem um retorno muito legal, então podemos fazer a sobrescrita do método toString e retornar os dados da forma que preferirmos.

É importante lembrar que para fazer a sobrescrita de qualquer método no java temos de seguir exatamente a forma como que foi escrito na classe Object, seguinda o mesmo nome e a mesma quantidade de parametros

Podemos também adicionar a anotação `@Override` no método.

```java
public class Anime {
    private String nome;

    @Override
    public String toString() {
        return "Anime{" +
                "nome='" + nome + '\'' +
                '}';
    }
}
```

## Modificador final

O modificador final nada mais são do que as constantes que usamos em outras linguagens, as contantes são valores que nunca vão mudar na nossa aplicação.

Toda constantes que vamos criar, precisam ter um valor inicial;

### Tipo primitivo

Existem alguns pontos importantes que devemos prestar atenção quando vamos declarar constantes no Java, observe o exemplo abaixo:

- Contantes devem ser declaradas e inicializadas, ou seja `devem ter um valor`.
- Por convenção deve colocar todas as `letras em Maisculas`
- As palavras devem ser separas por `_`
- Por padrão sempre vamos ver `static final


```java
public class Carro {
    private static final double VELOCIDADE_LIMITE = 250;
}
```

Outra forma de criar constantes é:

```java
static {
    VELOCIDADE_MAXIMA = 280;
} 
```

Para acessar atributos constantes devemos acessar através da classe

```java
public class CarroTest01 {
    public static void main(String[] args) {
        Carro carro = new Carro();
        System.out.println(Carro.VELOCIDADE_LIMITE);
    }
}
```

### Classes e métodos

### Classes

Existem casos que não queremos que nossa classe seja extendida, para isso podemos utilizar:


```java
public final class Carro { //Classes final não podem ser extendidas
    private String nome;
    public static final double VELOCIDADE_LIMITE = 250;
    public final Comprador COMPRADOR = new Comprador();

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
```

Uma exemplo de classe final no Java, é a própria classe `String` e existe um motivo para isso:

- Quando você permite a heranca ou você permite a sobrescrita, você está falando que toda classe que herdar dessa pode trocar o comportamento dos métodos e isso seria muito ruim neste caso.
- Então essa classe String precisa garantir seu uso, atualmente temos bilhoẽs de JVM do java instalada, e imagine se alterassemos o comportamento dessa classe a quantidade de problemas que ocorre em outros projetos...

### Métodos

O mesmo pode ser aplicado para métodos, ou seja, se um método é final então ele não pode ser sobrescrito por nenhuma classe.

Veja o exemplo abaixo na classe `Carro` temos o método `public final void imprime()`e esse método não pode ser sobrescrito.

```java
public class Carro {
    
    // método final não pode ser alterado
    public final void imprime() {
        System.out.println("Não posso ser alterado!");
    }
}
```

## Enumeração

Vamos primeiro analisar um problema que pode ocorrer em nossa aplicação:

- No exemplo abaixo temos um classe chamada `Cliente`
- O Cliente pode ser uma pessoa `física`, ou `jurídica`.
- Agora no que pode haver uma inconsistência de informações, pois temos vários padrões de escrita no momento em que instanciamos nosso `Cliente`.

```java
public class ClienteTest01 {
    public static void main(String[] args) {
        Cliente cliente1 = new Cliente("Tsubasa", "PESSOA_FISICA");
        Cliente cliente2 = new Cliente("Tsubasa", "PESSOA_JURIDICA");
        Cliente cliente3 = new Cliente("Tsubasa", "pessoa fisica");
    }
}
```

Para resolver esse problema podemos criar um `enumerator`: 

```java
public enum TipoCliente {
    PESSOA_FISICA,
    PESSOA_JURIDICA
}
```

Agora na nossa classe podemos chamar da seguinte forma:

```java
public class ClienteTest01 {
    public static void main(String[] args) {
       Cliente cliente1 = new Cliente("Paulo", TipoCliente.PESSOA_FISICA);
       Cliente cliente2 = new Cliente("Marcos", TipoCliente.PESSOA_JURIDICA);
       Cliente cliente3 = new Cliente("Maria", TipoCliente.PESSOA_FISICA);

        System.out.println(cliente1);
        System.out.println(cliente2);
        System.out.println(cliente3);
    }
}
```

Os enums podem ser criado dentro de uma classe, como no exemplo abaixo, óbvio que tem toda uma questão de cultura da empresa, questão pessoal etc.

Eu particularmente não gosto de colocar enums dentro de classes, prefiro separar em arquivos diferentes.

```java
public class Cliente {
    private String nome;
    private TipoCliente tipo;

    // ENUM
    private enum TipoPagamento {
        DEBITO, CREDITO
    }

    private TipoPagamento tipoPagamento;
}
```

### Construtores e atributos

Observações importantes sobre enums:

- Todo enum possui um construtor. 
- E podemos criar atributos dentro de um enum.

```java
public enum TipoCliente {
    PESSOA_FISICA(1),
    PESSOA_JURIDICA(2);

    public final int VALOR;

    TipoCliente(int valor) {
        this.VALOR = valor;
    }
}
```

### Sobrescrita de métodos em enums

```java
public enum TipoPagamento {
    DEBITO {
        @Override
        public double calcularDesconto(double valor) {
            return valor * 0.1;
        }
    }, CREDITO {
        @Override
        public double calcularDesconto(double valor) {
            return valor *0.05;
        }
    };

    // Método sem corpo, criado apenas para ser sobrescrito.
    public abstract double calcularDesconto(double valor);
}
```

## Classes abstratas

Classes abstratas foram criadas para resolver um problema de design, ou desenho de código.

Imagine que você tem sua empresa, e sua empresa tem funcionários e esses funcionários tem cargos, gerente, desenvolvedores, supervisor etc...

O Funcionário é algo abstrato, é como se fosse um template, o funcionário em si não existe, o que existe é uma das implementações do funcionário, gerente, desenvolvedor, etc..

Quando criamos classes abstratas, significa que não podemos criar objetos a partir dela, não podemos dar new nela, ela não pode ser instanciada.

```java
// Classe abstrata não pode ser instanciada
public abstract class Funcionario {
    protected String nome;
    protected double salario;

    public Funcionario(String nome, double salario) {
        this.nome = nome;
        this.salario = salario;
    }

    @Override
    public String toString() {
        return "Funcionario{" +
                "nome='" + nome + '\'' +
                ", salario=" + salario +
                '}';
    }
}
```

Uma observação, não existe classe abastrata final, pois se uma classe é abstrata ela foi feita para ser extendida, e uma classe final não pode ser extendida, portanto isso `public abstract final` não existe.

Agora neste momento temos que criar objetos que extendem dessa classe.

```java
public class FuncionarioTest01 {
    public static void main(String[] args) {
        Gerente gerente = new Gerente("Nami", 5000);
        Desenvolvedor desenvolvedor = new Desenvolvedor("Touya", 12000);
        
        System.out.println(gerente);
        System.out.println(desenvolvedor);
    }
}
```

## Métodos abstratos

Método abstratos só podem existir dentro de classes abstratas

```java
public abstract class Funcionario {
    protected String nome;
    protected double salario;

    public Funcionario(String nome, double salario) {
        this.nome = nome;
        this.salario = salario;
        calculaBonus();
    }

    public abstract void calculaBonus();
}
```

Nunca vamos ter um método abstrato dentro de uma classe concreta

Classes abstratas podem ter métodos tanto métodos abastratos como métodos concretos.

## Métodos abstratos regras

Se tivermos uma cadeia de classes abstratas, a primeira classe concreta deve implementar os métodos abstratos.

Veja o exemplo:

- Temos uma classe abstrata  `Pessoa` que possui o método impriem que é uma método abstrato.
- Então as classes que extenderem pessoa devem implementar o método `imprime`.

```java
public abstract class Pessoa {
    public abstract void imprime();
}
```

Temos a classe `Funcionario` que extende de `Pessoa`, porém a classe Funcionario também é abstrata então ela não pode implementar o método imprime

```java
public abstract class Funcionario extends Pessoa {
    protected String nome;
    protected double salario;

    public Funcionario(String nome, double salario) {
        this.nome = nome;
        this.salario = salario;
        calculaBonus();
    }

    public abstract void calculaBonus();
}
```

Agora temos mais uma classe `Desenvolvedor` que extende de `Funcionario`, porém a classe Desenvolvedor é uma classe concreta, então ela deve implementar o método imprime, pois é a primeira classe concreta da arvore.

```java
public class Desenvolvedor extends Funcionario{
    public Desenvolvedor(String nome, double salario) {
        super(nome, salario);
    }

    @Override
    public void calculaBonus() {
        this.salario = this.salario + this.salario * 0.05;
    }

    @Override
    public String toString() {
        return "Desenvolvedor{" +
                "nome='" + nome + '\'' +
                ", salario=" + salario +
                '}';
    }

    @Override
    public void imprime() {
    }
}
```

O método só vai ser implementado por uma classe.

## Interfaces

Interfaces quando o Java foi criado tinham o intuito de servir como contrato, no fim das contas as classes abstratas são interfaces.

Em interfaces não se utiliza a palavra final, pois contratos são criados para serem implementados.

Por padrão no Java todas os métodos de uma interface são `public abstract`portanto na declaração de uma interface devemos colocar apenas o retorno e nome da função `void DataLoader()`;

Criando uma interface:

```java
public interface DataLoader {
    void load();
}
```

Implementando a interface `DataLoader`

```java
public class DatabaseLoader implements DataLoader{

    @Override
    public void load() {
        System.out.println("Carregando dados do banco");
    }
}
```

Implementando uma classe que carrega os dados de um arquivo

```java
public class FileLoader implements DataLoader {
    @Override
    public void load() {
        System.out.println("Carregando dados de um arquivo");
    }
}
```

Chamando na classe principal

```java
public class DataLoaderTest01 {
    public static void main(String[] args) {
        DatabaseLoader databaseLoader = new DatabaseLoader();
        FileLoader fileLoader = new FileLoader();

        databaseLoader.load();
        fileLoader.load();
    }
}
```

### Implementqando múltiplas interfaces

Uma das vantagens de usar interfaces é que podemos prover implementação para múltiplas interfaces

```java
public class FileLoader implements DataLoader, DataRemover {
    @Override
    public void load() {
        System.out.println("Carregando dados de um arquivo");
    }

    @Override
    public void remove() {
        System.out.println("Removendo dados de um arquivo");
    }
}
```

A partir do Java 8 foi inserida na linguagem que uma interface pode ter métodos com implementação.

<b>Mas porque uma classe pode ter um método com implementação?</b>

- Bom existem interfaces no java que são usadas pelo mundo todo, imagine que os arquitetos do Java adicionam mais um método abstrato nessa interface, o planeta todo vai ter que fazer a implementação desse novo método, então isso quebraria todos os sistemas java.

- Então eles precisavam de uma solução para atualizar a linguagem sem quebrar as versões anteriores, ou seja, a linguagem tem que melhorar mas sem quebrar o código de quem usa.

- Então a solução foi criar método concretos em uma interface, com isso não há necessidade de ser implementado por quem já está usando.

Então se precisarmos alterar um interface que é muito ultizada no nosso programa, sem quebrar as implementações que já foram realizadas podemos criar um método com implementação na nossa interface.


Veja no exemplo abaixo que incluimos o método checkPermission na nossa interface, e as classes que implementam essa interface não apresentaram erro algum.

```java
public interface DataLoader {
    void load();

    // Método com implementação
    default void checkPermission() {
        System.out.println("Fazendo checagem de permissões");
    }
}
```

Isso não impede de fazer a sobrescrita do método em quem implementa essa interface

```java
public class FileLoader implements DataLoader, DataRemover {
    @Override
    public void load() {
        System.out.println("Carregando dados de um arquivo");
    }

    @Override
    public void remove() {
        System.out.println("Removendo dados de um arquivo");
    }

    // Sobrescreve o método
    @Override
    public void checkPermission() {
        System.out.println("Checando permissão no arquivo");
    }
}
```

### Atributos e método estáticos

### Atributos

Uma interface no java pode ter atributos porém existem algumas regras para isso:

- Por padrão todos os atributos de uma interface são  `constantes`

```java
public class DatabaseLoader implements DataLoader, DataRemover {
    public static final int MAX_DATA_SIZE = 10;
}
```

já que os atributos por padrão são constantes podemos omitir algumas informações.

### Métodos estáticos

- A partir do Java 8 temos a possibilidade de uma interface poder adicionar métodos estáticos.
- Métodos estáticos nunca serão sobrescritos, pois pertencem a interface.

```java
public class DatabaseLoader implements DataLoader, DataRemover {

    public static final int MAX_DATA_SIZE = 10;

    // Não pode ser sobrescrito
    static void retrieveMaxDataSize() {
        System.out.println("Dentro do retrieveMaxDataSize na interface");
    }
}
```

### Observações:

- Vale lembrar que não é possível criar objetos a partir de uma interface.
- Cuidados com os modificadores de acesso

## Polimorfismo

Para entendermos o que é polimorfismo, vamos criar algumas classes:

Primiero vamos criar uma classe abstrata chamada `Produto`:

```java
public abstract class Produto {
    protected String nome;
    protected double valor;

    public Produto(String nome, double valor) {
        this.nome = nome;
        this.valor = valor;
    }

    public String getNome() {
        return nome;
    }

    public double getValor() {
        return valor;
    }
}
```

Agora vamos criar uma classe chamada `Computador` que extende de `Produto`:

```java
public class Computador extends Produto {

    public Computador(String nome, double valor) {
        super(nome, valor);
    }
}
```

E vamos criar também outra classe chamada `Tomate` que também extende de produto

```java
public class Tomate extends Produto{

    public Tomate(String nome, double valor) {
        super(nome, valor);
    }
}
```

Agora imagine que nossos produtos precisam ser taxados, ou precisam de um calculo de imposto.

Para resolvermos esse problema temos algumas opções e vamos a uma aqui:

vamos criar uma interface chamada `Imposto` com um método chamado `calcularImposto e vamos dizer que Produto vai implementar Imposto, ou seja, todo produto tem que calcular seu imposto:

```java
public interface Imposto {
    public double calcularImposto();
}
```

Agora vamos dizer que a classe `Produto` vai implementar a interface `Imposto`.

```java
public abstract class Produto implements Imposto {
    protected String nome;
    protected double valor;

    public Produto(String nome, double valor) {
        this.nome = nome;
        this.valor = valor;
    }

    public String getNome() {
        return nome;
    }

    public double getValor() {
        return valor;
    }
}
```

Agora que a classe Produto implementa a interface Imposto, suas subclasses devem implementar o método `calcularImposto`.

```java
public class Computador extends Produto {
    public static final double IMPOSTO_POR_CENTO = 0.21;

    public Computador(String nome, double valor) {
        super(nome, valor);
    }

    @Override
    public double calcularImposto() {
        System.out.println("Calculando imposto do Computador");
        return this.valor * IMPOSTO_POR_CENTO;
    }
}
```

```java
public class Tomate extends Produto{
    public static final double IMPOSTO_POR_CENTO = 0.06;

    public Tomate(String nome, double valor) {
        super(nome, valor);
    }

    @Override
    public double calcularImposto() {
        System.out.println("Calculando imposto do Tomate");
        return this.valor * IMPOSTO_POR_CENTO;
    }
}
```

Agora vamos criar uma classe de serviço `CalculaImposto`:

```java
public class CalculadoraImposto {
    public static void calcularImpostoComputador(Computador computador) {
        System.out.println("Relatório de imposto do computador");
        double imposto = computador.calcularImposto();

        System.out.println("Computador: "+ computador.getNome());
        System.out.println("Valor: "+computador.getValor());
        System.out.println("Imposto: "+ NumberFormat.getCurrencyInstance().format(computador.calcularImposto()));
    }

    public static void calcularImpostoTomate(Tomate tomate) {
        System.out.println("Relatório de imposto do computador");
        double imposto = tomate.calcularImposto();

        System.out.println("Tomate: "+ tomate.getNome());
        System.out.println("Valor: "+tomate.getValor());
        System.out.println("Imposto: "+ NumberFormat.getCurrencyInstance().format(tomate.calcularImposto()));
    }
}
```

### Funcionamento

Até o momento trabalhamos da seguinte forma:

    Computador computador = new Computador();

Porém com o polimorfismo, vamos trabalhar com multiplas formas ou seja, polimorfismo significa que `podemos trocar o tipo da variável de referência` mas ainda continuar utilizando outros objetos, e isso só é aplicado na Herança.


- Note no exemplo abaixo que temos a mesma variável (controle) que consegue enviar sinais tanto para a TV 2019 quanto para a TV 2020.
- Note também que não é possível o controle enviar um sinal para a TV 2020 executar a ação do Youtube


<img alt="Polimorfismo funcionamento" src="./public/images/polimorfismo.png" width="850">

Após os exemplo vamos ver como funciona isso no mundo real:

Vamos criar uma classe nova de teste:

```java
public class ProdutoTest02 {
    public static void main(String[] args) {
        Produto produto = new Computador("Ryzen 9", 3000); // Instancia objeto do tipo computador
        System.out.println("Nome: "+ produto.getNome());
        System.out.println("Valor: "+produto.getValor());
        System.out.println("Imposto:"+ produto.calcularImposto());// executa método da classe Computador

        System.out.println("=======================================");

        Produto produto2 = new Tomate("Tomate verde", 10); // instancia objeto do tipo tomate
        System.out.println("Nome: "+ produto2.getNome());
        System.out.println("Valor: "+produto2.getValor());
        System.out.println("Imposto:"+ produto2.calcularImposto()); // executa método da classe Tomate
    }
}
```

<img alt="Polimorfismo funcionamento" src="./public/images/polimorfismo2.png" width="850">

### Parametros polimorficos

Usando o polimorfismo podemos melhorar a nossa classe de serviço da seguinte forma:

```java
public class CalculadoraImposto {
    public static void calcularImposto(Produto produto) {
        System.out.println("Relatório de imposto");
        System.out.println("Produto: "+produto.getNome());
        System.out.println("Preço: "+NumberFormat.getCurrencyInstance().format(produto.getValor()));
        System.out.println("Produto: "+ NumberFormat.getCurrencyInstance().format(produto.calcularImposto()));
    }
}
```

- Agora temos um classe bem mais enxuta com apenas um método que faz a mesma coisa da anterior, calcula imposto.
- Facilita a manutenção dessa classe em um futuro.

Isso também facilita a criação de novos objetos, por exemplo vamos criar uma classe nova chamada `Televisao` que também extende de Produto


### Cast e instanceof

```java
public class CalculadoraImposto {
    public static void calcularImposto(Produto produto) {
        System.out.println("Relatório de imposto");
        System.out.println("Produto: "+produto.getNome());
        System.out.println("Preço: "+NumberFormat.getCurrencyInstance().format(produto.getValor()));
        System.out.println("Produto: "+ NumberFormat.getCurrencyInstance().format(produto.calcularImposto()));

        if (produto instanceof Tomate) {
            Tomate tomate = (Tomate) produto;
            System.out.println(tomate.getDatValidade());
        }
    }
}
```

## Programação Orientada a interfaces

Vamos ver como funciona a programação orientada a objetos:

Vamos criar uma um pacote chamado de `Repository` dentro criar uma `interface` Chamada `RepositoryInterface`:

```java
public interface Repository {
    void save();
}
```

Agora dentro da pasta service, vamos criar 3 classes que vão implementar a interface `RepositoryInterface`. 

```java
public class DatabaseRepository implements RepositoryInterface {
    @Override
    public void save() {
        System.out.println("Salvando no banco de dados");
    }
}
```

```java
public class MemoryRepository implements RepositoryInterface {
    @Override
    public void save() {
        System.out.println("Salvando em memória");
    }
}
```

```java
public class FileRepository implements RepositoryInterface {
    @Override
    public void save() {
        System.out.println("Salvando em um arquivo");
    }
}
```

Usando:

Agora na nossa classe principal `RepositoryTest` vamos criar um novo objeto do tipo `DatabaseRepository`;

```java
public class RepositoryTest {
    public static void main(String[] args) {
        DatabaseRepositoryInterface databaseRepository = new DatabaseRepositoryInterface();
        databaseRepository.save();
    }
}
```

ao executarmos nossa classe principal ela vai chamar o objeto `databaseRepository.save()`que salva as informações no banco de dados.

Agora imagine que temos que alterar o nosso tipo de armazenamento para Arquivos.

Então ao invés de chamarmos a classe principal como no exemplo anterior podemos colocar o tipo mais genérico antes:

```java
public class RepositoryTest {
    public static void main(String[] args) {
        
        // tipo mais genérico
        RepositoryInterface repository = new DatabaseRepository(); // Abordagem melhor
        repository.save();
    }
}
```

```java
public class RepositoryTest {
    public static void main(String[] args) {

        // tipo mais genérico
        RepositoryInterface repository = new FileRepository(); // Abordagem melhor
        repository.save();
    }
}
```

com isso, agora podemos alterar facilmente nosso código, basta alterar o tipo do objeto 

```java
RepositoryInterface repository = new DatabaseRepository();
RepositoryInterface repository = new FileRepository();
RepositoryInterface repository = new MemoryRepository();
```
