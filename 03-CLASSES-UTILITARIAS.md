## Classes Utilitárias

## Sumário

1. [Wrappers](#wrappers)
2. [Strings](#strings)
3. [Date](#date)
4. [Calendar](#calendar)
5. [Date Format](#date-format)
6. [Internacionalização Números com Locale](#internacionalização-números-com-locale)
7. [Internacionalização Moeda com Locale](#internacionalização-moeda-com-locale)
8. [Simple Date Format](#simple-date-format)
9. [Local Date](#local-date)
10. [Local Time](#local-time)
11. [Local DateTime](#local-datetime)
12. [Instant](#instant)
13. [Duration](#duration)
14. [Period](#period)
15. [ChronoUnit](#chronoUnit)
16. [TemporalAdjusters](#temporaladjusters)
17. [TemporalAdjuster](#temporaladjuster)
18. [ZonedDateTime Zoneld OffsetDateTime](#zoneddatetime-zoneld-offsetdatetime)
19. [DateTimeFormatter](#datetimeformatter)
20. [ResourceBundle](#resourcebundle)
21. [Regex Pattern e Matcher](#regex-pattern-e-matcher)
22. [Regex Pattern e Matcher-Meta characteres](#regex-pattern-e-matcher-meta-characteres)
23. [Regex Pattern e Matcher-Range](#regex-pattern-e-matcher-range)
24. [Regex Pattern e Matcher-Quantificadores](#regex-pattern-e-matcher-quantificadores)
25. [Regex Pattern e Matcher-Anchor](#regex-pattern-e-matcher-anchor)
26. [Scanner Tokens e Delimitadores](#scanner-tokens-e-delimitadores)
26. [IO File](#io-file)
26. [IO File Writer](#io-file-writer)
26. [IO File Reader](#io-file-reader)
26. [IO BufferedWriter](#io-bufferedwriter)
26. [IO BufferedReader](#io-bufferedReader)
26. [IO File para diretórios](#io-file-para-diretórios)
26. [NIO Path Paths Files](#nio-path-paths-files)
27. [NIO Normalização](#nio-normalização)
27. [NIO Resolvendo Paths](#nio-resolvendo-paths)
27. [NIO Relativize](#nio-relativize)
27. [NIO BasicFileAttributes](#nio-basicfileattributes)
27. [NIO DosFileAttribute](#nio-dosfileattribute)
27. [NIO PosixFileAttributes](#nio-posixfileattributes)
27. [NIO DirectoryStream](#nio-directorystream)
27. [NIO SimpleFileVisitor](#nio-simplefilevisitor)
27. [NIO PAthMatcher](#nio-pathmatcher)
27. [NIO ZipOutStream](#nio-zipoutstream)
27. [Serialization](#serialization)


## Wrappers

Wrappers nada mais são do que objetos que vão encapsular os tipos primitivos, temos 8 tipos primitivos:

```java
byte byteP = 1;
short shortp = 1;
int intP = 1;
long longP = 10L;
float floatP = 10F;
double doubleP = 10D;
char charP = 'W';
boolean booleanP = false;
```

O Java é orientado a objetos e os tipos primitivos são só tipos primitivos ou seja, não temos nada em memória além dos valores representando os bits.

No Java não tinhamos uma forma de trabalhar com valores passados por referência para tipos primitivos, então o pessoal do Jva criou as classes que chamamos de `Wrappers` que na verdade só vão encapsular os tipos primitivos e transformá-los em objetos.


### Como criar Wrapper

Para criarmos um `Wrapper basta pegar o nome do tipo primitivo e começar com letra `maiscula` com duas exceções que são para `int` e `char`.

```java
Byte byteW = 1;
Short shortW = 1;
Integer intW = 1;
Long longW = 10L;
Float floatW = 10F;
Double doubleW = 10D;
Character charW = 'W';
Boolean booleanW = false;
```

Agora temos classes que representam os tipos primitivos.

### Autoboxing e Unboxing

- `Autoboxing`: É quando temos um tipo primitivo e faz o Java transformar ele para um tipo `Wrapper`.
- `Unboxing`: É o contrário do Autoboxing o Java se encarrega de transformar o tipo Wrapper para tipo primitivo.
  - int i = intW; // unboxing


```java
int i = intW; //unboxing
// Integer intW2 = Integer.parseInt("2");
boolean verdadeiro = Boolean.parseBoolean("TruE");
System.out.println(Character.isDigit('A'));
System.out.println(Character.isDigit('9'));
System.out.println(Character.isLetterOrDigit('!'));
System.out.println(Character.isUpperCase('A'));
System.out.println(Character.isLowerCase('a'));
System.out.println(Character.toUpperCase('a'));
System.out.println(Character.toLowerCase('A'));
```

## Strings

- Strings no Java são imutáveis.
- O Java possui o que chamamos de String pool (piscina de strings)

<img alt="String pool" src="./public/images/string-pool.png" width="850">

```java
public class StringTest01 {
    public static void main(String[] args) {
        String nome = "André"; //String constant pool
        String nome2 = "André";

        // Verifica se as string fazem referência para o mesmo local na memória
        System.out.println(nome == nome2);
    }
}
```

Como as strings são imutávies não é possível alterar seu valor.

### Método úteis para string

```java
public class StringTest02 {
    public static void main(String[] args) {
        String nome = "Luffy";
        System.out.println(nome.charAt(0)); // Uma posição no array
        System.out.println(nome.length());
        System.out.println(nome.replace("f", "l"));
        System.out.println(nome.toLowerCase());
        System.out.println(nome.toUpperCase());

        String numeros = "012345";
        System.out.println(numeros.length());
        System.out.println(numeros.substring(0, 2));
        System.out.println(nome.trim()); // remove espaços em branco
    }
}
```

### Desempenho

A performance das string no java vai diminuindo conforme vai aumentando o tamanho da quantidade de strings no seu programa:


```java
public class StringPerformanceTest {
    public static void main(String[] args) {
        long inicio = System.currentTimeMillis();
        concatString(100_000);
        long fim = System.currentTimeMillis();
        System.out.println("Tempo gasto para String: "+ (fim - inicio) + "ms");
    }

    private static void concatString(int tamanho) {
        String texto = "";

        for (int i=0; i< tamanho; i++) {
            texto += i; // 0
        }
    }
}
```

Note que o tempo aumenta muito conforme aumentamos a quantidade, imagine uma aplicação grande com milhares de usuários, isso pode piorar cada vez mais. 

Para resolver esse problema de performance o Java criou algumas classes ajudam com essa parte das String e uma delas é a `String Builder`

```java
public class StringPerformanceTest {
    public static void main(String[] args) {
        long inicio = System.currentTimeMillis();
      concatStringBuilder(100_000);
        long fim = System.currentTimeMillis();
        System.out.println("Tempo gasto para StringBuilder: "+ (fim - inicio) + "ms");
    }

    private static void concatStringBuilder(int tamanho) {
        StringBuilder sb = new StringBuilder(tamanho);

        for (int i=0; i< tamanho; i++) {
            sb.append(i);
        }
    }
}
```

Podemos notar que com o StringBuilder o processo é muito mais rápido

Essa classe serve para quando trabalhamos em ambientes com multiplas threads ao mesmo recurso, ou seja muitas pessoas tentando acessar o mesmo recurso

```java
public class StringPerformanceTest {
    public static void main(String[] args) {
        long inicio = System.currentTimeMillis();
        concatStringBuffer(100_000);
        long fim = System.currentTimeMillis();
        System.out.println("Tempo gasto para StringBuffer: "+ (fim - inicio) + "ms");
    }

    

    private static void concatStringBuffer(int tamanho) {
        StringBuffer sb = new StringBuffer(tamanho);

        for (int i=0; i< tamanho; i++) {
            sb.append(i);
        }
    }
}
```

### Quando usar String, StringBuilder ou StringBuffer???

O que vai dizer se você vai usar stringBuilder ou String Builder são as regras de negócio, depende muito de sistema para sistema.

Na maioria das vezes usaremos string normalmente, porém varia de caso para caso 

### String Builder

Quando trabalhamos com `StringBuilder` não temos mais o conceito de string imutável, pois StringBuilder não é uma string é uma classe que vai trabalhar com strings


## Date

Atualmente ninguém usa Date no Java em breve será mostrado outras formas, porém é importante conhecer como funciona, pode ser que você vai dar manutenção em um sistema legado.

Devido a vários problemas que foram surgindo ao longo dos anos a classe Date se tornou obsoleta.

```java
public class DateTest01 {
    public static void main(String[] args) {
        Date date = new Date(1_000_000_000L); //long 100000
        System.out.println(date);
        System.out.println(date.getTime());

        date.setTime(date.getTime() + 3_600_000);
    }
}
```

## Calendar

Da mesma forma que a classe Date, só vamos trabalhar com isso em sistemas legados, ninguem utiliza

```java
public class CalendarTest01 {
    public static void main(String[] args) {
        Calendar c = Calendar.getInstance();
        if(c.getFirstDayOfWeek() == Calendar.SUNDAY) {
            System.out.println("Domingão é o primeiro dia da semana");
        }
        System.out.println(c.get(Calendar.DAY_OF_WEEK));
        System.out.println(c.get(Calendar.DAY_OF_MONTH));
        System.out.println(c.get(Calendar.DAY_OF_YEAR));
        System.out.println(c.get(Calendar.DAY_OF_WEEK_IN_MONTH));

        c.add(Calendar.DAY_OF_MONTH, 2);
        c.roll(Calendar.HOUR, 12);
        Date date = c.getTime();
        System.out.println(date);

    }
}

```

## Date Format

```java
public class DateFormatTest01 {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        DateFormat[] df = new DateFormat[7];
        df[0] = DateFormat.getInstance();
        df[1] = DateFormat.getDateInstance();
        df[2] = DateFormat.getDateTimeInstance();
        df[3] = DateFormat.getDateInstance(DateFormat.SHORT);
        df[4] = DateFormat.getDateInstance(DateFormat.MEDIUM);
        df[5] = DateFormat.getDateInstance(DateFormat.LONG);
        df[6] = DateFormat.getDateInstance(DateFormat.FULL);

        for (DateFormat dateFormat : df) {
            System.out.println(dateFormat.format(calendar.getTime()));
        }

    }
}
```

## Internacionalização Números com Locale

A classe local foi criada para trabalharmos com internacionalização é utilizado para trabalharmos com Datas, moedas e números baseado no usuário ou na localização da JVM dele.

```java
public class LocaleTest01 {
    public static void main(String[] args) {
        // pt-BR
        Locale localeItaly = new Locale("it", "IT");
        Locale localeCH = new Locale("it", "CH");
        Locale localeBR = new Locale("pt", "BR");
        Locale localeIndia = new Locale("hi", "IN");
        Locale localeJapan = new Locale("ja", "JP");
        Locale localeHolanda = new Locale("nl", "NL");


        Calendar calendar = Calendar.getInstance();
        DateFormat df1 = DateFormat.getDateInstance(DateFormat.FULL, localeItaly);
        DateFormat df2 =  DateFormat.getDateInstance(DateFormat.FULL, localeCH);
        DateFormat df3 =  DateFormat.getDateInstance(DateFormat.FULL, localeBR);
        DateFormat df4 =  DateFormat.getDateInstance(DateFormat.FULL, localeIndia);
        DateFormat df5 =  DateFormat.getDateInstance(DateFormat.FULL, localeJapan);
        DateFormat df6 =  DateFormat.getDateInstance(DateFormat.FULL, localeHolanda);

        System.out.println("Italia "+ df1.format(calendar.getTime()));
        System.out.println("Italia "+ df2.format(calendar.getTime()));
        System.out.println("Brasil "+ df3.format(calendar.getTime()));
        System.out.println("India "+ df4.format(calendar.getTime()));
        System.out.println("Japão "+ df5.format(calendar.getTime()));
        System.out.println("Holanda "+ df6.format(calendar.getTime()));

    }
}
```

## Internacionalização Moeda com Locale

```java
public class NumberFormatTest02 {
    public static void main(String[] args) {
        Locale localeDefault = Locale.getDefault();
        Locale localePT = new Locale("pt", "BR");
        Locale localeJP = Locale.JAPAN;
        Locale localeIT = Locale.ITALY;
        NumberFormat[] nfa = new NumberFormat[4];
        nfa[0] = NumberFormat.getCurrencyInstance();
        nfa[1] = NumberFormat.getCurrencyInstance(localeJP);
        nfa[2] = NumberFormat.getCurrencyInstance(localePT);
        nfa[3] = NumberFormat.getCurrencyInstance(localeIT);
        double valor = 10_000.2130;
        for (NumberFormat numberFormat : nfa) {
            System.out.println(numberFormat.getMaximumFractionDigits());
            System.out.println(numberFormat.format(valor));
            System.out.println(numberFormat.format(valor));
        }

        String valorString = "￥10,000"; // deve fazer o parse da moeda
        try {
            System.out.println(nfa[1].parse(valorString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
```

## Simple Date Format

A classe Simple Date format é um pouco mais facil do que a classe Date Format

```java
public class SimpleDateFormatTest01 {
    public static void main(String[] args) {
        String pattern = "yyyy.MM.dd G 'at' HH:mm:ss z";
        String pattern2 = "'Amsterdam' dd 'de' MMMM 'de' yyyy"; // em aspas simples será ignorado

        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        SimpleDateFormat sdf2 = new SimpleDateFormat(pattern2);

        System.out.println(sdf.format(new Date()));
        System.out.println(sdf2.format(new Date()));
        try {
            System.out.println(sdf2.parse("Amsterdam 18 de março de 2023"));
        }catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
```

## Local Date

No geral as novas aplicações em Java devem utilizar a classe `LocalDate` que é a melhor forma de se trabalhar com data na atualidade.

```java
public class LocalDateTest01 {
    public static void main(String[] args) {
        LocalDate date = LocalDate.of(2023, Month.JANUARY, 27);
        System.out.println(date.getYear());
        System.out.println(date.getMonth());
        System.out.println(date.getMonthValue());
        System.out.println(date.getDayOfWeek());
        System.out.println(date.getDayOfMonth());
        System.out.println(date.lengthOfMonth());
        System.out.println(date.isLeapYear()); // é bisexto
        System.out.println(date.get(ChronoField.YEAR));
        System.out.println(date.get(ChronoField.DAY_OF_MONTH));
        System.out.println(date);

        LocalDate agora = LocalDate.now();
        System.out.println(agora);
        System.out.println(LocalDate.MAX);
        System.out.println(LocalDate.MIN);
    }
}
```

## Local Time

É uma classe muito boa para trabalhar com datas, relatórios etc.

```java
public class LocalTimeTest01 {
    public static void main(String[] args) {
        LocalTime time = LocalTime.of(23, 32, 12);
        LocalTime timeNow = LocalTime.now();
        System.out.println(time);
        System.out.println(time.getHour());
        System.out.println(time.getMinute());
        System.out.println(time.getSecond());
        System.out.println(time.get(ChronoField.HOUR_OF_DAY));
        System.out.println(time.get(ChronoField.CLOCK_HOUR_OF_AMPM));
        System.out.println(LocalTime.MIN);
        System.out.println(LocalTime.MAX);
    }
}
```

## Local DateTime

Muito boa para trabalhar com datas 

```java
public class LocalDateTimeTest01 {
    public static void main(String[] args) {
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDate date = LocalDate.parse("2022-08-06");
        LocalTime time = LocalTime.parse("09:45:00");

        System.out.println(localDateTime);
        System.out.println(date);
        System.out.println(time);

        LocalDateTime ldt1 = date.atTime(time);
        LocalDateTime ldt2 = time.atDate(date);
        System.out.println(ldt1);
    }
}
```

## Instant

```java
public class InstantTest01 {
    public static void main(String[] args) {
        Instant now = Instant.now();
        System.out.println(now);
        System.out.println(LocalDateTime.now());
        System.out.println(now.getEpochSecond());
        System.out.println(now.getNano());
        System.out.println(Instant.ofEpochSecond(3, 0));
    }
}
```

## Duration

Foi criada para trabalhar com a quantidade de tempo, geralmente utilizada para pegar intervalo entre datas.

```java
public class DurationTest01 {
  public static void main(String[] args) {
    LocalDateTime now = LocalDateTime.now();
    LocalDateTime nowAfterTwoYears = LocalDateTime.now().plusYears(2);
    LocalTime timeNow = LocalTime.now();
    LocalTime timeMinus7Hour = LocalTime.now().minusHours(7);

    Duration d1 = Duration.between(now, nowAfterTwoYears);
    Duration d2 = Duration.between(timeNow, timeMinus7Hour);
    Duration d3 = Duration.between(Instant.now(), Instant.now().plusSeconds(10000));
    Duration d4 = Duration.ofDays(20);
    Duration d5 = Duration.ofMinutes(3);

    System.out.println(d1);
    System.out.println(d2);
    System.out.println(d3);
    System.out.println(d4);
    System.out.println(d5);
  }
}
```

## Period

```java
public class PeriodTest01 {
  public static void main(String[] args) {
    LocalDate now = LocalDate.now();
    LocalDate nowAfterTwoYear = LocalDate.now().plusYears(2).plusDays(7);
    Period p1 = Period.between(now, nowAfterTwoYear);
    Period p2 = Period.ofDays(10);
    Period p3 = Period.ofWeeks(58);
    Period p4 = Period.ofMonths(3);
    Period p5 = Period.ofYears(3);

    System.out.println(p1);
    System.out.println(p2);
    System.out.println(p3);
    System.out.println(p4);
    System.out.println(p5);
    System.out.println(p3.getMonths());
    System.out.println(Period.between(now, now.plusDays(p3.getDays())).getMonths());
    System.out.println(now.until(now.plusDays(p3.getDays()), ChronoUnit.DAYS));
  }
}
```

## ChronoUnit

Bem prática para trabalhar com intervalos de datas

```java
public class ChronoUnitTest01 {
    public static void main(String[] args) {
        LocalDateTime aniversario = LocalDateTime.of(1987, Month.DECEMBER, 21, 0, 0);
        LocalDateTime now = LocalDateTime.now();
        System.out.println(ChronoUnit.DAYS.between(aniversario, now));
        System.out.println(ChronoUnit.WEEKS.between(aniversario, now));
        System.out.println(ChronoUnit.MONTHS.between(aniversario, now));
    }
}
```

## TemporalAdjusters

Classe para trabalharmos com ajustes no tempo

```java
public class TemporalAdjustersTest01 {
    public static void main(String[] args) {
        LocalDate now = LocalDate.now();
        now = now.plusDays(20);
        System.out.println(now);
        System.out.println(now.getDayOfWeek());

        now = LocalDate.now().with(TemporalAdjusters.nextOrSame(DayOfWeek.THURSDAY));
        System.out.println(now);
        System.out.println(now.getDayOfWeek());

        now = LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.THURSDAY));
        System.out.println(now);
        System.out.println(now.getDayOfWeek());

        now = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
        System.out.println(now);
        System.out.println(now.getDayOfWeek());

        now = LocalDate.now().with(TemporalAdjusters.lastDayOfMonth());
        System.out.println(now);
        System.out.println(now.getDayOfWeek());

        now = LocalDate.now().with(TemporalAdjusters.firstDayOfNextYear());
        System.out.println(now);
        System.out.println(now.getDayOfWeek());

        now = LocalDate.now().with(TemporalAdjusters.firstDayOfNextMonth());
        System.out.println(now);
        System.out.println(now.getDayOfWeek());
    }
}
```

## TemporalAdjuster

```java
class ObterProximoDiaUtil implements TemporalAdjuster {

    @Override
    public Temporal adjustInto(Temporal temporal) {
        DayOfWeek dayOfWeek = DayOfWeek.of(temporal.get(ChronoField.DAY_OF_WEEK));
        int addDays = 1;
        switch (dayOfWeek) {
            case FRIDAY: addDays = 3; break;
            case SATURDAY: addDays = 2; break;
            default: addDays = 1;
        }
        return temporal.plus(addDays, ChronoUnit.DAYS);
    }
}
public class TemporalAdjusterTest01 {
    public static void main(String[] args) {
        LocalDate now = LocalDate.now();
        System.out.println(now);
        System.out.println(now.getDayOfWeek());

        now = LocalDate.now().with(new ObterProximoDiaUtil());
        System.out.println(now);
        System.out.println(now.getDayOfWeek());
    }
}
```

## ZonedDateTime Zoneld OffsetDateTime

```java
public class ZoneTest01 {
    public static void main(String[] args) {
        Map<String, String> shortIds = ZoneId.SHORT_IDS; // Listando as Zonas disponíveis no java
        System.out.println(shortIds);
        System.out.println(ZoneId.systemDefault());
        ZoneId tokyoZone =  ZoneId.of("Asia/Tokyo");
        System.out.println(tokyoZone);
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);

        ZonedDateTime zonedDateTime = now.atZone(tokyoZone);
        System.out.println(zonedDateTime);

        Instant nowInstant = Instant.now();
        System.out.println(nowInstant);
        ZonedDateTime zonedDateTime2 = nowInstant.atZone(tokyoZone);
        System.out.println(zonedDateTime2);

        System.out.println(ZoneOffset.MIN);
        System.out.println(ZoneOffset.MAX);

        ZoneOffset offsetManaus = ZoneOffset.of("-04:00");
        OffsetDateTime offsetDateTime = now.atOffset(offsetManaus);
        System.out.println(offsetDateTime);

        OffsetDateTime offsetDateTime2 = OffsetDateTime.of(now, offsetManaus);
        System.out.println(offsetDateTime2);

        OffsetDateTime offsetDateTime3 = nowInstant.atOffset(offsetManaus);
        System.out.println(offsetDateTime3);

        JapaneseDate japaneseDate = JapaneseDate.from(LocalDate.now());
        System.out.println(japaneseDate);
        LocalDate meijiEraLocalDate = LocalDate.of(1900,2,1);
        JapaneseDate meijiEra = JapaneseDate.from(meijiEraLocalDate);
        System.out.println(meijiEra);
    }
}
```

## DateTimeFormatter

Um conceito importante quando trabalhamos com formatação é que toda vez que você vê a palavra `format` significa que você está transformando do seu objeto para uma string e toda vez que você vê a palvra `parse` está transformando do seu string para um objeto.

```java
public class DateTimeFormatterTest01 {
    public static void main(String[] args) {
        LocalDate date = LocalDate.now();
        String s1 = date.format(DateTimeFormatter.BASIC_ISO_DATE);
        String s2 = date.format(DateTimeFormatter.ISO_DATE);
        String s3 = date.format(DateTimeFormatter.ISO_LOCAL_DATE);

        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);

        LocalDate parse1 = LocalDate.parse("20230318", DateTimeFormatter.BASIC_ISO_DATE);
        LocalDate parse2 = LocalDate.parse("2023-03-18", DateTimeFormatter.ISO_DATE);
        LocalDate parse3 = LocalDate.parse("2023-03-18", DateTimeFormatter.ISO_LOCAL_DATE);
        System.out.println(parse1);
        System.out.println(parse2);
        System.out.println(parse3);

        LocalDateTime now = LocalDateTime.now();
        String s4 = now.format(DateTimeFormatter.ISO_DATE_TIME);
        System.out.println(s4);

        LocalDateTime parse4 = LocalDateTime.parse("2023-03-18T09:27:38.604426");
        System.out.println(parse4);

        // dd/MM/yyyy
        DateTimeFormatter formatterBR = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String formatBR = LocalDate.now().format(formatterBR);
        System.out.println(formatBR);
    }
}
```

## ResourceBundle

É uma forma de internacionalizar suas mensagens.

Para isso vamos criar uma arquivo dentro da pasta `src` e deve ser dentro dessa pasta e o arquivo tem um padrão de nome.

Vamos criar uma arquivo para mensagens em ingles, então deve ser exatamente como abaixo:

    messages_en_US.properties

O arquivo `properties`é uma arquivo onde temos chave e valor

## Regex-Pattern e Matcher

Expressões regulares nda mais é do que uma linguagem que foi desenvolvida que utiliza metacharacter que utiliza simbolos para encontrar padrões no texto.


```java
public class PatternMatcherTest01 {
    public static void main(String[] args) {
        String regex = "ab";
        String texto = "abaaba";
        String texto2 = "abababa";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(texto);

        System.out.println("texto: "+texto);
        System.out.println("indice: 0123456789");
        System.out.println("regex: "+ regex);
        System.out.println("Posições encontradas");

        while (matcher.find()) {
            System.out.print(matcher.start() + " ");
        }
    }
}
```

## Regex-Pattern e Matcher-Meta characteres

### Metacaracter

São atalhos para conseguir pegar determinados caracteres

- `\d`: Todos os dígitos
- `\D`: Tudo que não for dígito inclusive caracteres especiais
- `\s`: Todos os espaços em branco `\t \n \f \r
- `\S`: Todos os caracteres excluindo os brancos
- `\w`: Tudo que for de `a-zA-Z, dígitos, _` (excluir os caracteres especiais)
- `\W`: Tudo que não for incluso no `\w` (ao contrário do \w)
- `?`: Zero ou uma ocorrência
- `*`: zero ou mais
- `+`: uma ou mais
- `{n,m}`: de n até m
- `()`: de agrupamento
- `|`: o(o|c)o ovo | oco
- `$`: Representa o fim da linha
- `. 1.3`: 123, 133, 1@3 1A3

```java
public class PatternMatcherTest03 {
    public static void main(String[] args) {
        // \d = Todos os dígitos
        // \D = Tudo que não for dígito inclusive caracteres especiais
        // \s = Todos os espaços em branco \t \n \f \r
        // \S = Todos os caracteres excluindo os brancos
        // \w = Tudo que for de a-zA-Z, dígitos, _ (excluir os caracteres especiais)
        // \W = Tudo que não for incluso no \w
        // ? Zero ou uma ocorrência
        // * zero ou mais
        // + uma ou mais
        // {n,m} de n até m
        // () de agrupamento
        // | o(o|c)o ovo | oco
        //  $ representa o fim da linha
        // . 1.3 = 123, 133, 1@3 1A3
        
        String regex = "\\S";
        String texto = "@#jk32 m\tkl1";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(texto);

        System.out.println("texto: "+texto);
        System.out.println("indice: 0123456789");
        System.out.println("regex: "+ regex);
        System.out.println("Posições encontradas");

        while (matcher.find()) {
            System.out.print(matcher.start() + " "+matcher.group()+"\n");
        }
    }
}
```

## Regex-Pattern e Matcher-Range

- `[]`: Procura em um range
- `[a-zA-c]`: Procura letras entre a à z letras maiuculas entre A-C

Valores em Hexadecimal no java precisam ter `0x`ou `0X` antes do número, por exemplo

```java
public class PatternMatcherTest03 {
    public static void main(String[] args) {
        
        // Número em Hexadecimal
        int numeroHexadecimal = 0x59F86A;
        System.out.println(numeroHexadecimal);
    }
}
```

### Exercício 

Imagine que temos um texto e precisamos procurar no texto, números hexadecimais válidos

```java
public class PatternMatcherTest03 {
    public static void main(String[] args) {

        String regex = "0[xX][0-9a-fA-F]";
        String texto = "12 0x 0x 0xFFABC 0x10G 0x1";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(texto);

        System.out.println("texto: "+texto);
        System.out.println("indice: 0123456789");
        System.out.println("regex: "+ regex);
        System.out.println("Posições encontradas");

        while (matcher.find()) {
            System.out.print(matcher.start() + " "+matcher.group()+"\n");
        }

        int numeroHexadecimal = 0x59F86A;
        System.out.println(numeroHexadecimal);
    }
}
```

## Regex-Pattern e Matcher-Quantificadores

Quantificadores são caracteres que te dão poder de pegar determinada expressão baseada na quantidade que o metacaracter representa.


```java
public class PatternMatcherTest03 {
    public static void main(String[] args) {
        String regex = "0[xX]([0-9a-fA-F])+(\\s|$)";
        String texto = "12 0x 0x 0xFFABC 0x10G 0x1";

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(texto);

        System.out.println("texto: "+texto);
        System.out.println("indice: 0123456789");
        System.out.println("regex: "+ regex);
        System.out.println("Posições encontradas");

        while (matcher.find()) {
            System.out.print(matcher.start() + " "+matcher.group()+"\n");
        }
    }
}
```

vamos criar um expressão regular para encontrar e-mails válidos no texto abaixo:

Texto: 
```text
luffy@gmail.com, 123jotaro@gmail.com, #@!xoro@gmail.com.br ola meu amigo, teste@gmail.com, sakura@mail.com
```

```java
public class PatternMatcherTest04 {
    public static void main(String[] args) {
        String regex = "([a-zA-Z0-9\\._-])+@([a-zA-Z])+(\\.([a-zA-Z])+)+";
        String texto = "luffy@gmail.com, 123jotaro@gmail.com, #@!zoro@gmail.com.br ola meu amigo, teste@gmail.com, sakura@mail.com";
        System.out.println("zoro@gmail.com.br".matches(regex));

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(texto);

        System.out.println("texto: "+texto);
        System.out.println("indice: 0123456789");
        System.out.println("regex: "+ regex);
        System.out.println("Posições encontradas");

        while (matcher.find()) {
            System.out.print(matcher.start() + " "+matcher.group()+"\n");
        }
    }
}
```

## Regex-Pattern e Matcher-Anchor

Site: https://regexr.com/

## Scanner Tokens e Delimitadores

Imagine que temos um texto `luffy@htomail.com, 123@gmail.com` neste caso os tokens seria o email, e os delimitadores seriam as virgulas.

Vamos ver um exemplo simples utilizando strings

```java
public class ScannerTest01 {
    public static void main(String[] args) {
        String texto = "Levi, Eren, Mikasa, true, 200";
        String[] nomes = texto.split(",");
        for (String nome : nomes) {
            System.out.println(nome.trim());
        }
    }
}
```

Agora usando a classe scanner:

```java
public class ScannerTest02 {
    public static void main(String[] args) {
        String texto = "Levi,Eren,Mikasa,true,200";
        Scanner scanner = new Scanner(texto);
        scanner.useDelimiter(",");
        while (scanner.hasNext()) {
            if (scanner.hasNextInt()) {
                int i = scanner.nextInt();
                System.out.println("Int "+i);
            } else if (scanner.hasNextBoolean()) {
                boolean b = scanner.nextBoolean();
                System.out.println("Boolean "+b);
            } else {
                System.out.println(scanner.next());
            }
        }
    }
}
```

## IO File

Trabalhando com arquivos com a classe `File`

```java
public class FileTest01 {
    public static void main(String[] args) {
        File file = new File("tmp/tile.txt");

        createNewFile(file);

//        if (deleteFile(file)) {
//            System.out.println("Arquivo deletado com sucesso");
//        } else {
//            System.out.println("Nenhum arquivo encontrado");
//        }
    }

    private static void createNewFile(File file) {
        try {
            boolean isCreated = file.createNewFile();
            System.out.println(isCreated);
            System.out.println("path: "+file.getPath());
            System.out.println("path absolute: "+file.getAbsolutePath());
            System.out.println("is directory: "+file.isDirectory());
            System.out.println("is file: "+file.isFile());
            System.out.println("is Hidden: "+file.isHidden()); // é oculto
            System.out.println("last modified: "+ Instant.ofEpochMilli(file.lastModified()).atZone(ZoneId.systemDefault()).toLocalDateTime());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static Boolean deleteFile(File file) {
        if (file.exists()) {
             return file.delete();
        }
        return false;
    }
}
```

## IO File Writer

Trabalhando com a Classe Writer

Vale lembrar que quando trabalhamos com leitura e escrita de arquivos, estamos pegando recursos dos sistema operacional, e quase sempre temos que fechar os recursos.

Deste modo podemos usar o `try with resources` porém temos de ter certeza que de classe FileWriter implementa o `Closable` então podemos utilizar no try com recursos.


Quando trabalhamos com arquivos existe a possiblidade de fecharmos o nosso arquivo porém ainda tem coisas dentro do buffer e como ele será fechado automaticamente já que estamos utilizando o try with resources, então usamos o flush para limpar os dados do buffer.

```java
public class FileWriterTest01 {
    public static void main(String[] args) {
        File file = new File("tmp/file2.txt");

        try (FileWriter fw = new FileWriter(file)) {
            fw.write("O DevDojo é o melhor curso de Java\nContinuando na próxima linha");
            // Existe a possiblidade de fecharmos o nosso arquivo porém ainda tem coisas dentro do buffer e como ele será fechado automaticamente já que estamos utilizando o try with resources, então usamos o flush
            fw.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
```

Quando fazemos a escrita novamente no nossa arquivo, ele é criado novamente e tudo será substituido, podemos alterar esse comportamento fazendo uma espécie de append utilizando um segundo parametro boolean que define se é append ou não.

Com a opção append ativada o arquivo não será sobrescrito:

```java
public class FileWriterTest01 {
    public static void main(String[] args) {
        File file = new File("tmp/file2.txt");

        try (FileWriter fw = new FileWriter(file, true)) {
            fw.write("O DevDojo é o melhor curso de Java\nContinuando na próxima linha");
            // Existe a possiblidade de fecharmos o nosso arquivo porém ainda tem coisas dentro do buffer e como ele será fechado automaticamente já que estamos utilizando o try with resources, então usamos o flush
            fw.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
}
```

## IO File Reader

É uma das classes mais baixo nível que nos temos.

```java
public class FileWriterTest01 {
    public static void main(String[] args) {
        File file = new File("tmp/file2.txt");

        try (FileReader fr = new FileReader(file)) {
            char[] in = new char[30];
//            fr.read(in);
//            for(char c: in) {
//                System.out.print(c);
//            }
            int i;
            while ((i= fr.read()) != -1) {
                System.out.print((char)i);
            }
            System.out.println(fr.read());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```

## IO BufferedWriter

As classes `File`, `FileWriter` e `FileReader` não foram criadas pensando em performance, por conta disso quando trabalhamos com arquivos muito grandes usamos as classes `Buffered`:

```java
public class BufferedReaderTest01 {
    public static void main(String[] args) {
        File file = new File("tmp/file3.txt");

        try (FileWriter fw = new FileWriter(file, true);
             BufferedWriter bw = new BufferedWriter(fw)) {
            bw.write("O DevDojo é o melhor curso de Java");
            bw.newLine(); // Pega o line separator do SO
            bw.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
```

## IO BufferedReader

```java
public class BufferedReaderTest01 {
    public static void main(String[] args) {
        File file = new File("tmp/file2.txt");

        try (FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr)) {

            String line;
            while((line = br.readLine()) != null) {
                System.out.println(line);
            }
            br.readLine(); // Lê uma linha inteira

        } catch (IOException e) {
            e.printStackTrace();
        }
    
```

## IO File para diretórios

```java
public class FileTest02 {
    public static void main(String[] args) throws IOException {
        File fileDirectory = new File("tmp/pasta");
        boolean isDirectorCreated =  fileDirectory.mkdir();
        System.out.println(isDirectorCreated);

        File fileArquivoDirectory = new File(fileDirectory, "arquivo.txt");
        boolean isFileCreated = fileArquivoDirectory.createNewFile();
        System.out.println(isFileCreated);

        // Rename file
        File fileRenamed = new File(fileDirectory, "arquivo_renomeado.txt");
        boolean isRenamed = fileArquivoDirectory.renameTo(fileRenamed);
        System.out.println(isRenamed);

        File directoryRenamed = new File("tmp/pasta2");
        boolean isDirectoryRenamed = fileDirectory.renameTo(directoryRenamed);
        System.out.println(isDirectoryRenamed);
    }
}
```

## NIO Path Paths Files

O Java criou a interface `Path` para substituir a classe `File` 

```java
public class PathTest01 {
    public static void main(String[] args) {
        Path p1 = Paths.get("tmp/file2.txt");
        System.out.println(p1.getFileName());
        System.out.println(p1.getFileName().toFile());

        Path p2 = Paths.get("tmp","file2.txt");
        System.out.println(p2.toFile());
        System.out.println(p2.getFileName());
    }
}
```

```java
public class PathTest02 {
    public static void main(String[] args) throws IOException {
        // Pega o diretório que estamos
        Path pastapath = Paths.get("tmp/pasta");

        // Se a pasta já existe ele lança exception
        if (Files.notExists(pastapath)) {
            Path pastaDirectory = Files.createDirectory(pastapath);
        }

        // Neste caso não lança exception caso o diretório já exista!!!
        Path subPatasPaths = Paths.get("tmp/pasta3/subpasta/susubpasta");
        Path subPastaDirectory = Files.createDirectories(subPatasPaths);


        Path filePath = Paths.get(subPatasPaths.toString(), "file2.txt");
        if (Files.notExists(pastapath)) {
            Path filePathCreated = Files.createFile(filePath);
        }

        // Copy file

        Path source = filePath;
        Path target = Paths.get(filePath.getParent().toString(), "file_renamed.txt");
        Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
    }
}
```

## NIO Normalização

```java
public class NormalizeTest01 {
    public static void main(String[] args) {
        String diretorioProjeto = "tmp/home/dev";
        String arquivoTxt = "../../arquivo.txt";
        Path path1 = Paths.get(diretorioProjeto, arquivoTxt);
        System.out.println(path1.normalize());
    }
}
```

## NIO Resolvendo Paths

Vai unir casos temos dois paths separados.

```java
public class PathTest03 {
    public static void main(String[] args) {
        Path dir = Paths.get("tmp/home/andre");
        Path arquivo = Paths.get("dev/arquivo.txt");
        Path resolve = dir.resolve(arquivo); // junta os paths
        System.out.println(resolve);
    }
}
```

devemos tomar cuidados com caminhos absolutos e relativos, pois dependendo vamos ter problemas:

```java
public class PathTest03 {
    public static void main(String[] args) {
        Path dir = Paths.get("tmp/home/andre");
        Path arquivo = Paths.get("dev/arquivo.txt");
        Path resolve = dir.resolve(arquivo); // junta os paths
        System.out.println(resolve);

        Path absoluto = Paths.get("tmp/home/andre");
        Path relativo = Paths.get("dev");
        Path file = Paths.get("file.txt");

        System.out.println("1 "+ absoluto.resolve(relativo));
        System.out.println("2 "+ absoluto.resolve(file));
        System.out.println("3 "+ relativo.resolve(absoluto));
        System.out.println("4 "+ relativo.resolve(file));
    }
}
```

## NIO Relativize

Relativar um path, digamos que temos dois paths, o path1 e o path2 como você faz para chegar no path2 a partir do path1

Como podemos chegar de um diretório para outro.

```java
public class RelativizeTest01 {
    public static void main(String[] args) {
        Path dir = Paths.get("/home/andre");
        Path classe = Paths.get("/home/andre/dev/OlaMundo.java");

        Path pathToClasse = dir.relativize(classe);
        System.out.println(pathToClasse);

        Path absoluto1 = Paths.get("/home/andre");
        Path absoluto2 = Paths.get("/usr/local");
        Path absoluto3 = Paths.get("/home/andre/devdojo/OlaMundo.java");

        Path relativo1 = Paths.get("temp");
        Path relativo2 = Paths.get("/temp/temp.8921030");

        System.out.println("1 "+absoluto1.relativize(absoluto3));
        System.out.println("2 "+absoluto3.relativize(absoluto1));
        System.out.println("3 "+absoluto1.relativize(absoluto2));
        System.out.println("4 "+relativo1.relativize(relativo2));
        System.out.println("3 "+absoluto1.relativize(relativo1));
    }
}
```

## NIO BasicFileAttributes

No Java forma introduzidas 3 interfaces básicas para trabalhar com atributos básicos de arquivos:

No java foram criadas 3 classes para trabalhar com diferentes tipos de sistemas operacionais:  `BasicFileAttributes`, `DosFileAttributes`, `PosixFileAttributes`.

Trocando a data de modificação de um arquivo.

```java
public class BasicFileAttributesTest01 {
    public static void main(String[] args) throws IOException {
        //BasicFileAttributes, DosFileAttributes, PosixFileAttributes
        LocalDateTime date = LocalDateTime.now().minusDays(10);
        File file = new File("tmp/pasta2/novo.txt");
        boolean isCreated = file.createNewFile();
        boolean lastModified = file.setLastModified(date.toInstant(ZoneOffset.UTC).toEpochMilli());
        System.out.println(lastModified);

        Path path = Paths.get("tmp/pasta2/novo_path.txt");
        Files.createFile(path);

        FileTime fileTime = FileTime.from(date.toInstant(ZoneOffset.UTC));
        Files.setLastModifiedTime(path, fileTime);
        System.out.println(Files.isWritable(path));
        System.out.println(Files.isReadable(path));
        System.out.println(Files.isExecutable(path));
    }
}
```

```java
public class BasicFileAttributesTest02 {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("tmp/pasta2/novo_path.txt");
        BasicFileAttributes basicFileAttributes = Files.readAttributes(path, BasicFileAttributes.class);
        FileTime creationTime = basicFileAttributes.creationTime();
        FileTime lastModifiedTime = basicFileAttributes.lastModifiedTime();
        FileTime lastAccessTime = basicFileAttributes.lastAccessTime();

        System.out.println("CreationTime: "+creationTime);
        System.out.println("LastModifiedTime: "+lastModifiedTime);
        System.out.println("lastAccessTime: "+lastAccessTime);

        BasicFileAttributeView fileAttributeView = Files.getFileAttributeView(path, BasicFileAttributeView.class);
        FileTime newCreationTime = FileTime.fromMillis(System.currentTimeMillis());

        fileAttributeView.setTimes(lastModifiedTime, newCreationTime,creationTime);

        creationTime = fileAttributeView.readAttributes().creationTime();
        lastModifiedTime = fileAttributeView.readAttributes().lastModifiedTime();
        lastAccessTime = fileAttributeView.readAttributes().lastAccessTime();

        System.out.println("CreationTime: "+creationTime);
        System.out.println("LastModifiedTime: "+lastModifiedTime);
        System.out.println("lastAccessTime: "+lastAccessTime);
    }
}
```

## NIO DosFileAttribute

```java
public class DosFileAttributesTest01 {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("tmp/pasta/teste.txt");
        if (Files.notExists(path)) Files.createFile(path);

        DosFileAttributes dosFileAttributes = Files.readAttributes(path, DosFileAttributes.class);
        System.out.println(dosFileAttributes.isHidden());
        System.out.println(dosFileAttributes.isReadOnly());

        DosFileAttributeView fileAttributeView = Files.getFileAttributeView(path, DosFileAttributeView.class);
        fileAttributeView.setHidden(true);
        fileAttributeView.setReadOnly(true);

        System.out.println(fileAttributeView.readAttributes().isHidden());
        System.out.println(fileAttributeView.readAttributes().isReadOnly());
    }
}
```

## NIO PosixFileAttributes

```java
public class PosixFileAttributesTest01 {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("tmp/dev/file.txt");
        PosixFileAttributes posixFileAttributes = Files.readAttributes(path, PosixFileAttributes.class);
        System.out.println(posixFileAttributes.permissions());
        PosixFileAttributeView fileAttributeView = Files.getFileAttributeView(path, PosixFileAttributeView.class);

        Set<PosixFilePermission> posixFilePermissions = PosixFilePermissions.fromString("rw-rw-rw-");

        fileAttributeView.setPermissions(posixFilePermissions);
        System.out.println(fileAttributeView.readAttributes().permissions());
    }
}
```

## NIO DirectoryStream

Listando todos os diretórios da nossa pasta do curso porém não acessa os subníves de cada pasta:


```java
public class DirectoryStreamTest01 {
    public static void main(String[] args) {
        Path dir = Paths.get(".");
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            for (Path path: stream) {
                System.out.println(path);
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```

## NIO SimpleFileVisitor

Diferente da DirectoryStream, a `SimpleFileVisitor` nos permite acessar os subníveis das pastas:

Neste caso ele lista exatamente tudo que temos na nossa pasta:

```java
class ListAllFiles extends SimpleFileVisitor<Path> {
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        System.out.println(file.getFileName());
        return FileVisitResult.CONTINUE;
    }
}

public class SimpleFileVisitorTest01 {
    public static void main(String[] args) throws IOException {
        Path root = Paths.get(".");

        Files.walkFileTree(root, new ListAllFiles());
    }
}
```

Listando os arquivos `.Java`:

```java
class ListAllFiles extends SimpleFileVisitor<Path> {
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        if (file.getFileName().toString().endsWith(".java")) {
            System.out.println(file.getFileName());
        }
        return FileVisitResult.CONTINUE;
    }
}

public class SimpleFileVisitorTest01 {
    public static void main(String[] args) throws IOException {
        Path root = Paths.get(".");

        Files.walkFileTree(root, new ListAllFiles());
    }
}
```

```java
class ListAllFiles extends SimpleFileVisitor<Path> {
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        if (file.getFileName().toString().endsWith(".java")) {
            System.out.println(file.getFileName());
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        System.out.println("pre visit "+dir.getFileName());
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        return super.visitFileFailed(file, exc);
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        System.out.println("post "+dir.getFileName());
        return FileVisitResult.CONTINUE;
    }
}

public class SimpleFileVisitorTest02 {
    public static void main(String[] args) throws IOException {
        Path root = Paths.get("tmp/folder");

        Files.walkFileTree(root, new ListAllFiles());
    }
}
```

## NIO PathMatcher

A classe `PathMatcher`foi criada para facilitar um pouco a busca de paths.

```java
public class PathMatcherTest01 {
    public static void main(String[] args) {
        Path path1 = Paths.get("pasta/subpast/file.bpk");
        Path path2 = Paths.get("pasta/subpast/file.txt");
        Path path3 = Paths.get("pasta/subpast/file.java");

        matches(path1, "glob:*.bkp");
        matches(path1, "glob:**.bkp");
        matches(path1, "glob:**/*.bkp");
        matches(path1, "glob:**/*.{bkp,txt,java}");
        matches(path2, "glob:**/*.{bkp,txt,java}");
        matches(path3, "glob:**/*.{bkp,txt,java}");
        matches(path1, "glob:**/*.???");
        matches(path2, "glob:**/*.???");
        matches(path3, "glob:**/*.???");
    }

    private static void matches(Path path, String glob) {
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(glob);
        System.out.println(glob + ": " +matcher.matches(path));
    }
}
```

### Exerício

Utilizando a <b>PathMatcher</b> e a <b>SimpleFileVisitor</b> retorne todos os arquivos do nosso projeto que contenham <b>.Test</b> e que tenham a extesão <b>.Java</b> ou <b>.class</b>

```java
class FindAllTestJavaOrClass extends SimpleFileVisitor<Path> {
    private PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**/*{Test*}.{java,class}");

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (matcher.matches(file)) {
            System.out.println(file.getFileName());
        }
        return FileVisitResult.CONTINUE;
    }
}

public class ExercicioTest01 {
    public static void main(String[] args) throws IOException {
        Path root = Paths.get(".");
        Files.walkFileTree(root, new FindAllTestJavaOrClass());
    }
}
```

## NIO ZipOutStream

Zipando pastas com ZipOutStream

```java
public class ZipOutputStreamTest01 {
    public static void main(String[] args) {
        Path arquivoZip = Paths.get("tmp/pasta/arquivo.zip");
        Path aruivosParaZipar = Paths.get("tmp/pasta");
        zip(arquivoZip, aruivosParaZipar);
    }

    private static void zip(Path arquivoZip, Path arquivosParaZipar) {
        try(ZipOutputStream zipstream = new ZipOutputStream(Files.newOutputStream(arquivoZip));
            DirectoryStream<Path> directoryStream = Files.newDirectoryStream(arquivosParaZipar)) {

            for (Path file : directoryStream) {
                ZipEntry zipEntry = new ZipEntry(file.getFileName().toString());
                zipstream.putNextEntry(zipEntry);
                Files.copy(file, zipstream);
                zipstream.closeEntry();
            }

            System.out.println("Arquivo criado com sucesso");

        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
```

## Serialization

Pega um objeto que temos em memória e persistir esse objeto onde quisermos.

Neste exemplo vamos pegar um objeto que está em memória e salvar ele em uma arquivo.

```java
public class SerializacaoTest01 {
    public static void main(String[] args) {
        Aluno aluno = new Aluno(1L, "Andre Luiz", "1231231796");
        //serializar(aluno);
        deserializar();
    }

    private static void serializar(Aluno aluno) {
        Path path = Paths.get("tmp/aluno.ser");
        try (ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(path))) {
            oos.writeObject(aluno);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void deserializar() {
        Path path = Paths.get("tmp/aluno.ser");
        try (ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(path))) {
            Aluno aluno = (Aluno) ois.readObject();
            System.out.println(aluno);
        }catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
```

```java
public class SerializacaoTest01 {
    public static void main(String[] args) {
        Aluno aluno = new Aluno(1L, "Andre Luiz", "1231231796");
        Turma turma = new Turma("Maratona Java");
        aluno.setTurma(turma);
        serializar(aluno);
        deserializar();
    }

    private static void serializar(Aluno aluno) {
        Path path = Paths.get("tmp/aluno.ser");
        try (ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(path))) {
            oos.writeObject(aluno);
        }catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void deserializar() {
        Path path = Paths.get("tmp/aluno.ser");
        try (ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(path))) {
            Aluno aluno = (Aluno) ois.readObject();
            System.out.println(aluno);
        }catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
```