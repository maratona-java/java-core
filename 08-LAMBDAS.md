## Lambdas

## Sumário

1. [Predicate](#predicate)
2. [Consumer](#consumer)
3. [Function](#function)

## Predicate

- Para que as lambdas funcionem elas precisam que a interface que vocês está trabalhando seja um `interface funcional`.

Exemplo de interface funcional:

```java
public interface CarPredicate {
    boolean test(Car car);
}
```

- As lambdas ajudam a criar um código menor, mas enxuto.
- AS lambdas são anônimas, são funções, pois não estão atreladas a nenhuma classe.

<b>Regras</b>: 

- Uma `interface funcional` é uma interface onde você tem apenas um `método abstrato`.
- A lambda tem o parametro e o corpo.
- A expressão deve retornar um `boolean`.
  - `(parametro) -> <expressão>`
  - `(Car car) -> car.getColor().equals("green")`

    
Então qualquer interface funcional pode usar lambdas.
    
## Consumer

O consumer é um interface funcional parecida com a Predicate, porém a diferença é que ele executa uma operação e não retorna nada, seu retorno é `void`.

Exemplo de consumer:

```java
@FunctionalInterface
public interface Consumer<T> {
    void accept(T var1);
}
```


```java
public class LambdaTest01 {
    public static void main(String[] args) {
        List<String> strings = List.of("André", "Suane", "Paulo", "Luffy");
        forEach(strings, (String s) -> System.out.println(s));
    }

    private static <T> void forEach(List<T> list, Consumer<T> consumer) {
        for (T e : list) {
            consumer.accept(e);
        }
    }
}
```

Podemos ainda melhorar a escrita fazendo o seguinte replace:

```java
public class LambdaTest01 {
    public static void main(String[] args) {
        List<String> strings = List.of("André", "Suane", "Paulo", "Luffy");
        List<Integer> numbers = List.of(1,2,3,4,5);
        
        // Olha esse replace
        forEach(strings, System.out::println);
        forEach(numbers, System.out::println);
    }

    private static <T> void forEach(List<T> list, Consumer<T> consumer) {
        for (T e : list) {
            consumer.accept(e);
        }
    }
}
```

## Function

A function também é uma interface funcional e ela tem uma diferença, ou seja ela tem dois tipos.

Exemplo de function:

```java
@FunctionalInterface
public interface Function<T, R> {
    R apply(T var1);
}
```

Vamos fazer um exemplo onde vamos passar uma lista de strings e retornar uma lista com o tamanho de cada string que esta la dentro.

```java
public class lambdaTest02 {
    public static void main(String[] args) {
        List<String> strings = List.of("Natsu", "Allucard");
        List<Integer> integers = map(strings, (String s) -> s.length());
        System.out.println(integers);
    }

    private static <T, R>List<R> map(List<T> list, Function<T, R> function) {
        List<R> result = new ArrayList<>();
        for (T e : list) {
            R r = function.apply(e);
            result.add(r);
        }
        return result;
    }
}
```